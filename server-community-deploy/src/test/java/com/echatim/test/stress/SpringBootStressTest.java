package com.echatim.test.stress;

import com.alibaba.fastjson.JSONObject;
import com.echatim.Application;
import com.echatim.ApplicationWrapper;
import com.echatim.entity.User;
import com.echatim.form.HistoryListForm;
import com.echatim.form.HistoryListSessionForm;
import com.echatim.form.MessageSendForm;
import com.echatim.service.app.HistoryMessageService;
import com.echatim.service.app.MessageService;
import com.echatim.service.app.UserService;
import com.echatim.test.tools.LoopRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)

/**
 * SpringBoot 压力测试
 */

public class SpringBootStressTest {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private Environment environment;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;
    @Autowired
    private HistoryMessageService historyMessageService;

    @Before
    public void beforeApplicationContextStart(){
        ApplicationWrapper.setContext(applicationContext);
    }

    /**
     * test
     */
    @Test
    public void _0_test_send_message() {
        MessageSendForm messageSendForm = new MessageSendForm();
        messageSendForm.setFromUser("admin");
        messageSendForm.setToTarget("admin");
        messageSendForm.setToTargetAuid("admin");
        messageSendForm.setWay("P2P");
        messageSendForm.setType("TEXT");
        messageSendForm.setBody(JSONObject.parseObject("{\"body\": \"12345\"}"));
        // LoopRunner run 【send_message】 finish, use 39271ms
        // LoopRunner run 【send_message】 finish, use 13281ms (去除代码: userService.lambdaQuery().eq(User::getAuid, "admin").count();)
        LoopRunner.run("send_message", 10000, (_v)->{
            messageService.send(messageSendForm);
        });
    }


    @Test
    public void _1_test_db_user_auid_select() {
        // LoopRunner run 【user_auid_select】 finish, use 6756ms  1,492 QPS
        LoopRunner.run("db_user_auid_select", 10000, (_v)->{
            userService.lambdaQuery().eq(User::getAuid, "admin").count();
        });
    }



    @Test
    public void _4_test_get_history() throws ParseException {
        // 创建1000条数据
        MessageSendForm messageSendForm = new MessageSendForm();
        messageSendForm.setFromUser("admin");
        messageSendForm.setToTarget("admin");
        messageSendForm.setToTargetAuid("admin");
        messageSendForm.setWay("P2P");
        messageSendForm.setType("TEXT");
        messageSendForm.setBody(JSONObject.parseObject("{\"body\": \"12345\"}"));
        LoopRunner.run("send_message", 1000, (_v)->{
            messageService.send(messageSendForm);
        });

        HistoryListForm historyListForm = new HistoryListForm();
        historyListForm.setFromUser("admin");
        historyListForm.setToTarget("admin");
        historyListForm.setWay("P2P");
        historyListForm.setLimit(10);
        // 一小时内的历史记录
        historyListForm.setStartTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2021-05-08 16:43:18").getTime());
        historyListForm.setEndTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2021-06-23 23:59:59").getTime());

        // LoopRunner multiRun 【get_history】 finish, use 41315ms  0条Message缓存, 加入DB索引`from_to_time_idx` 多线程吞吐量625 QPS
        LoopRunner.multiRun("get_history", 100, (_v)->{
            historyMessageService.listMessage(historyListForm);
        });
    }


    @Test
    public void _5_test_get_session() throws ParseException {
        // 创建1000条缓存数据
        MessageSendForm messageSendForm = new MessageSendForm();
        messageSendForm.setFromUser("admin");
        messageSendForm.setToTarget("admin");
        messageSendForm.setToTargetAuid("admin");
        messageSendForm.setWay("P2P");
        messageSendForm.setType("TEXT");
        messageSendForm.setBody(JSONObject.parseObject("{\"body\": \"12345\"}"));
        LoopRunner.run("send_message", 1000, (_v)->{
            messageService.send(messageSendForm);
        });
        HistoryListSessionForm historyListSessionForm = new HistoryListSessionForm();
        historyListSessionForm.setAuid("admin");
        // 一小时内的历史记录
        historyListSessionForm.setStartTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2021-05-08 16:43:18").getTime());
        historyListSessionForm.setEndTimestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2021-06-23 23:59:59").getTime());
        // LoopRunner multiRun 【get_session】 finish, use 750,886ms  0条Message缓存, 加入DB索引`from_to_time_idx` 多线程吞吐量1287 QPS
        LoopRunner.multiRun("get_session", 1000, (_v)->{
            historyMessageService.listSessions(historyListSessionForm);
        });
    }


    @Test
    public void _7_test_db_message_select() {
        // MAC LoopRunner multiRun 【db_message_select】 finish, use 68,460ms  147 QPS
        LoopRunner.multiRun("db_message_select", 10000, (_v)->{
            messageService.list();
        });
    }

}
