package com.echatim.test.springboot;

import com.broker.base.protocol.response.Resp;
import com.echatim.Application;
import com.echatim.ApplicationWrapper;
import com.echatim.dto.ServerInfoDTO;
import com.echatim.service.app.GitPropertiesService;
import com.echatim.service.app.MessageService;
import com.echatim.service.app.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)

/**
 * SpringBoot 压力测试
 */

public class SpringBootGitVersion {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private Environment environment;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;
    @Autowired
    private GitPropertiesService gitPropertiesService;

    @Before
    public void beforeApplicationContextStart(){
        ApplicationWrapper.setContext(applicationContext);
    }

    /**
     * test
     */
    @Test
    public void _0_test_get_git_version() throws IOException {
//        ClassLoader classLoader = getClass().getClassLoader();
//        InputStream inputStream = classLoader.getResourceAsStream("git.properties");

//        String jsonStr = StreamUtils.copyToString(inputStream, Charset.defaultCharset());
//        JSONObject jsonObject = JSON.parseObject(jsonStr);
//        Set<Map.Entry<String, Object>> entrySet = jsonObject.entrySet();
//        System.out.println("entry:" + Beans.json(entrySet));

//        Properties confProperties = new Properties();
//        confProperties.load(inputStream);
//        String branch = confProperties.getProperty("git.branch");
//        String gitIdAbbrev = confProperties.getProperty("git.commit.id.abbrev");
//        String gitTime = confProperties.getProperty("git.commit.time");

        Resp<ServerInfoDTO> resp = gitPropertiesService.getInfo();

//        Map<String, Object> build = gitPropertiesService.getBuild();
//        Map<String, Object> commit = gitPropertiesService.getCommit();
        System.out.println("commit:" + resp.getData());
    }

}
