package test;


import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/12 10:20
 * Description: echatimbroker
 **/
public class RoomNumberTest {
    @Before
    public void init_test(){

    }

    @Test
    public void _0_test_pinyin_ok() {
        int i = 0;
        while (i++ < 100){
            System.out.println("gen room number:" + genRoomNumber(8));
        }
    }


    private Long genRoomNumber(int length){
        StringBuilder number = new StringBuilder();
        int index = 0;
        do {
            int n = 0;
            // 第一位不能为0
            if(index == 0){
                while ((n = new Random().nextInt(9)) == 0);
            }
            else {
                n = new Random().nextInt(9);
            }
            number.append(n);
            index++;
        }while (number.length() < length);
        return Long.parseLong(number.toString());
    }
}
