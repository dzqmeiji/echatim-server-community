package test;


import com.utils.PinyinTool;
import org.junit.Before;
import org.junit.Test;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/12 10:20
 * Description: echatimbroker
 **/
public class PinyinTest {
    @Before
    public void init_test(){

    }

    @Test
    public void _0_test_pinyin_ok() {
        System.out.println(PinyinTool.toPinYin("李超"));
        System.out.println(PinyinTool.toPinYin("a李超"));
        System.out.println(PinyinTool.toPinYin("李超b"));
        System.out.println(PinyinTool.toPinYin("李c超b"));
        System.out.println(PinyinTool.toPinYin("李c超$"));
        System.out.println(PinyinTool.toPinYin("^&*李c超$"));
    }

}
