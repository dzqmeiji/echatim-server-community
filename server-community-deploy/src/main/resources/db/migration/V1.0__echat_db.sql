/*
 Navicat Premium Data Transfer

 Source Server         : local-mysql8.0
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : echat

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 30/06/2021 16:09:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for apply_record
-- ----------------------------
DROP TABLE IF EXISTS `apply_record`;
CREATE TABLE `apply_record`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '群ID',
  `from_id` bigint(0) NULL DEFAULT NULL COMMENT '请求来源id',
  `to_id` bigint(0) NULL DEFAULT NULL COMMENT '目标来源id',
  `attach_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '附言',
  `source` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '来源 ADD_FRIEND:添加朋友',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '申请状态(NORMAL:正常;REJECT:已拒绝;EXPIRE:超时未处理;FINISH:已完成;DEL: 已删除)',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_idx`(`from_id`) USING BTREE,
  INDEX `rid_idx`(`source`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '申请记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for file_info
-- ----------------------------
DROP TABLE IF EXISTS `file_info`;
CREATE TABLE `file_info`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP ID(废弃)',
  `server` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件服务器',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'OSS 对象名(组成: 路径+文件名)',
  `size` int(0) NULL DEFAULT NULL COMMENT '上传的文件大小(字节)',
  `mimeType` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上传文件的mime类型',
  `height` int(0) NULL DEFAULT NULL COMMENT '若是图片，这是图片的高度',
  `width` int(0) NULL DEFAULT NULL COMMENT '若是图片，这是图片的宽度',
  `originFileName` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上传的原始文件名',
  `fileType` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型: IMAGE, FILE, OTHER',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件下载地址',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '阿里云OSS文件信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for large_room
-- ----------------------------
DROP TABLE IF EXISTS `large_room`;
CREATE TABLE `large_room`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群名',
  `owner` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群主,用户auid',
  `announce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群公告',
  `introduce` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群简介',
  `invite_text` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邀请文字',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群LOGO,头像',
  `conf_joinmode` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群加入模式',
  `conf_beinvite` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群员被邀请方式',
  `conf_inviteother` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群员邀请权限(OWNER:仅群主;ALL:任何人也可以)',
  `conf_update` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群信息更新权限(OWNER:仅群主;ALL:任何人也可以)',
  `max_member` int(0) NULL DEFAULT NULL COMMENT '群最大成员数量',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件状态(NORMAL:正常;DEL: 已解散)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_idx`(`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for large_room_user
-- ----------------------------
DROP TABLE IF EXISTS `large_room_user`;
CREATE TABLE `large_room_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '群ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `rid` bigint(0) NULL DEFAULT NULL COMMENT '群ID',
  `auid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户auid',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_idx`(`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '组用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP ID(废弃)',
  `way` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息发送方式(P2P:点对点;P2G:点对多)',
  `from_user` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '来源用户auid',
  `to_target` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '目标用户auid/目标群uid',
  `type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'TEXT,IMAGE,AUDIO,VIDEO,POSITON,FILE,NOTIFY,CUSTOM',
  `body` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息体,json格式',
  `msg_option` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息选项设置',
  `at_users` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息要AT的用户,json 数组格式',
  `offline` tinyint(0) NULL DEFAULT NULL COMMENT '是否离线消息(1:是;0:否)',
  `offline_expire_date` datetime(0) NULL DEFAULT NULL COMMENT '离线消失超时时间(仅offline为1时有效)',
  `cancel` tinyint(0) NULL DEFAULT 0  COMMENT '消息是否已撤回',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '消息撤回时间',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息状态(NORMAL:正常;DEL: 已删除;CANCEL:已撤回)',
  `system_meno` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '系统备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `from_to_time_idx`(`from_user`, `to_target`, `create_time`) USING BTREE,
  INDEX `time_idx`(`create_time`) USING BTREE,
  INDEX `uid_idx`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2563541 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '普通消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message_stable_offline
-- ----------------------------
DROP TABLE IF EXISTS `message_stable_offline`;
CREATE TABLE `message_stable_offline`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `mid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息UID',
  `rid` bigint(0) NULL DEFAULT NULL COMMENT '群ID',
  `way` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息发送方式(P2P:点对点;P2G:点对多)',
  `from_user` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息来源用户auid',
  `to_user` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息目标用户auid',
  `body` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '待发送的消息正文(冗余)',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息的发送状态(WAIT:待发送, OFFLINE:离线中, SEND:发送完成)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_idx`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2819969 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '不丢失的发送消息/离线消息管理表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` bigint NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `room_number` bigint NULL DEFAULT NULL COMMENT '群号',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群名',
  `owner` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群主,用户auid',
  `announce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群公告',
  `introduce` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群简介',
  `invite_text` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邀请文字',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群LOGO,头像',
  `conf_joinmode` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群加入模式',
  `conf_beinvite` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群员被邀请方式',
  `conf_inviteother` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群员邀请权限(OWNER:仅群主;ALL:任何人也可以)',
  `conf_update` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群信息更新权限(OWNER:仅群主;ALL:任何人也可以)',
  `max_member` int NULL DEFAULT NULL COMMENT '群最大成员数量',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件状态(NORMAL:正常;DEL: 已解散)',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `sdk_id` bigint NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  `muted` int NULL DEFAULT NULL COMMENT '群是否设置了全员禁言(1:是 0:否)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_idx`(`uid` ASC) USING BTREE,
  UNIQUE INDEX `number_idx`(`room_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for room_user
-- ----------------------------
DROP TABLE IF EXISTS `room_user`;
CREATE TABLE `room_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '群ID',
  `uid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'UUID格式的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `rid` bigint(0) NULL DEFAULT NULL COMMENT '群ID',
  `auid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户auid',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '群员状态(NORMAL:正常;DEL: 已移除)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_idx`(`uid`) USING BTREE,
  INDEX `rid_idx`(`rid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 180 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '组用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sdk_app
-- ----------------------------
DROP TABLE IF EXISTS `sdk_app`;
CREATE TABLE `sdk_app`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'UUID格式的ID',
  `owner` bigint(0) NULL DEFAULT NULL COMMENT 'APP 拥有者, sdk_user id',
  `app_key` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'app_key:识别是那个APP',
  `level` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'app 的等级(TEST: 使用版; BASE: 基础版; PROFESSIONAL: 专业版)',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'APP名字',
  `business_type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '行业类型',
  `business_platform` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)',
  `introduce` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '简介',
  `app_secret` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'app_secret. 用于1. 用户后台API 请求',
  `client_secret` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'client_secret.用于1.文件服务器; 2. embed app',
  `limit_connection` tinyint(0) NULL DEFAULT NULL COMMENT '限制的连接数',
  `allow_use_day` tinyint(0) NULL DEFAULT NULL COMMENT 'app 允许使用时间(天)',
  `expire_date` datetime(0) NULL DEFAULT NULL COMMENT 'app 过期时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态(NORMAL:正常;DEL: 已删除)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '平台的APP管理SDK' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sdk_license
-- ----------------------------
DROP TABLE IF EXISTS `sdk_license`;
CREATE TABLE `sdk_license`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录用户',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录密码',
  `info` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '机器信息',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '证书过期时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '机器证书' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sdk_user
-- ----------------------------
DROP TABLE IF EXISTS `sdk_user`;
CREATE TABLE `sdk_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录名',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电子邮件',
  `phone` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机号码',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '密码(md5加盐)',
  `role` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '角色(ADMIN:管理员; SUPER_ADMIN: 超级管理员)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_document
-- ----------------------------
DROP TABLE IF EXISTS `system_document`;
CREATE TABLE `system_document`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP ID(废弃)',
  `category` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文档的业务分类',
  `type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'URL:链接方式;TEXT:文本方式',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '文本标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '文本正文',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文档图标',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文档链接地址',
  `pass_user_info` int(0)  NULL DEFAULT NULL COMMENT '当type=URL时, 是否传递用户jwt等用户信息 1:是 0:否',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `from_to_time_idx`(`category`, `create_time`) USING BTREE,
  INDEX `time_idx`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2563398 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统文档表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_message
-- ----------------------------
DROP TABLE IF EXISTS `system_message`;
CREATE TABLE `system_message`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP ID(废弃)',
  `to_target` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '目标用户auid',
  `type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'TEXT',
  `category` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '业务分类',
  `body` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息体,json格式',
  `ex` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '业务附加消息,json格式',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '消息状态(NORMAL:正常)',
  `message_read` tinyint(1) NULL DEFAULT 0 COMMENT '是否已读(1:已读; 0:未读)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `from_to_time_idx`(`to_target`, `create_time`) USING BTREE,
  INDEX `time_idx`(`create_time`) USING BTREE,
  INDEX `uid_idx`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2563410 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `auid` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'APP用户账号ID(与客户系统对接的用户ID)',
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '账号昵称',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户头像',
  `token` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户登录token',
  `token_expire_date` datetime(0) NULL DEFAULT NULL COMMENT '用户登录token 过期时间',
  `sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户签名',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户email',
  `birth` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户生日',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户mobile',
  `gender` tinyint(0) NULL DEFAULT NULL COMMENT '用户性别，0表示未知，1表示男，2女表示女',
  `ex` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户扩展字段',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户状态(NORMAL:正常, DISABLE:封禁)',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `user_type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户类型(USER:普通用户;VISITOR:游客)',
  `business_platform` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)',
  `name_pinyin` char(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '账号昵称拼音',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auid_unique`(`auid`) USING BTREE,
  UNIQUE INDEX `uid_idx`(`uid`) USING BTREE,
  UNIQUE INDEX `mobile_idx`(`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1034 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_relation
-- ----------------------------
DROP TABLE IF EXISTS `user_relation`;
CREATE TABLE `user_relation`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '内部ID',
  `uid` bigint(0) NULL DEFAULT NULL COMMENT '唯一性带时间戳自增长的ID',
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `auid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '当前用户auid',
  `target_auid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关系用户auid',
  `type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关系(FRIEND:好友)',
  `apply_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '请求添加好友时的请求文字',
  `alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注名',
  `blacklist` tinyint(0) NULL DEFAULT 0 COMMENT '是否拉黑(1:是;0:否)',
  `forbid` tinyint(0) NULL DEFAULT 0 COMMENT '是否禁言(1:是;0:否)',
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态(NORMAL:正常;DEL:删除)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for validated_code
-- ----------------------------
DROP TABLE IF EXISTS `validated_code`;
CREATE TABLE `validated_code`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mobile` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机号码',
  `business` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'REGISTER:注册; PASSWORD_RESET:密码重置',
  `code` char(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '4位数的验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '验证码过期时间',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态(NORMAL:正常)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '手机验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for visitor
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `app_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'SDK APP KEY(废弃)',
  `user_id` bigint(0) NULL DEFAULT NULL COMMENT '用户id',
  `device_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '设备id',
  `nickname` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '游客名称',
  `ip` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'IP地址',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '游客地区',
  `province` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '游客的省',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '游客状态(NORMAL:正常, DISABLE:封禁)',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '账号的过期时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `sdk_id` bigint(0) NULL DEFAULT 1 COMMENT 'sdk id, 用于租户隔离',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '游客表' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for room_user_admin
-- ----------------------------
DROP TABLE IF EXISTS `room_user_admin`;
CREATE TABLE `room_user_admin`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '群ID',
  `rid` bigint(0) NULL DEFAULT NULL COMMENT '群ID',
  `auid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '管理员用户auid',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rid_idx`(`rid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 249 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '组用户管理员表' ROW_FORMAT = Dynamic;



INSERT INTO `user`(`id`, `uid`, `app_key`, `auid`, `name`, `avatar`, `token`, `token_expire_date`, `sign`, `email`, `birth`, `mobile`, `gender`, `ex`, `status`, `create_time`, `user_type`, `business_platform`) VALUES (1, 1, 'TSDKTEST00001', 'admin', 'admin', 'http://5b0988e595225.cdn.sohucs.com/images/20180520/0715cc3657094f0d8e02f18d246c7aaf.jpeg', 'admin', NULL, 'admin的签名', 'kk@qq.com', '2019-02-20', '13500136011', NULL, NULL, NULL, '2020-02-21 14:15:38', 'USER', 'BASE');
INSERT INTO `user`(`id`, `uid`, `app_key`, `auid`, `name`, `avatar`, `token`, `token_expire_date`, `sign`, `email`, `birth`, `mobile`, `gender`, `ex`, `status`, `create_time`, `user_type`, `business_platform`) VALUES (2, 2, 'TSDKTEST00001', 'chat', 'chat', 'http://5b0988e595225.cdn.sohucs.com/images/20180520/38493510af9542649fa2eb833cc1f009.jpeg', 'chat', NULL, 'fate the best', 'cc@qq.com', '2019-02-20', '13500136012', NULL, NULL, NULL, '2021-07-07 14:15:38', 'USER', 'BASE');
INSERT INTO `sdk_app`(`id`, `uid`, `owner`, `app_key`, `level`, `name`, `business_type`, `business_platform`, `introduce`, `app_secret`, `client_secret`, `limit_connection`, `allow_use_day`, `expire_date`, `create_time`, `status`) VALUES (1, '1', NULL, 'TSDKTEST00001', NULL, 'TSDKTEST00001', NULL, NULL, NULL, 'TSDKTEST00001', NULL, 100, 100, '2032-09-30 10:32:35', '2020-04-14 10:32:41', 'NORMAL');

-- ----------------------------
-- Records of validated_code
-- ----------------------------
INSERT INTO `validated_code` VALUES (1, NULL, NULL, '8888', '2037-08-25 21:54:29', NULL, '2021-08-25 21:54:51', 1);


SET FOREIGN_KEY_CHECKS = 1;
