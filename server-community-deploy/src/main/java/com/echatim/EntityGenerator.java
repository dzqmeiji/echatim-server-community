package com.echatim;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.BeetlTemplateEngine;

/**
 *  实体类生成工具.
 *  参考: https://www.cnblogs.com/enenen/p/11704351.html
 * */
public class EntityGenerator {
    //核心类，所有操作配置都围绕该类展开。
    private static final AutoGenerator mpg = new AutoGenerator();
    //获取项目目录
    private static final String projectPath = System.getProperty("user.dir");
    //项目基础包
    private static final String Package = "com.echatim";
    //子模块包名，最终生成的是类似  com.echatim.entity 这样的
    private static final String ModuleName = ""; // 不用写, 默认就是 entity

    public static void main(String[] args) {
        //数据库连接配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/echat?useSSL=false");
        dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("root");
        mpg.setDataSource(dataSourceConfig);
        //公用的一些配置，一看就懂的就不加注释了。
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(projectPath + "/src/main/java");
        globalConfig.setAuthor("sun");
        //日期类型的字段使用哪个类型，默认是 java8的 日期类型，此处改为 java.util.date
        globalConfig.setDateType(DateType.ONLY_DATE);
        //是否覆盖 已存在文件，默认 false 不覆盖
        globalConfig.setFileOverride(true);
        //mapper.xml 是否生成 ResultMap，默认 false 不生成
        globalConfig.setBaseResultMap(false);
        //mapper.xml 是否生成 ColumnList，默认 false 不生成
        globalConfig.setBaseColumnList(false);
        //生成的实体类名字，增加前后缀的
//        globalConfig.setEntityName("%sEntity");
        //是否生成完成后打开资源管理器
        globalConfig.setOpen(false);
        mpg.setGlobalConfig(globalConfig);

        PackageConfig pc = new PackageConfig();
        pc.setParent(Package);
        pc.setModuleName(ModuleName);
        mpg.setPackageInfo(pc);

        StrategyConfig strategy = new StrategyConfig();
        //支持正则表达式，字符串数组。 和 Exclude 二选一
        //  此处使用正则生成时，提示有些bug。
        //  被正则过滤的表会提示 “^dev_.*$” 表不存在，其实需要生成代码的表已经正常生成完毕了。
        //strategy.setInclude("^dev_.*$");
        //  不需要生成的表
        //  strategy.setExclude("sequence");
        //此处配置为 下划线转驼峰命名
        strategy.setNaming(NamingStrategy.underline_to_camel);
        //生成的字段 是否添加注解，默认false
        strategy.setEntityTableFieldAnnotationEnable(true);
        //表前缀，配置后 生成的的代码都会把前缀去掉
//        strategy.setTablePrefix("dev_");
        //实体类的基础父类。没有可以不配置。
//        strategy.setSuperEntityClass("com.mz.mzservice.entity.BaseEntity");
        //这里本来以为配置上 生成的实体类就没有父类的属性了，但其实不是。
        //如何去掉父类属性，下面有说明。
//        strategy.setSuperEntityColumns("creater","createTime","editor","editTime","remark");
        //是否启用 Lombok
        strategy.setEntityLombokModel(true);
        //是否启用 builder 模式 例：new DevDevice().setDealerId("").setDeviceCode("");
        strategy.setEntityBuilderModel(true);
        mpg.setStrategy(strategy);

        TemplateConfig templateConfig = new TemplateConfig();
        //自定义模版
//        templateConfig.setController("/templates/btl/controller.java");
//        templateConfig.setEntity("/templates/btl/entity.java");
        // 关闭所有, 仅保留entity 的生成
        templateConfig.setController(null);
        templateConfig.setService(null);
        templateConfig.setServiceImpl(null);
        templateConfig.setMapper(null);
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);
        //使用beetl模版引擎
        mpg.setTemplateEngine(new BeetlTemplateEngine());
        mpg.execute();
    }
}
