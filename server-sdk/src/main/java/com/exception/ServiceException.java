package com.exception;

import com.broker.base.protocol.response.IRespErrorCode;

/**
 * @author Daizhiqiang <androidsimu@163.com>
 * create by 2020/5/8 9:24
 * Description: easyim
 **/
public class ServiceException extends RuntimeException{
    private IRespErrorCode respErrorCode;
    public ServiceException(IRespErrorCode code) {
        super(String.format("%s,错误码:%d", code.getMsg(), code.getCode()));
        this.respErrorCode = code;
    }
    public ServiceException(String msg) {
        super(String.format("%s,错误码:%d", msg, -1));
        this.respErrorCode = new IRespErrorCode(){

            @Override
            public long getCode() {
                return -1;
            }

            @Override
            public String getMsg() {
                return msg;
            }
        };
    }

    public IRespErrorCode getRespErrorCode() {
        return respErrorCode;
    }
}
