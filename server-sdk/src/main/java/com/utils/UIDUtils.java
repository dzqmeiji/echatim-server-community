package com.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/13 16:40
 * Description: echatim
 **/
public class UIDUtils {
    public static final String ID_MILLISECOND = "yyMMddHHmmssSSS"; //
    public static Long gen(){
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String dateStr = new SimpleDateFormat(ID_MILLISECOND).format(new Date());
        dateStr = dateStr.substring(1);
        return Long.parseLong(dateStr + String.format("%03d", new Random().nextInt(999)));
    }
}
