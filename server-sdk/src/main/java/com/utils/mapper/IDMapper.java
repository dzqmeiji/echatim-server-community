package com.utils.mapper;

import java.util.Objects;

public class IDMapper<T> {
    public Long id;
    public T obj;

    IDMapper(Long id, T obj) {
        this.id = id;
        this.obj = obj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IDMapper<?> idMapper = (IDMapper<?>) o;
        return this.id.equals(idMapper.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }
}
