package com.utils.mapper;

import java.util.Objects;

public class KeyMapper<T> {
    public String key;
    public T obj;

    KeyMapper(String key, T obj) {
        this.key = key;
        this.obj = obj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyMapper<?> idMapper = (KeyMapper<?>) o;
        return Objects.equals(key, idMapper.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.key);
    }
}
