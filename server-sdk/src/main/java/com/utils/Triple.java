package com.utils;


public final class Triple<S, T, R> {
    private S first;
    private T second;
    private R third;

    public Triple(S first, T second, R third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    /**
     * Creates a new {@link Triple} for the given elements.
     *
     * @param first  must not be {@literal null}.
     * @param second must not be {@literal null}.
     * @return
     */
    public static <S, T, R> Triple<S, T, R> of(S first, T second, R third) {
        return new Triple<>(first, second, third);
    }

    /**
     * Returns the first element of the {@link Triple}.
     *
     * @return
     */
    public S getFirst() {
        return first;
    }

    /**
     * Returns the second element of the {@link Triple}.
     *
     * @return
     */
    public T getSecond() {
        return second;
    }

    /**
     * Returns the second element of the {@link Triple}.
     *
     * @return
     */
    public R getThird() {
        return third;
    }

}

