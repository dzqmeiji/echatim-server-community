package com.commom;

import com.utils.Beans;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;

import java.util.Optional;

public class Config {
    /**
     *  获取配置常量, 优先级分别是: 系统参数 > 环境变量 > springboot 配置
     *
     * */
    @NonNull
    public static String get(@NonNull ApplicationContext context, String key){
          String systemValue = System.getProperty(key, "");
          String envValue = Optional.ofNullable(System.getenv(key)).orElse("");
          String springValue = Optional.ofNullable(context.getEnvironment().getProperty(key)).orElse("");

          return Beans.strNotEmpty(systemValue) ? systemValue : (Beans.strNotEmpty(envValue) ? envValue : springValue);
    }
}
