package com.commom;


public class LocalFileRoute {
    public static final String VERSION = "v1";

    public static class common {
        public static final String base = "/" + VERSION + "/" + "common";

        public static final String constants = "/constants.js";

        public static final class file {
            public static final String base = "/file";

            public static final String getUploadSign = base + "/get_upload_sign";
            public static final String upload = base + "/upload";
            public static final String download = base + "/download";
            public static final String uploadAsyncCallback = base + "/upload_async_callback";
            public static final String uploadSyncCallback = base + "/upload_sync_callback";
        }
    }

}

