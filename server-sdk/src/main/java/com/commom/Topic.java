package com.commom;


import java.util.Arrays;
import java.util.List;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 9:28
 * Description: echatim
 **/
public class Topic {
    public static final String TOPIC_PREFIX = "topic.";
    public static final String HTTP_API_PREFIX = "/v1/";

    // 客户端上传错误信息
    public static class ERROR{
        public static final String name = TOPIC_PREFIX +"error";
        public static final String base_uri = HTTP_API_PREFIX +"error";
        public static class METHOD{

        }
    }

    public static class CONNECTION{
        public static final String name = TOPIC_PREFIX +"connection";
//        public static final String base_uri = "connection";
        public static final String base_uri = HTTP_API_PREFIX +"connection";
        public static class METHOD{
               public static final String AUTHORITY_REQUEST = "authority_request";
               public static final String HEARTBEAT = "heartbeat";
        }
    }

    public static class APP_USER{
        public static final String name = TOPIC_PREFIX +"user";
//        public static final String base_uri = "user";
        public static final String base_uri = HTTP_API_PREFIX +"user";
        public static class METHOD{
//            public static final String USER_ADD = "user/add";
            public static final String ADD = "add";
            public static final String UPDATE = "update";
            public static final String LIST = "list";
            public static final String GET = "get";
            public static final String UPDATE_TOKEN = "update_token";
            public static final String REFRESH_TOKEN = "refresh_token";
        }
    }

    // 通过服务器向客户端发送消息的TOPIC
    public static class APP_UPSTREAM_MESSAGE {
        public static final String name = TOPIC_PREFIX +"upstream_message";
        //        public static final String base_uri = "user";
        public static final String base_uri = HTTP_API_PREFIX +"upstream_message";
        public static class METHOD{
            public static final String SEND = "send";
            public static final String BATCH_SEND = "batch_send";
            public static final String SEND_ACK_TO_SERVER = "sendack_toserver";
            public static final String CANCEL_MESSAGE = "cancel_message"; // 撤回消息
        }
    }

    // 服务器向客户端发送消息的TOPIC
    public static class APP_DOWNSTREAM_MESSAGE {
        public static final String name = TOPIC_PREFIX +"downstream_message";
        //        public static final String base_uri = "user";
        public static final String base_uri = HTTP_API_PREFIX +"downstream_message";
        public static class METHOD{
            public static final String SEND_MESSAGE_TO_CLIENT = "sendmessage_toclient";
            public static final String SEND_EVENT_TO_CLIENT = "sendevent_toclient";
        }
    }


    public static class APP_CLIENT{
        public static final String name = TOPIC_PREFIX +"client";
        //        public static final String base_uri = "user";
        public static final String base_uri = HTTP_API_PREFIX +"client";
        public static class METHOD{
            public static final String LIST = "list";// 列出已登录的客户端(clientId -> auid)
            public static final String USER_ONLINE = "user_online";// 列出用户的在线状态
            public static final String ADD_ONLINE_MONITOR = "add_online_monitor";// 设置用户的在线监听
            public static final String REMOVE_ONLINE_MONITOR = "remove_online_monitor";// 移除用户的在线监听
        }
    }


    public static class APP_ROOM {
        public static final String name = TOPIC_PREFIX +"room";
        //        public static final String base_uri = "user";
        public static final String base_uri = HTTP_API_PREFIX +"room";
        public static class METHOD{
            public static final String ADD = "add"; // 添加群
            public static final String UPDATE = "update"; // 更新群信息
            public static final String LIST = "list"; // 列出群信息
            public static final String DEL = "del"; // 删除群
            public static final String GET = "get"; // 获取单个群信息
            public static final String LIST_JOIN = "list_join"; // 列出当前用户加入的群
            public static final String LIST_MEMBER = "list_member"; // 列出群信息/群成员
            public static final String ADD_USER = "add_user"; // 添加群用户
            public static final String DEL_USER = "del_user"; // 删除群用户
            public static final String SEARCH = "search"; // 搜索群(通过群号)
            public static final String JOIN = "join"; // 加入群
            public static final String EXIT = "exit"; // 退出群
            public static final String MUTED = "muted"; // 设置群全员禁言
            public static final String ADD_ROOM_ADMIN = "add_room_admin"; // 添加群管理员
            public static final String REMOVE_ROOM_ADMIN = "remove_room_admin"; // 移除群管理员
            public static final String LIST_ROOM_ADMIN = "list_room_admin"; // 群管理员列表
            public static final String LIST_ROOM_MANAGED = "list_room_managed"; // 获取当前用户管理的群列表
            public static final String CANCEL_ROOM_MESSAGE = "cancel_room_message"; // 移除群成员的消息

            public static final String APPLY_RECORD = "apply_record";// 显示自己的入群申请记录
            public static final String ADMIN_APPLY_RECORD = "admin_apply_record";// 群管理员显示用户入群申请记录
            public static final String AGREE_ROOM = "agree_room";// 同意用户加入群
            public static final String REJECT_ROOM = "reject_room";// 拒绝用户加入群
        }
    }


    public static class APP_USER_RELATION{
        public static final String name = TOPIC_PREFIX +"user_relation";
        public static final String base_uri = HTTP_API_PREFIX +"user_relation";
        public static class METHOD{
            public static final String ADD_FRIEND = "add_friend"; // 添加好友
            public static final String DEL_FRIEND = "del_friend"; // 删除好友
            public static final String LIST_FRIENDS = "list_friends"; // 列出好友列表
            public static final String GET_FRIEND = "get_friend"; // 获取好友
            public static final String MODIFY_BLACKLIST_FORBID = "add_blacklist";// 添加/移除黑名单, 添加/移除黑禁言
            public static final String LIST_BLACKLIST_FORBID = "list_blacklists"; // 列出黑名单, 禁言列表
            public static final String MODIFY_ALIAS = "modify_alias";// 修改IM好友别名

            public static final String APPLY_RECORD = "apply_record";// 显示陌生人好友申请记录
            public static final String MY_APPLY_RECORD = "my_apply_record";// 显示我的好友申请记录
            public static final String AGREE_FRIEND = "agree_friend";// 同意陌生人为好友
            public static final String REJECT_FRIEND = "reject_friend";// 拒绝陌生人为好友
        }
    }


    // 通过服务器向客户端发送消息的TOPIC
    public static class APP_HISTORY_MESSAGE {
        public static final String name = TOPIC_PREFIX +"history";
        public static final String base_uri = HTTP_API_PREFIX +"history";
        public static class METHOD{
            public static final String LIST_MESSAGE = "list_message"; // 列出历史消息
            public static final String LIST_SESSION = "list_session"; // 列出用户历史会话列表
            public static final String LIST_ROOM_RECENTLY = "list_room_recently"; // 列出用户加入的群最新一条记录
        }
    }


    public static class APP_ADMIN_LOGIN{
        public static final String name = TOPIC_PREFIX +"admin_login";
        public static final String base_uri = HTTP_API_PREFIX +"admin_login";
        public static class METHOD{
            public static final String SDK_LOGIN = "sdk_login";
            public static final String SDK_USER_LOGIN = "sdk_user_login";
        }
    }


    // 系统信息
    public static class APP_SYSTEM {
        public static final String name = TOPIC_PREFIX +"system";
        public static final String base_uri = HTTP_API_PREFIX +"system";
        public static class METHOD{
            public static final String INFO = "info"; // 后台系统信息
        }
    }

    // 系统文档
    public static class APP_SYSTEM_DOCUMENT {
        public static final String name = TOPIC_PREFIX +"system_document";
        public static final String base_uri = HTTP_API_PREFIX +"system_document";
        public static class METHOD{
            public static final String LIST_DOCUMENT = "list_document"; // 后台系统文档
        }
    }


    // 所有的topic
    public static List<String> listenTopics = Arrays.asList(
            ERROR.name,
            CONNECTION.name,
            APP_USER.name,
            APP_UPSTREAM_MESSAGE.name,
//            APP_DOWNSTREAM_MESSAGE.name,
            APP_CLIENT.name,
            APP_ROOM.name,
            APP_USER_RELATION.name,
            APP_HISTORY_MESSAGE.name,
            APP_SYSTEM.name
    );
}
