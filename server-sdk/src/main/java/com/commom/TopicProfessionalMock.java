package com.commom;

import java.util.Arrays;
import java.util.List;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 16:43
 * Description: easyim
 **/
public class TopicProfessionalMock {
    public static final String TOPIC_PREFIX = "topic_professional.";
    public static final String HTTP_API_PREFIX = "/v1/";


    public static class APP_SOCIAL_USER_AUTH{
        public static final String name = TOPIC_PREFIX +"social_user_auth";
        public static final String base_uri = HTTP_API_PREFIX +"social_user_auth";
        public static class METHOD{
            public static final String USER_REGISTER = "user_register"; // 用户注册
            public static final String LOGIN = "login"; // 用户登入
            public static final String LOGOUT = "logout"; // 用户登出
            public static final String VALIDATED_CODE = "validated_code";// 获取验证码
            public static final String MODIFY_PASSWORD = "modify_password";// 修改用户登录密码
            public static final String POLL_SOCKET_STATUS = "poll_socket_status"; // 探测socket.io的连接登录状态
            public static final String BIND_SOCKET = "bind_socket";// 绑定登录用户id到socket.io client id
        }
    }

    public static class APP_SOCIAL_USER{
        public static final String name = TOPIC_PREFIX +"social_user";
        public static final String base_uri = HTTP_API_PREFIX +"social_user";
        public static class METHOD{
            public static final String INFO = "info";// 获取用户的信息
            public static final String FULL_INFO = "full_info";// 获取用户的全部信息
            public static final String SEARCH = "search";// 通过手机号/auid搜索用户
            public static final String QR_CODE = "qr_code";// 获取用户二维码
            public static final String UPDATE = "update";// 更新用户信息
        }
    }

    // 所有的topic, socket.io 服务会监听以下的TOPIC, 若新增了TOPIC, 必须设置增加到以下列表才会被socket.io服务管理
    public static List<String> listenTopics = Arrays.asList(
            TopicProfessionalMock.APP_SOCIAL_USER_AUTH.name,
            TopicProfessionalMock.APP_SOCIAL_USER.name
    );
}
