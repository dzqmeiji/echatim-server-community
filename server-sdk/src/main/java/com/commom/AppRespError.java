package com.commom;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.broker.base.protocol.response.IRespErrorCode;

/**
 *  Resp App 错误吗范围: [-2,-31];  [-201, -800]
 * */
public enum AppRespError implements IRespErrorCode, IErrorCode {
    ERROR(-1, "错误"),
    SERVICE_ERROR(-2, "服务器内部错误"),
    SERVICE_NOT_READY(-3, "服务器未启动"),
    REQUEST_VERIFY_ERROR(-4, "请求参数校验错误"),
    SDK_USER_LOGIN(-201, "登录错误, 用户名或密码错误"),
    SDK_LOGIN(-202, "登录错误, SDK KEY 或 SDK SECRET 错误"),
    CLIENT_LOGIN(-203, "登录错误, auid 或 token 错误"),
    UNKNOWN_TOKEN_TYPE(-204, "无法识别数字签名类型"),
    TOKEN_INVALID(-205, "非法的数字签名"),
    NO_TOKEN(-206, "没有数字签名"),
    AUTHORIZATION_FAILED(-207, "认证失败"),
    NO_PERMISSION(-208, "没有访问此API的权限"),
    CLIENT_OFFLINE(-209, "该客户端已下线，请重新登录"),

    SDK_KEY_NOT_EXIST(-224, "SDK KEY不存在"),
    SDK_KEY_EXPIRE(-225, "SDK KEY已超过使用期"),
    SDK_KEY_INVALID(-226, "SDK KEY失效"),

    UNKNOWN_MESSAGE_WAY(-230, "消息way必须是P2P,P2R,P2LR 其中之一"),
    FROM_AUID_NOT_EXIST(-231, "发送用户不存在"),
    TO_AUID_NOT_EXIST(-232, "目标用户不存在"),
    ROOM_NOT_EXIST(-233, "IM群不存在"),
    ROOM_MEMBER_NOT_EXIST(-234, "以下的群员不存在"),
    ROOM_NOT_FOUND(-235, "无法找到群"),
    ROOM_OWNER_NOT_MASTER(-236, "owner不是群主"),
    RELATION_TYPE(-237, "type必须为ADD,APPLY,AGREE,REJECT 其中之一"),
    RELATION_NOT_EXIST(-238, "好友关系不存在"),
    AUID_NOT_EXIST(-239, "用户不存在"),
    FRIEND_NOT_FOUND(-240, "无法找到好友信息"),
    BLACKLIST_TYPE(-241, "type必须为ADD_BLACKLIST,ADD_FORBID,REMOVE_BLACKLIST,REMOVE_FORBID 其中之一"),
    FRIEND_NOT_EXIST(-242, "好友不存在"),

    ERROR_API_NOT_EXIST(-404, "API不存在"),
    ERROR_END(-799, "错误");
    private Integer code = -1;
    private String msg = "";
    AppRespError(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
