package com.commom;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Daizhiqiang <androidsimu@163.com>
 * create by 2019/11/26 17:18
 * Description: education
 * 线性安全
 **/
public class NumberGenerator {
    // 多机器部署时需要替换成分布式锁
    public final static Object lock = new Object();
    // 多机器部署时需要替换成分布式锁
    public final static Object orderLock = new Object();
    public final static ConcurrentLimitedQueue<String> timeStampNumberCache = new ConcurrentLimitedQueue<>(100);


    public static String timestampGenerate(){
        String number = "";
        synchronized (orderLock){
            do{
                number = new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
            }while (timeStampNumberCache.contains(number));
            timeStampNumberCache.add(number);
        }
        return number;
    }


    /**
     *  功能: 按时间戳的方式生成唯一的文件名
     *  线性安全
     * */
    public static String fileNameGenerate(){
        return timestampGenerate();
    }


}
