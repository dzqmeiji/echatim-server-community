package com.commom;


/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 9:28
 * Description: echatim
 **/
public class PermissionConst {
    /**
     *  权限, 可为(ALL:全部; ADMIN: 仅管理员; SUPER_ADMIN: 仅超级管理员; CLIENT: 客户端)
     *  ALL/为空: 不限制, 不需要校验jwt
     *  CLIENT: 允许所有的客户端的请求, 最低权限(要求校验jwt);
     *  ADMIN: 允许客户的管理后台进行访问的接口(要求校验appKey, appSecret)
     *  SUPER_ADMIN: E-CHAT 的运维管理接口, 最高权限(要求校验超级登录账号jwt)
     * */
    public static final String ALL = "ALL";
    public static final String CLIENT = "CLIENT";
    public static final String ADMIN = "ADMIN";
    public static final String SUPER_ADMIN = "SUPER_ADMIN";
}
