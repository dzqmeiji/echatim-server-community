package com.commom;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrentLimitedQueue<E> extends ConcurrentLinkedQueue<E> {
    private int limit;

    public ConcurrentLimitedQueue(int limit) {
        this.limit = limit;
    }


    @Override
    public boolean add(E o) {
        super.add(o);
        while (size() > limit) { super.remove(); }
        return true;
    }

    public boolean isFull(){
        return this.size() >= limit;
    }
}
