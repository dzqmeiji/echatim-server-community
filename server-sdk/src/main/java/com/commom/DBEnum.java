package com.commom;

import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;


public interface DBEnum<K extends DBEnum>{

    @Nullable
    static Enum  of(Enum[] enums, String name){
        Optional<Enum> one = Stream.of(enums).filter(v->((Enum)v).name().equals(name)).findFirst();
        return one.orElse(null);
    }

    @Nullable
    static Enum  of(Enum[] enums, Integer code){
        Optional<Enum> one = Stream.of(enums).filter(v->((DBEnum)v).getCode().equals(code)).findFirst();
        return one.orElse(null);
    }

    @Nullable
    static String  ofMean(Enum[] enums, String name){
        Optional<Enum> one = Stream.of(enums).filter(v->((Enum)v).name().equals(name)).findFirst();
        if(one.isPresent()){
            return ((DBEnum)one.get()).getMean();
        }
        return "";
    }

    @Nullable
    static Integer  ofCode(Enum[] enums, String name){
        Optional<Enum> one = Stream.of(enums).filter(v->((Enum)v).name().equals(name)).findFirst();
        if(one.isPresent()){
            return ((DBEnum)one.get()).getCode();
        }
        return 0;
    }



    static <T extends Enum> Map<String, String> meanValues(Class<T> clz, Enum[] values){
        Map<String, String> maps = new HashMap<>();
        Stream.of(values).forEach(v->{
            Enum e = Enum.valueOf(clz, v.name());
            DBEnum dbEnum = (DBEnum)e;
            maps.put(e.name(), dbEnum.getMean());
        });
        return maps;
    }

    static <T extends Enum> Map<String, Integer> codeValues(Class<T> clz, Enum[] values){
        Map<String, Integer> maps = new HashMap<>();
        Stream.of(values).forEach(v->{
            Enum e = Enum.valueOf(clz, v.name());
            DBEnum extras = (DBEnum)e;
            maps.put(e.name(), extras.getCode());
        });
        return maps;
    }


    static <T extends Enum> Map<String, String> meanCodeValues(Class<T> clz, Enum[] values){
        Map<String, String> maps = new HashMap<>();
        Stream.of(values).forEach(v->{
            Enum e = Enum.valueOf(clz, v.name());
            DBEnum extras = (DBEnum)e;
            maps.put(e.name(), extras.getCode() + "_" + extras.getMean());
        });
        return maps;
    }




    default String getMean(){
        return "";
    }

    default Integer getCode(){
        return 0;
    }

}
