package com.commom;

import org.springframework.lang.Nullable;


public class DBConst {

    //  通用数据状态(用于逻辑删除)
    public enum EntryStatus implements DBEnum {
        NORMAL("正常"),
        DEL("已删除");
        private String mean = "";
        EntryStatus(String mean){
            this.mean = mean;
        }
        @Nullable
        public static EntryStatus of(String name){
            return (EntryStatus) DBEnum.of(EntryStatus.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }

    //  MessageStableOffline消息状态
    public enum MessageStableOfflineStatus implements DBEnum {
        OFFLINE("离线中"),
        SENT("已发送");
        private String mean = "";
        MessageStableOfflineStatus(String mean){
            this.mean = mean;
        }
        @Nullable
        public static MessageStableOfflineStatus of(String name){
            return (MessageStableOfflineStatus) DBEnum.of(MessageStableOfflineStatus.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }


    //  MessageStableOffline消息状态
    public enum MessageWay implements DBEnum {
        P2P("点对点消息"),
        P2R("点对群消息"),
        P2LR("点对超大群消息");
        private String mean = "";
        MessageWay(String mean){
            this.mean = mean;
        }
        @Nullable
        public static MessageWay of(String name){
            return (MessageWay) DBEnum.of(MessageWay.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }


    //  添加好友时的类型
    public enum AddFriendType implements DBEnum {
        ADD("直接加好友"),
        APPLY("请求加好友"),
        AGREE("同意加好友"),
        REJECT("拒绝加好友");
        private String mean = "";
        AddFriendType(String mean){
            this.mean = mean;
        }
        @Nullable
        public static AddFriendType of(String name){
            return (AddFriendType) DBEnum.of(AddFriendType.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }


    //  好友关系数据状态(用于逻辑删除)
    public enum AddFriendStatus implements DBEnum {
        NORMAL("正常"),
        DEL("已删除"),
        WAIT_AGREE("待同意"),
        REJECT("已拒绝");
        private String mean = "";
        AddFriendStatus(String mean){
            this.mean = mean;
        }
        @Nullable
        public static AddFriendStatus of(String name){
            return (AddFriendStatus) DBEnum.of(AddFriendStatus.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }

    //  添加黑名单/禁言时的类型
    public enum ModifyBlacklistType implements DBEnum {
        ADD_BLACKLIST("添加黑名单"),
        ADD_FORBID("添加禁言"),
        REMOVE_BLACKLIST("移除黑名单"),
        REMOVE_FORBID("移除禁言");
        private String mean = "";
        ModifyBlacklistType(String mean){
            this.mean = mean;
        }
        @Nullable
        public static ModifyBlacklistType of(String name){
            return (ModifyBlacklistType) DBEnum.of(ModifyBlacklistType.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }

    //  用户申请记录(添加好友, 加入群等)
    public enum ApplyRecordStatus implements DBEnum {
        NORMAL("正常"),
        REJECT("已拒绝"),
        EXPIRE("超时未处理"),
        FINISH("已完成"),
        DEL("已删除");
        private String mean = "";
        ApplyRecordStatus(String mean){
            this.mean = mean;
        }
        @Nullable
        public static ApplyRecordStatus of(String name){
            return (ApplyRecordStatus) DBEnum.of(ApplyRecordStatus.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }

    public enum ApplyRecordSource implements DBEnum {
        ADD_FRIEND("添加朋友"),
        SEARCH_ROOM("自己搜索群号加入"),
        APPEND_ROOM("经其它群员拉入群");
        private String mean = "";
        ApplyRecordSource(String mean){
            this.mean = mean;
        }
        @Nullable
        public static ApplyRecordSource of(String name){
            return (ApplyRecordSource) DBEnum.of(ApplyRecordSource.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }
    // 系统消息分类
    public enum SystemMessageCategory implements DBEnum {
        ADD_FRIEND("直接添加朋友"),
        APPLY_FRIEND("申请添加朋友"),
        APPLY_FRIEND_AGREE("同意申请添加朋友"),
        CREATE_ROOM("创建群"),
        JOIN_ROOM("加入群"),
        APPLY_ROOM("申请加入群"),
        APPLY_ROOM_PASS("申请加入群完毕"),
        EXIT_ROOM("退出群"),
        REMOVE_FROM_ROOM("被移除出群"),
        DELETE_ROOM("解散群"),
        DELETE_FRIEND("删除好友"),
        CANCEL_MESSAGE("撤销消息"),
        CANCEL_ROOM_MESSAGE("撤销群消息");
        private String mean = "";
        SystemMessageCategory(String mean){
            this.mean = mean;
        }
        @Nullable
        public static SystemMessageCategory of(String name){
            return (SystemMessageCategory) DBEnum.of(SystemMessageCategory.values(), name);
        }

        @Override
        public String getMean() {
            return this.mean;
        }
    }
}
