package com.commom;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.broker.base.protocol.response.IRespErrorCode;

/**
 *  Resp File 错误吗范围: -801, -900
 * */
public enum LocalFileRespError implements IRespErrorCode, IErrorCode {
    SYNC_VERIFY_ERROR(-801, "同步回调校验失败"),
    FETCH_UPLOAD_TOKEN(-802, "获取上传凭证失败"),
    NOT_UPLOAD_FILE(-803, "没有上传文件"),
    SIGNATURE_EXPIRE_OR_CACHE_EXPIRE(-804, "上传文件的signature已过期, 或上传缓存已失效"),
    SAVE_UPLOAD_FILE(-805, "保存上传文件失败"),
    ERROR2(-802, "文件服务器错误");
    private Integer code = -1;
    private String msg = "";
    LocalFileRespError(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}