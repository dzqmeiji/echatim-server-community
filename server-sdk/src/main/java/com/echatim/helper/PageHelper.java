package com.echatim.helper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.echatim.form.PageForm;
import com.utils.Beans;
import com.utils.Streams;
import lombok.extern.slf4j.Slf4j;

import java.util.function.BiConsumer;


@Slf4j
public class PageHelper {
    // 将page 内的成员类型从这种类型转成另一种类型
    public static <K,V> IPage<V> coverTo(IPage<K> pages, Class<V> objClz){
        IPage<V> targetPages = new Page<>(pages.getCurrent(), pages.getSize(), pages.getTotal());
        targetPages.setRecords(Streams.map(pages.getRecords(), v-> Beans.copy(v, objClz)));
        return targetPages;
    }
    // 将page 内的成员类型从这种类型转成另一种类型
    public static <K,V> IPage<V> coverTo(IPage<K> pages, Class<V> objClz, BiConsumer<K, V> consumer){
        IPage<V> targetPages = new Page<>(pages.getCurrent(), pages.getSize(), pages.getTotal());
        targetPages.setRecords(Streams.map(pages.getRecords(), v-> {
            V data = Beans.copy(v, objClz);
            consumer.accept(v, data);
            return data;
        }));
        return targetPages;
    }
    // 从 form 转成Page
    public static <K,V extends PageForm> Page<K> from(V form){
        return new Page<>(form.getPage(), form.getPageSize());
    }

}
