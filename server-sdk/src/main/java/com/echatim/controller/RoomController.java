package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.dto.*;
import com.echatim.form.*;
import com.echatim.service.app.RoomService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 群相关业务
 */
@Api(value = "IM群", tags = "IM群")
@RestController
@RequestMapping(Topic.APP_ROOM.base_uri)
public class RoomController extends ApiController {
    @Autowired
    private RoomService roomService;

    /**
     * APP添加IM群
     */
    @ApiOperation(value = "IM群-添加群")
    @SysLog("IM群-添加群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.ADD)
    public Resp<Long> addRoom(@RequestBody @Valid RoomAddForm addForm) {
        return roomService.add(addForm);
    }

    /**
     * APP更新IM群
     */
    @ApiOperation(value = "IM群-更新群")
    @SysLog("IM群-更新群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.UPDATE)
    public Resp<String> updateRoom(@RequestBody @Valid RoomUpdateForm updateForm) {
        return roomService.updateRoom(updateForm);
    }


    /**
     * APP获取IM群
     */
    @ApiOperation(value = "IM群-获取群")
    @SysLog("IM群-获取群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.LIST)
    public Resp<List<RoomListDTO>> listRooms(@RequestBody @Valid RoomListForm roomListForm) {
        return roomService.listRoom(roomListForm);
    }

    /**
     * APP获取单个IM群信息
     */
    @ApiOperation(value = "IM群-获取单个群")
    @SysLog("IM群-获取单个群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.GET)
    public Resp<RoomGetDTO> getRoom(@RequestBody @Valid RoomGetForm getForm) {
        return roomService.getRoom(getForm);
    }

    /**
     * APP获取当前用户加入的群
     */
    @ApiOperation(value = "IM群-获取当前用户加入的群")
    @SysLog("IM群-获取当前用户加入的群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.LIST_JOIN)
    public Resp<List<RoomListDTO>> listRoomsJoin(@RequestBody @Valid RoomListJoinForm roomListJoinForm) {
        return roomService.listRoomJoin(roomListJoinForm);
    }

    /**
     * APP获取IM群/IM群成员
     */
    @ApiOperation(value = "IM群-获取IM群/IM群成员")
    @SysLog("IM群-获取IM群/IM群成员")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.LIST_MEMBER)
    public Resp<List<RoomDetailListDTO>> listRoomDetail(@RequestBody @Valid RoomListForm roomListForm) {
        return roomService.listRoomDetail(roomListForm);
    }


    /**
     * APP IM群添加成员
     */
    @ApiOperation(value = "IM群-添加成员")
    @SysLog("IM群-添加成员")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.ADD_USER)
    public Resp<String> groupAddUser(@RequestBody @Valid RoomAddUserForm addUserForm) {
        return roomService.addRoomUser(addUserForm);
    }

    /**
     * APP IM群移除成员
     */
    @ApiOperation(value = "IM群-移除成员")
    @SysLog("IM群-移除成员")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.DEL_USER)
    public Resp<String> groupDelUser(@RequestBody @Valid RoomDelUserForm delUserForm) {
        return roomService.delRoomUser(delUserForm);
    }

    /**
     * APP IM 删除群/解散群
     */
    @ApiOperation(value = "IM群-删除群/解散群")
    @SysLog("IM群-删除群/解散群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.DEL)
    public Resp<String> groupDel(@RequestBody @Valid RoomDelForm delForm) {
        return roomService.delRoom(delForm);
    }

    /**
     * APP 搜索群(通过name)
     */
    @ApiOperation(value = "IM群-搜索群(通过name)")
    @SysLog("IM群-搜索群(通过name)")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.SEARCH)
    public Resp<List<RoomListDTO>> roomSearch(@RequestBody @Valid RoomSearchForm form) {
        return roomService.roomSearch(form);
    }

    /**
     * APP 加入群
     */
    @ApiOperation(value = "IM群-加入群")
    @SysLog("IM群-加入群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.JOIN)
    public Resp<Void> roomJoin(@RequestBody @Valid RoomJoinForm form) {
        return roomService.roomJoin(form);
    }

    /**
     * APP 退出群
     */
    @ApiOperation(value = "IM群-退出群")
    @SysLog("IM群-退出群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.EXIT)
    public Resp<Void> roomExit(@RequestBody @Valid RoomExitUserForm form) {
        return roomService.exitRoomUser(form);
    }


    /**
     * APP 设置禁言
     */
    @ApiOperation(value = "IM群-设置禁言")
    @SysLog("IM群-设置禁言")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.MUTED)
    public Resp<Void> roomMuted(@RequestBody @Valid RoomMutedForm form) {
        return roomService.roomMuted(form);
    }

    /**
     * APP 添加群管理员
     */
    @ApiOperation(value = "IM群-添加群管理员")
    @SysLog("IM群-添加群管理员")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.ADD_ROOM_ADMIN)
    public Resp<Void> addRoomAdmin(@RequestBody @Valid RoomAddAdminForm form) {
        return roomService.addRoomAdmin(form);
    }

    /**
     * APP 移除群管理员
     */
    @ApiOperation(value = "IM群-移除群管理员")
    @SysLog("IM群-移除群管理员")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.REMOVE_ROOM_ADMIN)
    public Resp<Void> removeRoomAdmin(@RequestBody @Valid RoomDelAdminForm form) {
        return roomService.removeRoomAdmin(form);
    }

    /**
     * APP 移除群成员的消息
     */
    @ApiOperation(value = "IM群-移除群成员的消息")
    @SysLog("IM群-移除群成员的消息")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.CANCEL_ROOM_MESSAGE)
    public Resp<Integer> cancelRoomUserMessage(@RequestBody @Valid RoomMessageCancelForm form) {
        return roomService.cancelRoomUserMessage(form);
    }


    /**
     * APP 群管理员列表
     */
    @ApiOperation(value = "IM群-群管理员列表")
    @SysLog("IM群-群管理员列表")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.LIST_ROOM_ADMIN)
    public Resp<List<RoomAdminListDTO>> listRoomAdmin(@RequestBody @Valid RoomListAdminForm form) {
        return roomService.listRoomAdmin(form);
    }


    /**
     * APP 获取当前用户管理的群列表
     */
    @ApiOperation(value = "IM群-获取当前用户管理的群列表")
    @SysLog("IM群-获取当前用户管理的群列表")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/" + Topic.APP_ROOM.METHOD.LIST_ROOM_MANAGED)
    public Resp<List<Long>> listRoomManaged(@RequestBody @Valid RoomListManagedForm form) {
        return roomService.listRoomManaged(form);
    }


    /**
     * 显示用户入群申请记录
     */
    @ApiOperation(value = "IM群-显示用户入群申请记录")
    @SysLog("IM群-显示用户入群申请记录")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_ROOM.METHOD.APPLY_RECORD)
    public Resp<List<UserApplyRecordsDTO>> applyRecord(@RequestBody @Valid RoomApplyRecordsForm form) {
        return roomService.applyRecord(form);
    }


    /**
     * 群管理员显示的用户入群申请记录(可以同意, 拒绝)
     */
    @ApiOperation(value = "IM群-群管理员显示的用户入群申请记录")
    @SysLog("IM群-群管理员显示的用户入群申请记录")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_ROOM.METHOD.ADMIN_APPLY_RECORD)
    public Resp<List<UserApplyRecordsDTO>> adminApplyRecord(@RequestBody @Valid RoomApplyRecordsForm form) {
        return roomService.adminApplyRecord(form);
    }


    /**
     * 同意用户入群
     */
    @ApiOperation(value = "IM群-同意用户入群")
    @SysLog("IM群-同意用户入群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_ROOM.METHOD.AGREE_ROOM)
    public Resp<Void> agreeRoom(@RequestBody @Valid RoomAgreeForm form) {
        return roomService.agreeRoom(form);
    }


    /**
     * 拒绝用户入群
     */
    @ApiOperation(value = "IM群-拒绝用户入群")
    @SysLog("IM群-拒绝用户入群")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_ROOM.METHOD.REJECT_ROOM)
    public Resp<Void> rejectRoom(@RequestBody @Valid RoomRejectForm form) {
        return roomService.rejectRoom(form);
    }

}
