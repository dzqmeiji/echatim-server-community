package com.echatim.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.Topic;
import com.echatim.entity.SystemDocument;
import com.echatim.form.SystemDocumentListForm;
import com.echatim.service.app.SystemDocumentService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


/**
 * IM用户相关业务
 */
@Api(value = "系统文档", tags = "系统文档")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(Topic.APP_SYSTEM_DOCUMENT.base_uri)
public class SystemDocumentController extends ApiController {
    @Autowired
    private SystemDocumentService systemDocumentService;

    /**
     * A系统信息-系统文档
     */
    @ApiOperation(value = "系统信息-系统文档")
    @SysLog("系统信息-系统文档")
    @PostMapping("/"+ Topic.APP_SYSTEM_DOCUMENT.METHOD.LIST_DOCUMENT)
    public Resp<List<SystemDocument>> listDocument(@RequestBody @Valid SystemDocumentListForm form) {
        return systemDocumentService.listDocument(form);
    }



}
