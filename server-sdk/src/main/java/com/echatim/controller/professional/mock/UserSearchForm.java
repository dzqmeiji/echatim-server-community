package com.echatim.controller.professional.mock;

import com.echatim.form.SocketHideProtocolMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserSearchForm extends SocketHideProtocolMessage {
    @NotBlank(message = "用户手机号/auid不能为空")
    private String loginAccount = ""; // 用户手机号/auid
}
