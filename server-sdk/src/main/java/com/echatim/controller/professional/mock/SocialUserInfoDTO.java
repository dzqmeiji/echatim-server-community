package com.echatim.controller.professional.mock;

import com.echatim.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/15 11:16
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class SocialUserInfoDTO extends User {

}
