package com.echatim.controller.professional.mock;

import com.echatim.form.SocketHideProtocolMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserClientRegisterForm extends SocketHideProtocolMessage {
    private String deviceId = ""; // 客户端的所在的设备id(唯一)

    private String auid = ""; // 用户auid
    private String token = ""; // 用户token(密码)
    private String type = ""; // 用户类型(USER: 普通用户; VISITOR: 游客)
    private Long expire = 0L; // 用户账号的有效期(单位:秒, 仅type=VISITOR 时有效)
    private String ip = ""; // 游客ip
    private String address = ""; // 游客ip区域
    private String province = ""; // 游客ip区域省
    // 以下是用户信息的可选字段
    private String name = ""; // 账号昵称
    private String avatar = ""; // 用户头像
    private String sign = ""; // 用户签名
    private String email = ""; // 用户email
    private String birth = ""; // 用户生日
    private String mobile = ""; // 用户mobile
//    @Range(min = 0, max = 2)
    private Integer gender = 0; // 用户性别，0表示未知，1表示男，2女表示女
    private String ex = ""; // 用户扩展字段

    private String userType = ""; // 用户类型(USER: 普通用户; VISITOR: 游客)
    private String businessPlatform = ""; // 业务平台(BASE:基础平台;SOCIAL:社交平台)
}
