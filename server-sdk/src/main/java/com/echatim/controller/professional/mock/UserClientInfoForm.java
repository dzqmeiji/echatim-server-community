package com.echatim.controller.professional.mock;

import com.echatim.form.SocketHideProtocolMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserClientInfoForm extends SocketHideProtocolMessage {
    private String appKey;

    @NotBlank(message = "用户auid 不能为空")
    private String auid = ""; // 用户auid
    @NotBlank(message = "用户类型 不能为空")
    private String userType = ""; // 用户类型(USER: 普通用户, VISITOR: 游客)
}
