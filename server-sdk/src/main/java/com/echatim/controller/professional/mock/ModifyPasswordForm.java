package com.echatim.controller.professional.mock;

import com.echatim.form.SocketHideProtocolMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ModifyPasswordForm extends SocketHideProtocolMessage {
    @NotBlank(message = "用户手机号码不能为空")
    private String mobile = ""; // 用户手机号码
    @NotBlank(message = "密码不能为空")
    private String password = ""; // 密码
    @NotBlank(message = "验证码不能为空")
    private String validatedCode = ""; // 验证码
    @NotNull(message = "验证码ID不能为空")
    private Integer validatedCodeId = 0; // 验证码ID
}
