package com.echatim.controller.professional.mock;

import com.broker.base.auth.UserJwt;
import com.broker.base.protocol.hook.UserLoginForm;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserClientLoginForm extends UserLoginForm {
    @ApiModelProperty(hidden = true)
    private String clientId = "";
    @ApiModelProperty(hidden = true)
    private String requestId = "";
    @ApiModelProperty(hidden = true)
    private Long timestamp = 0L;
    @ApiModelProperty(hidden = true)
    private String method = "";
    @ApiModelProperty(hidden = true)
    private String jwt = "";
    @ApiModelProperty(hidden = true)
    private Integer broadcastLoginEvent = 1;
    @ApiModelProperty(hidden = true)
    public UserJwt getUserJwt() {
        return super.getUserJwt();
    }

    @NotBlank(message = "appKey不能为空")
    private String appKey = ""; // appKey

    @ApiModelProperty("用户手机号/auid")
    @NotBlank(message = "用户手机号/auid不能为空")
    private String loginAccount = ""; // 用户手机号/auid

    @Length(min = 6, max = 20, message = "用户密码为6-20位")
    @NotBlank(message = "用户密码不能为空")
    private String password = ""; // 用户密码

}
