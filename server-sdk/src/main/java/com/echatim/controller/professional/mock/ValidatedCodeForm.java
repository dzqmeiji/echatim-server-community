package com.echatim.controller.professional.mock;

import com.echatim.form.SocketHideProtocolMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author kong <androidsimu@163.com>
 * create by 2020/10/12 17:29
 * Description: easyim
 **/

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ValidatedCodeForm extends SocketHideProtocolMessage {
    @Pattern(regexp = "[0-9]{11}", message = "不是有效的手机号码")
    @NotBlank(message = "手机号码不能为空")
    private String mobile = ""; // 用户auid
    @NotBlank(message = "业务不能为空")
    private String business = ""; // 业务 REGISTER:注册; PASSWORD_RESET:密码重置
}
