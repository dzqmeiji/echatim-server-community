package com.echatim.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.TopicProfessionalMock;
import com.echatim.controller.professional.mock.*;
import com.echatim.entity.User;
import com.echatim.service.app.UserService;
import com.echatim.syslog.annotation.SysLog;
import com.utils.Beans;
import com.utils.Streams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * API功能: 适配uniapp的部分用户API
 * 社交用户相关业务
 */
@Slf4j
@Api(value = "社交用户", tags = "社交用户")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(TopicProfessionalMock.APP_SOCIAL_USER_AUTH.base_uri)
public class SocialUserController extends ApiController {
    @Autowired
    private UserService userService;
    /**
     * 社交用户登入
     */
    @ApiOperation(value = "社交用户-社交用户登入")
    @SysLog("社交用户-社交用户登入")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER.METHOD.INFO)
    public Resp<SocialUserInfoDTO> info(@RequestBody @Valid UserClientInfoForm form) {
        User user = userService.lambdaQuery()
                .eq(User::getAuid, form.getAuid())
                .one();
        if(user != null){
            user.setToken("");
            SocialUserInfoDTO userInfoDTO = Beans.copy(user, SocialUserInfoDTO.class);
            return Resp.ok(userInfoDTO);
        }
        return Resp.failed("用户不存在, auid:" + form.getAuid());
    }

    /**
     * 社交用户登出
     */
    @ApiOperation(value = "社交用户-社交用户登入")
    @SysLog("社交用户-社交用户登入")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER.METHOD.FULL_INFO)
    public Resp<SocialUserInfoDTO> fullInfo(@RequestBody @Valid UserClientInfoForm form) {
        return this.info(form);
    }


    /**
     * 获取社交用户信息
     */
    @ApiOperation(value = "获取社交用户信息")
    @SysLog("获取社交用户信息")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER.METHOD.SEARCH)
    public Resp<List<SocialUserInfoDTO>> search(@RequestBody @Valid UserSearchForm form) {
        List<User> users = userService.lambdaQuery()
                .eq(isMobileNumber(form.getLoginAccount()), User::getMobile, form.getLoginAccount())
                .like(!isMobileNumber(form.getLoginAccount()), User::getAuid, form.getLoginAccount())
                .list();
        List<SocialUserInfoDTO> socialUserInfoDTOS = Streams.map(users, v->{
            SocialUserInfoDTO dto =  Beans.copy(v, SocialUserInfoDTO.class);
            dto.setToken("");
            return dto;
        });
        return Resp.ok(socialUserInfoDTOS);
    }
    private static final Pattern MOBILE_PATTERN = Pattern.compile("[0-9]{11}"); // 13800138000
    private boolean isMobileNumber(String mobile){
        Matcher m = MOBILE_PATTERN.matcher(mobile);
        return m.matches();
    }
    /**
     * 获取社交用户信息
     */
    @ApiOperation(value = "获取社交用户信息")
    @SysLog("获取社交用户信息")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER.METHOD.QR_CODE)
    public Resp<String> validatedCode(@RequestBody @Valid UserQRCodeForm form) {
        log.warn("该API仅专业版支持.");
        return Resp.ok("");
    }

    /**
     * 获取社交用户信息
     */
    @ApiOperation(value = "获取社交用户信息")
    @SysLog("获取社交用户信息")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER.METHOD.UPDATE)
    public Resp<Void> update(@RequestBody @Valid UserUpdateForm form) {
        log.warn("该API仅专业版支持.");
        return Resp.ok(null);
    }


}
