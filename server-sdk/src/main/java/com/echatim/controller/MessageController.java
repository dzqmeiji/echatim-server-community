package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.form.MessageCancelForm;
import com.echatim.form.MessageSendForm;
import com.echatim.service.app.MessageService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * IM用户相关业务
 */
@Api(value = "IM消息", tags = "IM消息")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(Topic.APP_UPSTREAM_MESSAGE.base_uri)
public class MessageController extends ApiController {
    @Autowired
    private MessageService messageService;

    /**
     * APP跟IM用户发送消息
     */
    @ApiOperation(value = "IM消息-发送消息")
    @SysLog("IM消息-发送消息")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_UPSTREAM_MESSAGE.METHOD.SEND)
    public Resp<String> sendMessage(@RequestBody @Valid MessageSendForm sendForm) {
        return messageService.send(sendForm);
    }

    /**
     * 个人撤回自己的消息
     */
    @ApiOperation(value = "IM消息-个人撤回自己的消息")
    @SysLog("IM消息-个人撤回自己的消息")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_UPSTREAM_MESSAGE.METHOD.CANCEL_MESSAGE)
    public Resp<Integer> cancelMessage(@RequestBody @Valid MessageCancelForm form) {
        return messageService.cancelMessage(form);
    }



}
