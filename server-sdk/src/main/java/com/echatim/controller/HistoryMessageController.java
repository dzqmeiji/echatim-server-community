package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.dto.HistorySessionDTO;
import com.echatim.dto.MessageListDTO;
import com.echatim.form.HistoryListForm;
import com.echatim.form.HistoryListSessionForm;
import com.echatim.form.HistoryRoomRecentlyForm;
import com.echatim.service.app.HistoryMessageService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * IM用户相关业务
 */
@Api(value = "历史消息", tags = "历史消息")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(Topic.APP_HISTORY_MESSAGE.base_uri)
public class HistoryMessageController extends ApiController {
    @Autowired
    private HistoryMessageService historyMessageService;
    /**
     * 获取IM用户历史消息
     */
    @ApiOperation(value = "历史消息-用户历史消息")
    @SysLog("历史消息-用户历史消息")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_HISTORY_MESSAGE.METHOD.LIST_MESSAGE)
    public Resp<List<MessageListDTO>> listMessage(@RequestBody @Valid HistoryListForm listForm) {
        return historyMessageService.listMessage(listForm);
    }

    /**
     * 获取IM用户会话列表
     */
    @ApiOperation(value = "历史消息-用户会话列表")
    @SysLog("历史消息-用户会话列表")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_HISTORY_MESSAGE.METHOD.LIST_SESSION)
    public Resp<List<HistorySessionDTO>> listSessions(@RequestBody @Valid HistoryListSessionForm listSessionForm) {
        return historyMessageService.listSessions(listSessionForm);
    }
    /**
     * 获取IM用户加入的群最新一条记录
     */
    @ApiOperation(value = "历史消息-用户加入的群最新一条记录")
    @SysLog("历史消息-用户加入的群最新一条记录")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_HISTORY_MESSAGE.METHOD.LIST_ROOM_RECENTLY)
    public Resp<List<MessageListDTO>> listRoomRecently(@RequestBody @Valid HistoryRoomRecentlyForm historyRoomRecentlyForm) {
        return historyMessageService.listRoomRecently(historyRoomRecentlyForm);
    }
}
