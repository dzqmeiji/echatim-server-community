package com.echatim.controller.fileserver;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 本地文件服务器 FileUploadRequestDTO
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */
@Accessors(chain = true)
@Data
public class FileUploadRequestDTO {
//    private String accessid = "";
//    private String policy = "";
    private String signature = "";
    private String dir = "";
    private String host = "";
    private Long expire = 0L;
    private String callback = "";
}
