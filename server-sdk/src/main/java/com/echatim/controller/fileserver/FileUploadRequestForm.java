package com.echatim.controller.fileserver;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 本地文件服务器 FileUploadRequestForm
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */
@ApiModel(description = "获取文件上传凭证")
@Accessors(chain = true)
@Data
public class FileUploadRequestForm {
    @ApiModelProperty("echatIMSDK key")
    @NotBlank(message = "sdkKey不能为空")
    private String sdkKey = ""; // echatIMSDK key
    @ApiModelProperty("文件分类,可为IMAGE, FILE, OTHER等, 会按该变量创建基础目录")
    private String fileType = ""; // IMAGE, FILE, OTHER
    @ApiModelProperty("上传的原始文件名")
    @NotBlank(message = "上传的文件名不能为空")
    private String fileName = ""; // 上传的文件名
}
