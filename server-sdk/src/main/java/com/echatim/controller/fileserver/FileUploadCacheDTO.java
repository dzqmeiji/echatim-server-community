package com.echatim.controller.fileserver;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 本地文件服务器上传缓存信息 FileUploadCacheDTO
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */
@Accessors(chain = true)
@Data
public class FileUploadCacheDTO {
    private String signature = "";
    private String dir = "";
    private String host = "";
    private Long expire = 0L;

    private Integer size = 0; // 大小(字节)
    private String sdkKey = ""; // echatIMSDK key
    private String fileType = ""; // IMAGE, FILE, OTHER
    private String fileName = ""; // 上传的文件名
}
