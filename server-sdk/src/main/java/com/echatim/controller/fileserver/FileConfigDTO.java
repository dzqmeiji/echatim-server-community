package com.echatim.controller.fileserver;


import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 本地文件服务器 AliOssConfigDTO
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */
@Accessors(chain = true)
@Data
public class FileConfigDTO {
    private String accessId = ""; // 请填写您的AccessKeyId。
    private String accessKeySecret = ""; // 请填写您的AccessKeySecret。
    private String endpoint = ""; // 请填写您的 endpoint。
    private String bucket = ""; // 请填写您的 bucketname 。
    private String host = ""; // host的格式为 bucketname.endpoint
    private String callbackBase = ""; // 回调域名
}


