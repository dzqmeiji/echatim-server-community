package com.echatim.controller.fileserver;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.TimeUnit;

public class FileSignatureCache {
    private static Cache<String, Object> cache = CacheBuilder.newBuilder()
            //设置cache的初始大小为10，要合理设置该值
            .initialCapacity(10)
            //设置并发数为5，即同一时间最多只能有5个线程往cache执行写入操作
            .concurrencyLevel(5)
            //设置写缓存后8秒钟过期
            .expireAfterWrite(1, TimeUnit.HOURS) // 1小时后失效
            //构建cache实例
            .build();

    private static FileSignatureCache _instance = new FileSignatureCache();
    public static FileSignatureCache instance(){
        return _instance;
    }

    public void putCache(String signature, FileUploadCacheDTO info){
        cache.put(signature, info);
    }

    public FileUploadCacheDTO getCache(String signature){
       return (FileUploadCacheDTO) cache.getIfPresent(signature);
    }
}
