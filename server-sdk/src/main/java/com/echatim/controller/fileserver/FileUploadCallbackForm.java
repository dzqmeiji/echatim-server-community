package com.echatim.controller.fileserver;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 本地文件回调数据
 *
 * @author Daizhiqiang
 * @date 2019/11/22 18:07
 */
@ApiModel(description = "本地文件回调数据")
@Accessors(chain = true)
@Data
public class FileUploadCallbackForm {
    @ApiModelProperty("OSS 对象名(组成: 路径+文件名)")
    @NotBlank(message = "oss 文件名不能为空")
    private String filename = ""; // OSS 对象名(组成: 路径+文件名)
    @ApiModelProperty("上传的文件大小(字节)")
    private String size = ""; // 上传的文件大小
    @ApiModelProperty("上传文件的mime类型")
    private String mimeType = ""; // 上传文件的mime类型
    @ApiModelProperty("若是图片，这是图片的高度")
    private String height = ""; // 若是图片，这是图片的高度
    @ApiModelProperty("若是图片，这是图片的宽度")
    private String width = ""; // 若是图片，这是图片的宽度
    @ApiModelProperty("上传的原始文件名")
    private String originFileName = ""; // 上传的原始文件名
    @ApiModelProperty("文件类型: IMAGE, FILE, OTHER")
    private String fileType = ""; // 文件类型: IMAGE, FILE, OTHER
}

