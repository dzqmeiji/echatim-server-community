package com.echatim.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.hook.UserLoginForm;
import com.broker.base.protocol.response.Resp;
import com.commom.Topic;
import com.echatim.service.app.LoginService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * IM用户登录
 */
@Api(value = "IM用户登录", tags = "IM用户登录")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(Topic.CONNECTION.base_uri)
public class LoginController extends ApiController {
    @Autowired
    private LoginService loginService;


    /**
     * user 使用auid, token 方式登录
     */
    @ApiOperation(value = "IM用户登录-登录")
    @SysLog("IM用户登录-登录")
    @PostMapping("/"+Topic.CONNECTION.METHOD.AUTHORITY_REQUEST)
    public Resp<String> sdkLogin(@RequestBody @Valid UserLoginForm userLoginForm) {
        return loginService.login(userLoginForm);
    }
}
