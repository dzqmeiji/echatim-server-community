package com.echatim.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.Topic;
import com.echatim.dto.ServerInfoDTO;
import com.echatim.service.app.GitPropertiesService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * IM用户相关业务
 */
@Api(value = "系统信息", tags = "系统信息")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(Topic.APP_SYSTEM.base_uri)
public class SystemController extends ApiController {
    @Autowired
    private GitPropertiesService gitPropertiesService;

    /**
     * A系统信息-获取消息
     */
    @ApiOperation(value = "系统信息-获取消息")
    @SysLog("系统信息-获取消息")
    @PostMapping("/"+ Topic.APP_SYSTEM.METHOD.INFO)
    public Resp<ServerInfoDTO> getInfo() {
        return gitPropertiesService.getInfo();
    }



}
