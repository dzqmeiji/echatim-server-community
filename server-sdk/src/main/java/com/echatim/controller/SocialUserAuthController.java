package com.echatim.controller;


import com.annotation.MethodFor;
import com.annotation.TopicFor;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.TopicProfessionalMock;
import com.echatim.controller.professional.mock.*;
import com.echatim.entity.User;
import com.echatim.service.app.LoginService;
import com.echatim.service.app.UserService;
import com.echatim.syslog.annotation.SysLog;
import com.utils.Beans;
import com.utils.PinyinTool;
import com.utils.UIDUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;

/**
 * API功能: 适配uniapp的部分登录API
 * 社交用户授权相关业务
 */
@Slf4j
@TopicFor(value = TopicProfessionalMock.APP_SOCIAL_USER_AUTH.name)
@Api(value = "社交用户授权", tags = "社交用户授权")
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@RestController
@RequestMapping(TopicProfessionalMock.APP_SOCIAL_USER_AUTH.base_uri)
public class SocialUserAuthController extends ApiController {
    @Autowired
    private UserService userService;
    @Autowired
    private LoginService loginService;
    /**
     * 社交用户注册
     */
    @ApiOperation(value = "社交用户授权-社交用户注册")
    @SysLog("社交用户授权-社交用户注册")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.USER_REGISTER)
    public Resp<String> userRegister(@RequestBody @Valid UserRegisterForm form) {
        User dbUser = userService.lambdaQuery().eq(User::getAuid, form.getMobile()).one();
        if(dbUser != null){
            return Resp.failed("该用户已注册, 请直接登录");
        }
        User user = Beans.copy(form, User.class)
                .setAuid(form.getMobile())
                .setToken(form.getPassword())
                .setName(form.getName())
                .setNamePinyin(PinyinTool.toPinYin(form.getName()))
                .setUserType("USER")
                .setBusinessPlatform("SOCIAL")
                .setUid(UIDUtils.gen())
                .setCreateTime(new Date());
        userService.save(user);
        return Resp.ok(user.getAuid());
    }


    /**
     * 社交用户登入
     */
    @ApiOperation(value = "社交用户授权-社交用户登入")
    @SysLog("社交用户授权-社交用户登入")
    @MethodFor(value = TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.LOGIN, consumer = UserClientLoginForm.class)
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.LOGIN)
    public Resp<String> login(@RequestBody @Valid UserClientLoginForm loginForm) {
        loginForm.setAuid(loginForm.getLoginAccount());
        loginForm.setToken(loginForm.getPassword());
        return loginService.authority(loginForm);
    }

    /**
     * 社交用户登出
     */
    @ApiOperation(value = "社交用户授权-社交用户登入")
    @SysLog("社交用户授权-社交用户登入")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.LOGOUT)
    public Resp<String> logout(@RequestBody @Valid UserClientLogoutForm logoutForm) {
        log.warn("该API仅专业版支持.");
        return Resp.ok("");
    }


    /**
     * 获取社交用户信息
     */
    @ApiOperation(value = "获取社交用户信息")
    @SysLog("获取社交用户信息")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.VALIDATED_CODE)
    public Resp<Long> validatedCode(@RequestBody @Valid ValidatedCodeForm form) {
        log.warn("该API仅专业版支持.");
        return Resp.ok(0L);
    }

    /**
     * 修改用户密码
     */
    @ApiOperation(value = "修改用户密码")
    @SysLog("修改用户密码")
    @PostMapping("/"+ TopicProfessionalMock.APP_SOCIAL_USER_AUTH.METHOD.MODIFY_PASSWORD)
    public Resp<Void> modifyPassword(@RequestBody @Valid ModifyPasswordForm form) {
        log.warn("该API仅专业版支持.");
        return Resp.ok(null);
    }


}
