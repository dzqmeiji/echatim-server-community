package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.dto.UserGetDTO;
import com.echatim.dto.UserListDTO;
import com.echatim.dto.UserRefreshTokenDTO;
import com.echatim.form.*;
import com.echatim.service.app.UserService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * IM用户相关业务
 */
@Api(value = "IM用户", tags = "IM用户")
@RestController
@RequestMapping(Topic.APP_USER.base_uri)
public class UserController extends ApiController {
    @Autowired
    private UserService userService;


    /**
     * APP添加IM用户
     */
    @ApiOperation(value = "IM用户-添加用户")
    @SysLog("IM用户-添加用户")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.ADD)
    public Resp<String> addUser(@RequestBody @Valid UserAddForm addForm) {
        return userService.add(addForm);
    }

    /**
     * APP更新IM用户
     */
    @ApiOperation(value = "IM用户-更新用户")
    @SysLog("IM用户-更新用户")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.UPDATE)
    public Resp<String> updateUser(@RequestBody @Valid UserUpdateForm updateForm) {
        return userService.updateUser(updateForm);
    }


    /**
     * APP获取IM用户
     */
    @ApiOperation(value = "IM用户-获取用户")
    @SysLog("IM用户-获取用户")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.LIST)
    public Resp<List<UserListDTO>> listUsers(@RequestBody @Valid UserListForm listForm) {
        return userService.listUser(listForm);
    }

    /**
     * APP获取单个IM用户
     */
    @ApiOperation(value = "IM用户-获取单个用户")
    @SysLog("IM用户-获取单个用户")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.GET)
    public Resp<UserGetDTO> listUsers(@RequestBody @Valid UserGetForm getForm) {
        return userService.getUser(getForm);
    }

    /**
     * APP更新用户token
     */
    @ApiOperation(value = "IM用户-更新用户token")
    @SysLog("IM用户-更新用户token")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.UPDATE_TOKEN)
    public Resp<String> updateUserToken(@RequestBody @Valid UserUpdateTokenForm userUpdateTokenForm) {
        return userService.updateToken(userUpdateTokenForm);
    }

    /**
     * APP刷新用户token
     */
    @ApiOperation(value = "IM用户-刷新用户token")
    @SysLog("IM用户-刷新用户token")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER.METHOD.REFRESH_TOKEN)
    public Resp<UserRefreshTokenDTO> refreshUserToken(@RequestBody @Valid UserRefreshTokenForm userRefreshTokenForm) {
        return userService.refreshToken(userRefreshTokenForm);
    }
}
