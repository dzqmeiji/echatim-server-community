package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.dto.ClientListDTO;
import com.echatim.dto.ClientUserOnlineDTO;
import com.echatim.form.ClientUserOnlineForm;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

/**
 * 客户端相关业务
 */
@Slf4j
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Api(value = "客户端监控", tags = "客户端监控")
@RestController
@RequestMapping(Topic.APP_CLIENT.base_uri)
public class ClientController extends ApiController {

    /**
     * APP获取已登录的IM客户端
     */
    @ApiOperation(value = "客户端监控-已登录的IM客户端")
    @SysLog("客户端监控-已登录的IM客户端")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_CLIENT.METHOD.LIST)
    public Resp<List<ClientListDTO>> listClients() {
        log.warn("该API仅专业版支持.");
        return Resp.ok(Collections.emptyList());
    }


    @ApiOperation(value = "客户端监控-在线客户端")
    @SysLog("客户端监控-在线客户端")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+ Topic.APP_CLIENT.METHOD.USER_ONLINE)
    public Resp<List<ClientUserOnlineDTO>> listClients(@RequestBody @Valid ClientUserOnlineForm onlineForm) {
        log.warn("该API仅专业版支持.");
        return Resp.ok(Collections.emptyList());
    }


}
