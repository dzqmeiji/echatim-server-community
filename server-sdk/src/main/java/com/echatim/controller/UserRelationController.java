package com.echatim.controller;


import com.annotation.Permission;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.broker.base.protocol.response.Resp;
import com.commom.PermissionConst;
import com.commom.Topic;
import com.echatim.dto.UserApplyRecordsDTO;
import com.echatim.dto.UserBlackForbidListDTO;
import com.echatim.dto.UserFriendGetDTO;
import com.echatim.dto.UserFriendListDTO;
import com.echatim.form.*;
import com.echatim.service.app.UserRelationService;
import com.echatim.syslog.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * IM用户相关业务
 */
@Api(value = "IM用户关系", tags = "IM用户关系")
@RestController
@RequestMapping(Topic.APP_USER_RELATION.base_uri)
public class UserRelationController extends ApiController {
    @Autowired
    private UserRelationService userRelationService;


    /**
     * 添加IM用户好友
     */
    @ApiOperation(value = "IM用户关系-添加好友")
    @SysLog("IM用户关系-添加好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.ADD_FRIEND)
    public Resp<String> addFriend(@RequestBody @Valid UserFriendAddForm addForm) {
        return userRelationService.addFriend(addForm);
    }

    /**
     * 删除IM用户好友
     */
    @ApiOperation(value = "IM用户关系-删除好友")
    @SysLog("IM用户关系-删除好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.DEL_FRIEND)
    public Resp<String> delFriend(@RequestBody @Valid UserFriendDelForm delForm) {
        return userRelationService.delFriend(delForm);
    }

    /**
     * 获取IM用户好友
     */
    @ApiOperation(value = "IM用户关系-获取好友")
    @SysLog("IM用户关系-获取好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.LIST_FRIENDS)
    public Resp<List<UserFriendListDTO>> listFriend(@RequestBody @Valid UserFriendListForm listForm) {
        return userRelationService.listFriend(listForm);
    }

    /**
     * 获取IM用户单个好友信息
     */
    @ApiOperation(value = "IM用户关系-获取单个好友")
    @SysLog("IM用户关系-获取单个好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.GET_FRIEND)
    public Resp<UserFriendGetDTO> getFriend(@RequestBody @Valid UserFriendGetForm getForm) {
        return userRelationService.getFriend(getForm);
    }


    /**
     * 修改IM用户好友黑名单, 禁言列表
     */
    @ApiOperation(value = "IM用户关系-修改好友黑名单/禁言列表")
    @SysLog("IM用户关系-修改好友黑名单/禁言列表")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.MODIFY_BLACKLIST_FORBID)
    public Resp<String> modifyBlacklistForbid(@RequestBody @Valid UserBlacklistModifyForm modifyForm) {
        return userRelationService.modifyBlacklistForbid(modifyForm);
    }

    /**
     * 获取IM黑名单,禁言名单
     */
    @ApiOperation(value = "IM用户关系-获取好友黑名单/禁言列表")
    @SysLog("IM用户关系-获取好友黑名单/禁言列表")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.LIST_BLACKLIST_FORBID)
    public Resp<UserBlackForbidListDTO> modifyBlacklistForbid(@RequestBody @Valid UserBlackForbidListForm listForm) {
        return userRelationService.listBlackForbid(listForm);
    }


    /**
     * 修改IM用户好友别名
     */
    @ApiOperation(value = "IM用户关系-修改好友别名")
    @SysLog("IM用户关系-修改好友别名")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.MODIFY_ALIAS)
    public Resp<String> modifyAlias(@RequestBody @Valid UserFriendAliasForm aliasForm) {
        return userRelationService.modifyAlias(aliasForm);
    }


    /**
     * 显示陌生人好友申请记录
     */
    @ApiOperation(value = "IM用户关系-显示陌生人好友申请记录")
    @SysLog("IM用户关系-显示陌生人好友申请记录")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.APPLY_RECORD)
    public Resp<List<UserApplyRecordsDTO>> applyRecord(@RequestBody @Valid UserApplyRecordsForm form) {
        return userRelationService.applyRecord(form);
    }


    /**
     * 显示我的好友申请记录
     */
    @ApiOperation(value = "IM用户关系-显示我的好友申请记录")
    @SysLog("IM用户关系-显示我的好友申请记录")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.MY_APPLY_RECORD)
    public Resp<List<UserApplyRecordsDTO>> myApplyRecord(@RequestBody @Valid UserApplyRecordsForm form) {
        return userRelationService.myApplyRecord(form);
    }

    /**
     * 同意陌生人为好友
     */
    @ApiOperation(value = "IM用户关系-同意陌生人为好友")
    @SysLog("IM用户关系-同意陌生人为好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.AGREE_FRIEND)
    public Resp<Void> agreeFriend(@RequestBody @Valid UserFriendAgreeForm form) {
        return userRelationService.agreeFriend(form);
    }


    /**
     * 拒绝陌生人为好友
     */
    @ApiOperation(value = "IM用户关系-拒绝陌生人为好友")
    @SysLog("IM用户关系-拒绝陌生人为好友")
    @Permission(value = PermissionConst.CLIENT)
    @PostMapping("/"+Topic.APP_USER_RELATION.METHOD.REJECT_FRIEND)
    public Resp<Void> rejectFriend(@RequestBody @Valid UserFriendRejectForm form) {
        return userRelationService.rejectFriend(form);
    }
}
