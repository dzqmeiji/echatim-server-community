package com.echatim.service.app;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.echatim.entity.SdkUser;
import com.echatim.mapper.SdkUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
@DS("master")
@Service
public class SdkUserService extends ServiceImpl<SdkUserMapper, SdkUser>  {
}
