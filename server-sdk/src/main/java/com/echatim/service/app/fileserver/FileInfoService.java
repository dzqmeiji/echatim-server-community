package com.echatim.service.app.fileserver;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.broker.base.protocol.response.Resp;
import com.echatim.controller.fileserver.FileUploadCallbackForm;
import com.echatim.entity.FileInfo;
import com.echatim.mapper.FileInfoMapper;
import com.utils.Beans;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Service("fileInfoService")
public class FileInfoService extends ServiceImpl<FileInfoMapper, FileInfo> {
    public Resp<String> saveOrUpdateFileInfo(FileUploadCallbackForm form, String server, String appKey, String downUrl){
        FileInfo fileInfo = this.lambdaQuery().eq(FileInfo::getUrl, downUrl).one();
        // 存在即更新数据
        if(Beans.isNotNull(fileInfo)){
            this.lambdaUpdate()
//                    .set(Beans.strNotEmpty(form.getFilename()), FileInfo::getFilename, form.getFilename())
                    .set(Beans.strNotEmpty(form.getSize()), FileInfo::getSize, form.getSize())
                    .set(Beans.strNotEmpty(form.getMimeType()), FileInfo::getMimeType, form.getMimeType())
                    .set(Beans.strNotEmpty(form.getHeight()), FileInfo::getHeight, form.getHeight())
                    .set(Beans.strNotEmpty(form.getWidth()), FileInfo::getWidth, form.getWidth())
                    .set(Beans.strNotEmpty(form.getOriginFileName()), FileInfo::getOriginFileName, form.getOriginFileName())
                    .set(Beans.strNotEmpty(form.getFileType()), FileInfo::getFileType, form.getFileType())
                    .eq(FileInfo::getUrl, downUrl)
                    .update();
        }
        // 不存在即插入数据
        else {
            this.save(Beans.copy(form, FileInfo.class)
                    .setServer(server)
                    .setAppKey(appKey)
                    .setWidth(Beans.strNotEmpty(form.getWidth()) ? Integer.parseInt(form.getWidth()) : 0)
                    .setHeight(Beans.strNotEmpty(form.getHeight()) ? Integer.parseInt(form.getHeight()) : 0)
                    .setSize(Beans.strNotEmpty(form.getSize()) ? Integer.parseInt(form.getSize()) : 0)
                    .setUrl(downUrl));
        }

        return Resp.ok("成功");
    }
}
