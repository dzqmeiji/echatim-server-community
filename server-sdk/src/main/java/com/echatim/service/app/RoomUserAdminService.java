package com.echatim.service.app;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.echatim.entity.Room;
import com.echatim.entity.RoomUserAdmin;
import com.echatim.entity.User;
import com.echatim.mapper.RoomMapper;
import com.echatim.mapper.RoomUserAdminMapper;
import com.echatim.mapper.UserMapper;
import com.utils.Streams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
//@TopicFor(value = Topic.APP_UPSTREAM_MESSAGE.name)
@DS("master")
@Service
public class RoomUserAdminService extends ServiceImpl<RoomUserAdminMapper, RoomUserAdmin>  {
    @Autowired
    private RoomMapper roomMapper;
    @Autowired
    private UserMapper userMapper;

    // 判断指定的用户是否指定群的管理员
    public boolean checkRoomAdmin(String auid, Long rid){
        Room room = Optional.ofNullable(roomMapper.selectById(rid)).orElse(new Room());
        int cnt = this.lambdaQuery()
                .eq(RoomUserAdmin::getAuid, auid)
                .eq(RoomUserAdmin::getRid, rid)
                .count();
        return cnt > 0 || auid.equals(room.getOwner());
    }

    public List<User> listRoomAdmin(Long rid){
        Room room = Optional.ofNullable(roomMapper.selectById(rid)).orElse(new Room());
        List<RoomUserAdmin> roomUserAdmins = this.lambdaQuery()
                .eq(RoomUserAdmin::getRid, rid)
                .list();
        List<String> auids = new ArrayList<String>(){{
            addAll(Streams.keys(roomUserAdmins, v->v.getAuid()));
            add(room.getOwner());
        }};
        return userMapper.selectList(Wrappers.<User>lambdaQuery().in(User::getAuid, auids));
    }
}
