package com.echatim.service.app;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.broker.base.protocol.response.Resp;
import com.echatim.dto.ServerInfoDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@DS("master")
@Component
public class GitPropertiesService {
    private static Properties confProperties = null;
    @Value("${echatim.sdk.auth-type}")
    private String authType;

    public Resp<ServerInfoDTO> getInfo(){
        if(confProperties == null){
            confProperties = new Properties();
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("git.properties");
            try {
                if(inputStream != null){
                    confProperties.load(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String branch = confProperties.getProperty("git.branch");
        String gitIdAbbrev = confProperties.getProperty("git.commit.id.abbrev");
        String gitTime = confProperties.getProperty("git.commit.time");
        return Resp.ok(new ServerInfoDTO()
                .setBranch(branch)
                .setGitIdAbbrev(gitIdAbbrev)
                .setGitTime(gitTime)
                .setAuthType(authType));
    }
}
