package com.echatim.service.app;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.broker.base.protocol.response.Resp;
import com.echatim.entity.SystemDocument;
import com.echatim.form.SystemDocumentListForm;
import com.echatim.mapper.SystemDocumentMapper;
import com.utils.Streams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
@DS("master")
@Service
public class SystemDocumentService extends ServiceImpl<SystemDocumentMapper, SystemDocument>  {
    public Resp<List<SystemDocument>> listDocument(SystemDocumentListForm form){
        return Resp.ok(this.lambdaQuery().in(Streams.isNotEmpty(form.getCategories()), SystemDocument::getCategory, form.getCategories())
                .orderByAsc(SystemDocument::getId).list());
    }
}
