package com.echatim.service.app;

import com.annotation.MethodFor;
import com.annotation.TopicFor;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.broker.base.protocol.response.Resp;
import com.commom.Topic;
import com.echatim.dto.UserGetDTO;
import com.echatim.dto.UserListDTO;
import com.echatim.dto.UserRefreshTokenDTO;
import com.echatim.entity.User;
import com.echatim.form.*;
import com.echatim.mapper.UserMapper;
import com.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@TopicFor(value = Topic.APP_USER.name)
@DS("master")
@Service
//public class UsersService  {
public class UserService extends ServiceImpl<UserMapper, User>  {
    @MethodFor(value = Topic.APP_USER.METHOD.ADD, consumer = UserAddForm.class)
    public Resp<String> add(@Valid UserAddForm userAddForm){
        log.info(" add: " + Beans.json(userAddForm));
//        log.info(" get userName: " + userAddForm.getUserJwt().getName());
        this.save(Beans.copy(userAddForm, User.class)
                .setUid(UIDUtils.gen())
                .setNamePinyin(PinyinTool.toPinYin(userAddForm.getName()))
                .setUserType(Beans.strNotEmpty(userAddForm.getUserType()) ? userAddForm.getUserType() : "USER")
                .setBusinessPlatform(Beans.strNotEmpty(userAddForm.getBusinessPlatform()) ? userAddForm.getBusinessPlatform() : "BASE")
                .setCreateTime(new Date()));
        return Resp.ok("添加成功");
    }



    public Resp<String> updateUser(UserUpdateForm updateForm) {
        this.lambdaUpdate()
                .set(Beans.strNotEmpty(updateForm.getName()), User::getName, updateForm.getName())
                .set(Beans.strNotEmpty(updateForm.getAvatar()), User::getAvatar, updateForm.getAvatar())
                .set(Beans.strNotEmpty(updateForm.getSign()), User::getSign, updateForm.getSign())
                .set(Beans.strNotEmpty(updateForm.getEmail()), User::getEmail, updateForm.getEmail())
                .set(Beans.strNotEmpty(updateForm.getBirth()), User::getBirth, updateForm.getBirth())
                .set(Beans.strNotEmpty(updateForm.getMobile()), User::getMobile, updateForm.getMobile())
                .set(User::getGender, updateForm.getGender())
                .set(Beans.strNotEmpty(updateForm.getEx()), User::getEx, updateForm.getEx())
                // where
                .eq(User::getAuid, updateForm.getAuid())
                .update();

        return Resp.ok("更新成功");
    }

    public Resp<String> updateToken(UserUpdateTokenForm updateTokenForm) {
        this.lambdaUpdate()
                .set(User::getToken, updateTokenForm.getToken())
                // where
                .eq(User::getAuid, updateTokenForm.getAuid())
                .update();

        return Resp.ok("更新成功");
    }

    public Resp<UserRefreshTokenDTO> refreshToken(UserRefreshTokenForm refreshTokenForm) {
        String token = UUIDUtils.gen();
        this.lambdaUpdate()
                .set(User::getToken, token)
                // where
                .eq(User::getAuid, refreshTokenForm.getAuid())
                .update();

        return Resp.ok(new UserRefreshTokenDTO()
                .setAuid(refreshTokenForm.getAuid())
                .setToken(token));
    }

    public Resp<List<UserListDTO>> listUser(UserListForm auids) {
        List<User> users = this.lambdaQuery().in(User::getAuid, auids.getAuids()).list();
        return Resp.ok(Streams.map(users, v->Beans.copy(v, UserListDTO.class)));
    }

    public Resp<UserGetDTO> getUser(UserGetForm form) {
        User user = this.lambdaQuery().eq(User::getAuid, form.getUserAuid()).one();
        return Resp.ok(user == null ? new UserGetDTO() : Beans.copy(user, UserGetDTO.class));
    }

    public User getUserInfo(String auid) {
        return Optional.ofNullable(this.lambdaQuery().eq(User::getAuid, auid).one()).orElse(new User());
    }
}
