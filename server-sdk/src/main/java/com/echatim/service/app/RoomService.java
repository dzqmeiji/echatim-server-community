package com.echatim.service.app;

import com.alibaba.fastjson.JSONObject;
import com.annotation.MethodFor;
import com.annotation.TopicFor;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.broker.base.protocol.response.Resp;
import com.commom.AppRespError;
import com.commom.DBConst;
import com.commom.Topic;
import com.echatim.broker.clusterbroadcast.SystemMessageClusterBroadcastEvent;
import com.echatim.broker.localbroadcast.SystemMessageLocalBroadcastEvent;
import com.echatim.dto.*;
import com.echatim.entity.*;
import com.echatim.form.*;
import com.echatim.mapper.MessageMapper;
import com.echatim.mapper.RoomMapper;
import com.exception.ServiceException;
import com.utils.Beans;
import com.utils.Streams;
import com.utils.UIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
@TopicFor(value = Topic.APP_ROOM.name)
@DS("master")
@Service
public class RoomService extends ServiceImpl<RoomMapper, Room>  {
    @Autowired
    private UserService userService;
    @Autowired
    private RoomUserService roomUserService;
    @Autowired
    private SystemMessageService systemMessageService;
    @Autowired
    private RoomUserAdminService roomUserAdminService;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private ApplyRecordService applyRecordService;

    @Transactional
    @MethodFor(value = Topic.APP_ROOM.METHOD.ADD, consumer = RoomAddForm.class)
    public Resp<Long> add(@Valid RoomAddForm roomAddForm){
        log.info(" add: " + Beans.json(roomAddForm));
        // TODO: get sdkid from jwt
        String appKey = "";
        Set<String> members = new HashSet<>(roomAddForm.getMembers());
        members.add(roomAddForm.getOwner()); // 添加群主
        // 检测 members, owner 的有效性
        List<User> users = userService.lambdaQuery().in(User::getAuid, members).list();
        List<String> memberNotExist = new ArrayList<>();
        members.forEach(v->{
            if(!Streams.map(users, vv->vv.getAuid()).contains(v)){
                memberNotExist.add(v);
            }
        });
        if(!memberNotExist.isEmpty()){
            return Resp.failed("以下的群员不存在:" + Beans.json(memberNotExist), AppRespError.ROOM_MEMBER_NOT_EXIST.getCode());
        }
        // 添加群
        Room room = Beans.copy(roomAddForm, Room.class)
                .setUid(UIDUtils.gen())
                .setRoomNumber(genRoomNumber(8)) // 群号为8位
                .setAppKey(appKey)
                .setCreateTime(new Date());
        this.save(room);
        // 添加群员
        addRoomUserToDB(appKey, room.getId(), new ArrayList<>(members));
        // 保存系统消息
        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v->v.getAuid());
        User ownerUser = userMapByAuid.getOrDefault(roomAddForm.getOwner(), new User());
        List<Long> systemMessageIds = new ArrayList<>();
        users.forEach(user -> {
            Long systemMessageId = systemMessageService.saveSystemMessage(ownerUser, user, DBConst.SystemMessageCategory.CREATE_ROOM.name(), String.format("%s创建了群:%s", ownerUser.getName(), room.getRoomNumber()), new JSONObject());
            systemMessageIds.add(systemMessageId);
        });
        systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));

        return Resp.ok(room.getId());
    }

    private Long genRoomNumber(int length){
        StringBuilder number = new StringBuilder();
        int index = 0;
        do {
            int n = 0;
            // 第一位不能为0
            if(index == 0){
                while ((n = new Random().nextInt(9)) == 0);
            }
            else {
                n = new Random().nextInt(9);
            }
            number.append(n);
            index++;
        }while (number.length() < length);
        return Long.parseLong(number.toString());
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.UPDATE, consumer = RoomUpdateForm.class)
    public Resp<String> updateRoom(RoomUpdateForm updateForm) {
        this.lambdaUpdate()
                .set(Beans.strNotEmpty(updateForm.getName()), Room::getName, updateForm.getName())
//                .set(Beans.strNotEmpty(updateForm.getOwner()), Room::getOwner, updateForm.getOwner())
                .set(Beans.strNotEmpty(updateForm.getAnnounce()), Room::getAnnounce, updateForm.getAnnounce())
                .set(Beans.strNotEmpty(updateForm.getIntroduce()), Room::getIntroduce, updateForm.getIntroduce())
                .set(Beans.strNotEmpty(updateForm.getAvatar()), Room::getAvatar, updateForm.getAvatar())
                .set(Beans.strNotEmpty(updateForm.getConfJoinmode()), Room::getConfJoinmode, updateForm.getConfJoinmode())
                .set(Beans.strNotEmpty(updateForm.getConfBeinvite()), Room::getConfJoinmode, updateForm.getConfBeinvite())
                .set(Beans.strNotEmpty(updateForm.getConfInviteother()), Room::getConfInviteother, updateForm.getConfInviteother())
                .set(Beans.strNotEmpty(updateForm.getConfUpdate()), Room::getConfUpdate, updateForm.getConfUpdate())
                .set(Room::getMaxMember, updateForm.getMaxMember())
                // where
                .eq(Room::getId, updateForm.getRid())
                .update();

        return Resp.ok("更新成功");
    }
    @MethodFor(value = Topic.APP_ROOM.METHOD.LIST, consumer = RoomListForm.class)
    public Resp<List<RoomListDTO>> listRoom(RoomListForm roomListForm) {
        // and status <> 'DEL' or status is NULL
        List<Room> users = this.lambdaQuery().in(Room::getId, roomListForm.getRids()).and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus)).list();
        return Resp.ok(Streams.map(users, v->Beans.copy(v, RoomListDTO.class).setRid(v.getId())));
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.GET, consumer = RoomGetForm.class)
    public Resp<RoomGetDTO> getRoom(RoomGetForm getForm) {
        Room room = this.lambdaQuery().eq(Room::getId, getForm.getRid()).and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus)).one();
        if(room == null){
            return Resp.failed("无法找到群,rid = " + getForm.getRid(), AppRespError.ROOM_NOT_FOUND.getCode());
        }
        RoomGetDTO dto = Beans.copy(room, RoomGetDTO.class);
        dto.setRid(room.getId());
        return Resp.ok(dto);
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.ADD_USER, consumer = RoomAddUserForm.class)
    public Resp<String> addRoomUser(RoomAddUserForm form) {
        if(form.getNeedAgree() == 0){
           if(!roomUserAdminService.checkRoomAdmin(form.getAuid(), form.getRid())){
               return Resp.failed(String.format("%s不是群:%d 的群管理员, 无法直接添加用户", form.getAuid(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
            }
            String sdkId = "";
            addRoomUserToDB(sdkId, form.getRid(), form.getMembers());
        }
        // 需要群管理员同意
        else {
            if(Beans.strEmpty(form.getInviteText()) && Beans.strNotEmpty(form.getAuid())){
                Room room = Optional.ofNullable(this.getById(form.getRid())).orElseThrow(()->new ServiceException("room not exist, rid = " + form.getRid()));
                form.setInviteText(String.format("经%s邀请入群:%s", userService.getUserInfo(form.getAuid()).getName(), room.getRoomNumber()));
            }
            createApplyRoomRecords(form.getRid(), form.getMembers(), form.getInviteText());
        }
        return Resp.ok("成功.");
    }

    /**
     * 创建群申请记录
     * @param rid: 群id
     * @param members: 群成员
     * @param inviteText: 入群请求文字
     * @return
     */
    private void createApplyRoomRecords(Long rid, List<String> members, String inviteText){
        Room room = Optional.ofNullable(this.getById(rid)).orElseThrow(()->new ServiceException("room not exist, rid = " + rid));
        List<String> auids = new ArrayList<String>(){{
            addAll(members);
            add(room.getOwner());
        }};
        List<User> users = userService.lambdaQuery().in(User::getAuid, auids).list();
        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v->v.getAuid());
        // 之前已存在的数据
        List<RoomUser> roomUsers = roomUserService.lambdaQuery()
                .eq(RoomUser::getRid, rid)
                .in(RoomUser::getAuid, members)
                .list();
        if(!roomUsers.isEmpty()){
            throw new ServiceException("用户已在该群中.");
        }
        members.forEach(member->{
            User originUser = userMapByAuid.getOrDefault(member, new User());
            ApplyRecord applyRecord = new ApplyRecord();
            applyRecord.setFromId(userMapByAuid.getOrDefault(member, new User()).getId());
            applyRecord.setToId(rid); // 这里设置为群id
            applyRecord.setAttachText(inviteText);
            applyRecord.setStatus(DBConst.ApplyRecordStatus.NORMAL.name());
            applyRecord.setSource(DBConst.ApplyRecordSource.APPEND_ROOM.name());
            applyRecord.setCreateTime(new Date());
            ApplyRecord record = applyRecordService.lambdaQuery()
                    .eq(ApplyRecord::getStatus, DBConst.ApplyRecordStatus.NORMAL.name())
                    .eq(ApplyRecord::getFromId, applyRecord.getFromId())
                    .eq(ApplyRecord::getToId, applyRecord.getToId())
                    .one();
            if(record != null){
                throw new ServiceException(String.format("该用户:%s 已存在入群申请记录.", originUser.getName()));
            }
            applyRecordService.save(applyRecord);
            // 加入系统消息(对群主, 群管理员都记录一条系统消息 = APPLY_ROOM)
            List<Long> systemMessageIds = new ArrayList<>();
            List<User> roomAdmins = roomUserAdminService.listRoomAdmin(rid);
            JSONObject ex = JSONObject.parseObject(Beans.json(applyRecord));
            roomAdmins.forEach(roomAdmin -> {
                systemMessageIds.add(systemMessageService.saveSystemMessage(originUser, roomAdmin, DBConst.SystemMessageCategory.APPLY_ROOM.name(), String.format("%s申请加入群:%s", originUser.getName(), room.getRoomNumber()), ex));
            });
            systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
        });
    }

    private void addRoomUserToDB(String sdkId, Long roomId, List<String> members){
        // 添加群员(修复: 群员会重复添加)
        List<RoomUser> dbRoomUsers = roomUserService.lambdaQuery()
                .eq(RoomUser::getRid, roomId)
                .in(RoomUser::getAuid, members)
                .list();
        List<RoomUser> roomUsers = members.stream()
                .map(v->{
                    return new RoomUser()
                            .setUid(UIDUtils.gen())
                            .setRid(roomId)
                            .setAuid(v)
                            .setAppKey(sdkId)
                            .setCreateTime(new Date());
                }).collect(Collectors.toList());
        Collection<RoomUser> newRoomUsers = Streams.unionKeyRight(dbRoomUsers, roomUsers, "auid");
        if(!newRoomUsers.isEmpty()){
            roomUserService.saveOrUpdateBatch(newRoomUsers);
        }
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.DEL_USER, consumer = RoomDelUserForm.class)
    public Resp<String> delRoomUser(RoomDelUserForm delUserForm) {
        // check is the room's master?
        if(!roomUserAdminService.checkRoomAdmin(delUserForm.getOwner(), delUserForm.getRid())){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群管理员", delUserForm.getOwner(), delUserForm.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        Room room = Optional.ofNullable(this.getById(delUserForm.getRid())).orElseThrow(()->new ServiceException("room not exist, rid = " + delUserForm.getRid()));
        delRoomUserFromDB(delUserForm.getRid(), delUserForm.getMembers());
        List<User> users = userService.lambdaQuery().in(User::getAuid, new ArrayList<String>(){{
            addAll(delUserForm.getMembers());
            add(delUserForm.getOwner());
        }}).list();
        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v->v.getAuid());
        User originUser = userMapByAuid.getOrDefault(delUserForm.getOwner(), new User());
        List<Long> systemMessageIds = new ArrayList<>();
        delUserForm.getMembers().forEach(member->{
            User targetUser = userMapByAuid.getOrDefault(member, new User());
            JSONObject ex = JSONObject.parseObject(Beans.json(room));
            systemMessageIds.add(systemMessageService.saveSystemMessage(originUser, targetUser, DBConst.SystemMessageCategory.REMOVE_FROM_ROOM.name(), String.format("你已被移出群号为:%s的群", room.getRoomNumber()), ex));
        });
        systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));

        return Resp.ok("成功.");
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.EXIT, consumer = RoomDelUserForm.class)
    public Resp<Void> exitRoomUser(RoomExitUserForm roomExitUserForm) {
        Room room = this.lambdaQuery().eq(Room::getId, roomExitUserForm.getRid()).and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus)).one();
        if(room == null){
            return Resp.failed("当前群不存在,可能已被解散,群id=" + roomExitUserForm.getRid());
        }
        delRoomUserFromDB(roomExitUserForm.getRid(), Collections.singletonList(roomExitUserForm.getAuid()));
        List<User> users = userService.lambdaQuery().in(User::getAuid, Arrays.asList(roomExitUserForm.getAuid(), room.getOwner())).list();
        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v->v.getAuid());
        User originUser = userMapByAuid.getOrDefault(roomExitUserForm.getAuid(), new User());
        User targetUser = userMapByAuid.getOrDefault(room.getOwner(), new User());
        List<Long> systemMessageIds = new ArrayList<>();
        systemMessageIds.add(systemMessageService.saveSystemMessage(originUser, targetUser, DBConst.SystemMessageCategory.EXIT_ROOM.name(), String.format("%s退出了群:%s", originUser.getName(), room.getName()), new JSONObject()));
        systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
        return Resp.ok(null);
    }

    private void delRoomUserFromDB(Long rid, List<String> members){
        int ownerCnt = this.lambdaQuery().in(Room::getOwner, members).count();
        if(ownerCnt > 0){
            throw new ServiceException("群主不能被移出群.");
        }
        List<RoomUser> roomUsers = roomUserService.lambdaQuery()
                .eq(RoomUser::getRid, rid)
                .in(RoomUser::getAuid, members)
                .list();
        if(!roomUsers.isEmpty()){
            roomUserService.removeByIds(Streams.ids(roomUsers, v->v.getId()));
            // 移除群管理员
            roomUserAdminService.remove(Wrappers.<RoomUserAdmin>lambdaQuery()
                    .in(RoomUserAdmin::getAuid, members)
                    .eq(RoomUserAdmin::getRid, rid)
            );
        }
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.DEL, consumer = RoomDelForm.class)
    public Resp<String> delRoom(RoomDelForm delForm) {
        Room room = this.lambdaQuery()
                .eq(Room::getId, delForm.getRid())
                .eq(Room::getOwner, delForm.getOwner())
                .one();
        if(room == null){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群管理员", delForm.getOwner(), delForm.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        // 标记删除
        this.lambdaUpdate()
                .set(Room::getStatus, DBConst.EntryStatus.DEL.name())
                .eq(Room::getId, delForm.getRid())
                .update();
        // 删组员
        List<RoomUser> roomUsers = roomUserService.lambdaQuery().in(RoomUser::getRid, delForm.getRid()).list();
        if(roomUsers.isEmpty()){
            return Resp.ok("删除成功");
        }
        roomUserService.removeByIds(Streams.ids(roomUsers, v->v.getId()));
        // 移除群管理员
        roomUserAdminService.remove(Wrappers.<RoomUserAdmin>lambdaQuery()
                .in(RoomUserAdmin::getAuid, Streams.keys(roomUsers, v->v.getAuid()))
                .eq(RoomUserAdmin::getRid, delForm.getRid())
        );
        List<User> users = new ArrayList<>();
        User user = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, delForm.getOwner()).one()).orElse(new User());
        if(Beans.isNotNull(user.getId())){
            users.add(user);
        }
        List<User> roomUsersInfo = userService.lambdaQuery().in(User::getAuid, Streams.keys(roomUsers, v->v.getAuid())).list();
        users.addAll(roomUsersInfo);
        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v->v.getAuid());
        List<Long> systemMessageIds = new ArrayList<>();
        roomUsers.forEach(roomUser -> {
            Long systemMessageId = systemMessageService.saveSystemMessage(user, userMapByAuid.getOrDefault(roomUser.getAuid(), new User()), DBConst.SystemMessageCategory.DELETE_ROOM.name(), String.format("%s解散了群:%s", user.getName(), room.getRoomNumber()), new JSONObject());
            systemMessageIds.add(systemMessageId);
        });
        systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
        return Resp.ok("成功.");
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.LIST_JOIN, consumer = RoomListJoinForm.class)
    public Resp<List<RoomListDTO>> listRoomJoin(RoomListJoinForm roomListJoinForm) {
        List<RoomUser> roomUsers = roomUserService.lambdaQuery()
                .eq(RoomUser::getAuid, roomListJoinForm.getAuid())
                .list();
        if(roomUsers.isEmpty()){
            return Resp.ok(Collections.emptyList());
        }
        List<Long> rids = Streams.ids(roomUsers, v->v.getRid());
        return this.listRoom(new RoomListForm().setRids(rids));
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.LIST_MEMBER, consumer = RoomListForm.class)
    public Resp<List<RoomDetailListDTO>> listRoomDetail(RoomListForm roomListForm) {
        List<Room> rooms = this.lambdaQuery().in(Room::getId, roomListForm.getRids()).and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus)).list();
        if(rooms.isEmpty()){
            return Resp.ok(new ArrayList<>());
        }
        List<RoomUser> roomUsers = roomUserService.lambdaQuery()
                .in(RoomUser::getRid, Streams.ids(rooms, v->v.getId()))
                .list();
        if(roomUsers.isEmpty()){
            return Resp.ok(new ArrayList<>());
        }
        List<User> users = userService.lambdaQuery()
                .in(User::getAuid, Streams.keys(roomUsers, v->v.getAuid()))
                .list();
        Map<String, UserListDTO> userMapByAuid = Streams.toMapByKey(
                Streams.map(users, v->Beans.copy(v, UserListDTO.class)), v->v.getAuid()
        );

//        Map<Integer, List<RoomUser>> roomUsersMapByGid = Streams.groupById(roomUsers, v->v.getRid());
        List<RoomDetailListDTO> dtos = new ArrayList<>();
        rooms.forEach(v->{
            RoomDetailListDTO dto = Beans.copy(v, RoomDetailListDTO.class);
            dto.setRid(v.getId());
            dto.setMembers(
                    roomUsers.stream()
                    .map(vv->userMapByAuid.getOrDefault(vv.getAuid(), new UserListDTO()))
                    .collect(Collectors.toList())
            );
            dtos.add(dto);
        });

        return Resp.ok(dtos);
    }


    @MethodFor(value = Topic.APP_ROOM.METHOD.SEARCH, consumer = RoomSearchForm.class)
    public Resp<List<RoomListDTO>> roomSearch(RoomSearchForm form) {
        List<Room> rooms = this.lambdaQuery()
                .eq(Room::getRoomNumber, form.getRoomNumber())
                .and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus))
                .list();
        return Resp.ok(Streams.map(rooms, v->Beans.copy(v, RoomListDTO.class).setRid(v.getId())));
    }


    @MethodFor(value = Topic.APP_ROOM.METHOD.JOIN, consumer = RoomJoinForm.class)
    public Resp<Void> roomJoin(RoomJoinForm form) {
        if(form.getNeedAgree() == 0){
            // 先做成简单加入群, 不需要申请
            Room room = this.lambdaQuery().eq(Room::getId, form.getRid()).and(condition -> condition.ne(Room::getStatus, DBConst.EntryStatus.DEL.name()).or().isNull(Room::getStatus)).one();
            if(room == null){
                return Resp.failed("当前群不存在,rid=" + form.getRid());
            }
            User joinUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, form.getAuid()).one()).orElseThrow(()->new ServiceException("用户不存在,auid=" + form.getAuid()));
            User targetUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, room.getOwner()).one()).orElseThrow(()->new ServiceException("房主信息不存在,rid=" + room.getId()));
            String sdkId = "";
            addRoomUserToDB(sdkId, form.getRid(), Collections.singletonList(form.getAuid()));
            List<Long> systemMessageIds = new ArrayList<>();
            systemMessageIds.add(systemMessageService.saveSystemMessage(joinUser, targetUser, DBConst.SystemMessageCategory.JOIN_ROOM.name(), String.format("%s加入了群:%s", joinUser.getName(), room.getName()), new JSONObject()));
            systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
            return Resp.ok(null);
        }
        // 需要群管理员同意
        else {
            Room room = Optional.ofNullable(this.getById(form.getRid())).orElseThrow(()->new ServiceException("room not exist, rid = " + form.getRid()));
            createApplyRoomRecords(form.getRid(), Arrays.asList(form.getAuid()), String.format("%s通过搜索申请加入群号为:%s的群", userService.getUserInfo(form.getAuid()).getName(), room.getRoomNumber()));
            return Resp.ok(null);
        }
    }

    public Resp<Void> roomMuted(RoomMutedForm form) {
//        Room room = this.lambdaQuery()
//                .eq(Room::getId, form.getRid())
//                .eq(Room::getOwner, form.getOwner())
//                .one();
        if(!roomUserAdminService.checkRoomAdmin(form.getOwner(), form.getRid())){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群管理员", form.getOwner(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        //  设置全员禁言(1:是 0:否)
        this.lambdaUpdate()
                .set(Room::getMuted, form.getMuted())
                .eq(Room::getId, form.getRid())
                .update();
        return Resp.ok(null);
    }


    @MethodFor(value = Topic.APP_ROOM.METHOD.ADD_ROOM_ADMIN, consumer = RoomAddAdminForm.class)
    public Resp<Void> addRoomAdmin(RoomAddAdminForm form) {
        Room room = this.lambdaQuery()
                .eq(Room::getId, form.getRid())
                .eq(Room::getOwner, form.getOwner())
                .one();
        if(room == null){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群主", form.getOwner(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        // 检测是否重复添加
        int cnt = roomUserAdminService.lambdaQuery()
                .eq(RoomUserAdmin::getRid, form.getRid())
                .in(RoomUserAdmin::getAuid, form.getMembers())
                .count();
        if(cnt > 0){
            return Resp.failed("存在重复添加为群管理员的用户");
        }
        List<RoomUserAdmin> roomUserAdmins = form.getMembers().stream().map(v->{
            return new RoomUserAdmin()
                    .setAuid(v)
                    .setRid(form.getRid())
                    .setCreateTime(new Date());
        }).collect(Collectors.toList());
        roomUserAdminService.saveBatch(roomUserAdmins);
        return Resp.ok(null);
    }


    @MethodFor(value = Topic.APP_ROOM.METHOD.REMOVE_ROOM_ADMIN, consumer = RoomAddAdminForm.class)
    public Resp<Void> removeRoomAdmin(RoomDelAdminForm form) {
        Room room = this.lambdaQuery()
                .eq(Room::getId, form.getRid())
                .eq(Room::getOwner, form.getOwner())
                .one();
        if(room == null){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群主", form.getOwner(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        roomUserAdminService.remove(Wrappers.<RoomUserAdmin>lambdaQuery()
                .eq(RoomUserAdmin::getRid, form.getRid())
                .in(RoomUserAdmin::getAuid, form.getMembers())
        );
        return Resp.ok(null);
    }



    @MethodFor(value = Topic.APP_ROOM.METHOD.LIST_ROOM_ADMIN, consumer = RoomListAdminForm.class)
    public Resp<List<RoomAdminListDTO>> listRoomAdmin(RoomListAdminForm form) {
//        if(!roomUserAdminService.checkRoomAdmin(form.getOwner(), form.getRid())){
//            return Resp.failed(String.format("owner:%s 不是群:%d 的群管理员", form.getOwner(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
//        }
        List<RoomUserAdmin> roomUserAdmins = roomUserAdminService.lambdaQuery()
                .eq(RoomUserAdmin::getRid, form.getRid())
                .list();
        if(roomUserAdmins.isEmpty()){
            return Resp.ok(new ArrayList<>());
        }
        List<User> users = userService.lambdaQuery().in(User::getAuid, Streams.map(roomUserAdmins, v->v.getAuid())).list();
        List<RoomAdminListDTO> roomAdminListDTOS = users.stream().map(v->{
            return Beans.copy(v, RoomAdminListDTO.class);
        }).collect(Collectors.toList());
        return Resp.ok(roomAdminListDTOS);
    }


    @MethodFor(value = Topic.APP_ROOM.METHOD.LIST_ROOM_MANAGED, consumer = RoomListAdminForm.class)
    public Resp<List<Long>> listRoomManaged(RoomListManagedForm form) {
        List<RoomUser> roomUsers = roomUserService.lambdaQuery()
                .eq(RoomUser::getAuid, form.getAuid())
                .list();
        // 没有加入任何群
        if(roomUsers.isEmpty()){
            return Resp.ok(new ArrayList<>());
        }
        List<Long> roomIds = Streams.ids(roomUsers, v->v.getRid());
        List<RoomUserAdmin> roomUserAdmins = roomUserAdminService.lambdaQuery()
                .in(RoomUserAdmin::getRid, roomIds)
                .eq(RoomUserAdmin::getAuid, form.getAuid())
                .list();
        List<Room> rooms = this.lambdaQuery().eq(Room::getOwner, form.getAuid()).list();
//        List<Long> roomManagedIds = Streams.ids(roomUserAdmins, v->v.getRid());
        Set<Long> roomManagedIds = new HashSet<Long>(){{
            addAll(Streams.ids(roomUserAdmins, v->v.getRid()));
            addAll(Streams.ids(rooms, v->v.getId()));
        }};
        return Resp.ok(new ArrayList<>(roomManagedIds));
    }


    // 撤回指定群员的消息(1:已更新对应的数据; 0:没有数据被更新)
    @MethodFor(value = Topic.APP_ROOM.METHOD.CANCEL_ROOM_MESSAGE, consumer = RoomMessageCancelForm.class)
    public Resp<Integer> cancelRoomUserMessage(RoomMessageCancelForm form) {
        if(!roomUserAdminService.checkRoomAdmin(form.getOwner(), form.getRid())){
            return Resp.failed(String.format("owner:%s 不是群:%d 的群管理员", form.getOwner(), form.getRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        Message message = messageMapper.selectOne(
                Wrappers.<Message>lambdaQuery()
                        .eq(Message::getId, form.getMsgId())
                        .eq(Message::getFromUser, form.getSenderAuid())
        );
        if(message == null){
            // 使用uid再查找一遍
            message = messageMapper.selectOne(
                    Wrappers.<Message>lambdaQuery()
                            .eq(Message::getUid, form.getMsgId())
                            .eq(Message::getFromUser, form.getSenderAuid())
            );
            if(message == null){
                return Resp.ok(0);
            }
        }
        message.setCancel(1);
        message.setCancelTime(new Date());
        messageMapper.updateById(message);
        List<RoomUser> roomUsers = roomUserService.lambdaQuery().eq(RoomUser::getRid, form.getRid()).list();
        User actUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, form.getOwner()).one()).orElseThrow(()->new ServiceException("用户不存在,auid=" + form.getOwner()));
        // 系统群消息
        Message finalMessage = message;
        String body = Beans.json(new HashMap<String, Object>(){{
            put("body", String.format("%s撤销了消息:%d", actUser.getName(), finalMessage.getId()));
            put("rid", finalMessage.getToTarget()); // 群id
        }});
        JSONObject ex = JSONObject.parseObject(Beans.json(message));
        systemMessageService.broadcast(new SystemMessageClusterBroadcastEvent()
                .setWay(DBConst.MessageWay.P2R.name())
                .setRoomMembers(Streams.map(roomUsers, v->v.getAuid()))
                .setCategory(DBConst.SystemMessageCategory.CANCEL_ROOM_MESSAGE.name())
                .setBody(body)
                .setEx(ex.toString())
                .setToTarget(form.getRid() + "")
                .setAppKey("TSDKTEST00001")
                .setStatus("NORMAL")
                .setType("TEXT")
                .setCreateTime(new Date())
        );

        return Resp.ok(1);
    }

    // 显示自己的入群申请记录(只能查看)
    @MethodFor(value = Topic.APP_ROOM.METHOD.APPLY_RECORD, consumer = RoomApplyRecordsForm.class)
    public Resp<List<UserApplyRecordsDTO>> applyRecord(RoomApplyRecordsForm form) {
        User currentUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, form.getAuid()).one()).orElseThrow(()->new ServiceException("用户不存在,auid=" + form.getAuid()));
        List<ApplyRecord> records = applyRecordService.lambdaQuery()
                .eq(ApplyRecord::getFromId, currentUser.getId())
//                .eq(ApplyRecord::getStatus, DBConst.ApplyRecordStatus.NORMAL.name())
                .in(ApplyRecord::getSource, Arrays.asList(DBConst.ApplyRecordSource.SEARCH_ROOM.name(), DBConst.ApplyRecordSource.APPEND_ROOM.name()))
                .orderByDesc(ApplyRecord::getCreateTime)
                .list();
        return Resp.ok(toRoomApplyRecordsDTO(records));
    }


    // 群管理员显示的用户入群申请记录(可以同意, 拒绝)
    @MethodFor(value = Topic.APP_ROOM.METHOD.ADMIN_APPLY_RECORD, consumer = RoomApplyRecordsForm.class)
    public Resp<List<UserApplyRecordsDTO>> adminApplyRecord(RoomApplyRecordsForm form) {
        User currentUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, form.getAuid()).one()).orElseThrow(()->new ServiceException("用户不存在,auid=" + form.getAuid()));
        // 获取自己管理的群
        Resp<List<Long>> respManaged = this.listRoomManaged(new RoomListManagedForm().setAuid(form.getAuid()));
        List<Long> rids = respManaged.getData();
        if(rids.isEmpty()){
            return Resp.ok(new ArrayList<>());
        }
        // 获取自己管理的群的申请记录
        List<ApplyRecord> records = applyRecordService.lambdaQuery()
                .in(ApplyRecord::getToId, rids)
                .eq(ApplyRecord::getStatus, DBConst.ApplyRecordStatus.NORMAL.name())
                .in(ApplyRecord::getSource, Arrays.asList(DBConst.ApplyRecordSource.SEARCH_ROOM.name(), DBConst.ApplyRecordSource.APPEND_ROOM.name()))
                .orderByDesc(ApplyRecord::getCreateTime)
                .list();
        return Resp.ok(toRoomApplyRecordsDTO(records));
    }

    private List<UserApplyRecordsDTO> toRoomApplyRecordsDTO(List<ApplyRecord> records){
        List<Long> userIds = new ArrayList<Long>(){{
            addAll(Streams.ids(records, v->v.getFromId()));
        }};
        if(userIds.isEmpty()){
            return new ArrayList<>();
        }
        List<Long> roomIds = new ArrayList<Long>(){{
            addAll(Streams.ids(records, v->v.getToId()));
        }};
        if(roomIds.isEmpty()){
            return new ArrayList<>();
        }
        List<User> users = userService.getBaseMapper().selectBatchIds(userIds);
        Map<Long, User> usersMapById= Streams.toMapById(users, User::getId);
        List<Room> rooms = this.getBaseMapper().selectBatchIds(roomIds);
        Map<Long, Room> roomMapById= Streams.toMapById(rooms, Room::getId);
        // 申请记录的详细信息
        List<UserApplyRecordsDTO> dtos = Streams.map(records, v->{
            User u = usersMapById.getOrDefault(v.getFromId(), new User());
            Room r = roomMapById.getOrDefault(v.getToId(), new Room());
            return new UserApplyRecordsDTO()
                    .setId(v.getId()) // 申请记录id
                    .setStatus(v.getStatus()) // 申请状态
                    .setSource(v.getSource()) // 来源
                    .setCreateTime(v.getCreateTime()) // 创建时间
                    .setAuid(u.getAuid()) // 用户auid
                    .setAttachText(v.getAttachText()) // 附加文字
                    .setName(u.getName())
                    .setAvatar(u.getAvatar())
                    .setSign(u.getSign())
                    .setAlias("") // 别名
                    // 设置群相关的信息
                    .setRid(r.getId()) // 群id
                    .setRoomName(r.getName()) // 群名称
                    .setRoomNumber(r.getRoomNumber()) // 群号
                    .setRoomAvatar(r.getAvatar()); // 群头像

        });
        return dtos;
    }

    private Resp<Void> doApplyRoom(RoomAgreeForm form, String status) {
        RoomUser existUserMembers = roomUserService.lambdaQuery()
                .eq(RoomUser::getRid, form.getTargetRid())
                .eq(RoomUser::getAuid, form.getTargetAuid())
                .one();
        if (existUserMembers != null) {
            return Resp.failed("该用户已是群成员");
        }
//        List<User> users = userService.lambdaQuery().in(User::getAuid, Arrays.asList(form.getAuid(), form.getTargetAuid())).list();
//        Map<Long, User> userMapById = Streams.toMapById(users, v -> v.getId());
//        Map<String, User> userMapByAuid = Streams.toMapByKey(users, v -> v.getAuid());
        ApplyRecord record = applyRecordService.getById(form.getApplyRecordId());
        if (record == null) {
            return Resp.failed("不存在申请记录.");
        }
        // 同意时添加群员
        if(DBConst.ApplyRecordStatus.FINISH.name().equals(status)){
//            UserRelation userRelation = new UserRelation()
//                    .setId(null)
//                    .setUid(UIDUtils.gen())
//                    .setAuid(userMapById.getOrDefault(record.getFromId(), new User()).getAuid())
//                    .setTargetAuid(userMapById.getOrDefault(record.getToId(), new User()).getAuid())
//                    .setType("FRIEND")
//                    .setApplyText(record.getAttachText())
//                    .setStatus(DBConst.EntryStatus.NORMAL.name())
//                    .setCreateTime(new Date());
//            this.save(userRelation);
//            saveTargetUserFriend(Beans.copy(userRelation, UserFriendAddForm.class));
//
//            User currentUser = userMapByAuid.getOrDefault(form.getAuid(), new User());
//            User targetUser = userMapByAuid.getOrDefault(form.getTargetAuid(), new User());
//
//            List<Long> systemMessageIds = new ArrayList<>();
//            JSONObject ex = new JSONObject();
//            ex.put("currentUser", currentUser);
//            ex.put("targetUser", targetUser);
//            ex.put("currentUserIsMine", 1);
//            systemMessageIds.add(systemMessageService.saveSystemMessage(currentUser, currentUser, DBConst.SystemMessageCategory.APPLY_FRIEND_AGREE.name(), String.format("你已添加好友:%s", targetUser.getName()), ex));
//            ex.put("currentUserIsMine", 0);
//            systemMessageIds.add(systemMessageService.saveSystemMessage(currentUser, targetUser, DBConst.SystemMessageCategory.APPLY_FRIEND_AGREE.name(), String.format("%s已同意你为好友", currentUser.getName()), ex));
//            systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
            RoomJoinForm roomJoinForm = new RoomJoinForm()
                    .setRid(form.getTargetRid())
                    .setAuid(form.getTargetAuid())
                    .setNeedAgree(0);
            this.roomJoin(roomJoinForm); // 加入群
            // 给入群的新用户发一条系统消息
            User targetUser = Optional.ofNullable(userService.lambdaQuery().eq(User::getAuid, form.getTargetAuid()).one()).orElse(new User());
            Room room = Optional.ofNullable(this.getById(form.getTargetRid())).orElse(new Room());
            List<Long> systemMessageIds = new ArrayList<>();
            systemMessageIds.add(systemMessageService.saveSystemMessage(targetUser, targetUser, DBConst.SystemMessageCategory.APPLY_ROOM_PASS.name(), String.format("你已加入了群:%s", room.getRoomNumber()), new JSONObject()));
            systemMessageService.broadcast(new SystemMessageLocalBroadcastEvent().setIds(systemMessageIds));
            // 任意群主, 群管理员同意入群申请后，对其它管理员也设置为系统消息已读.
            List<User> roomAdmins = roomUserAdminService.listRoomAdmin(form.getTargetRid());
            List<SystemMessage> systemMessagesUnread = systemMessageService.lambdaQuery()
                    .in(SystemMessage::getToTarget, Streams.keys(roomAdmins, v->v.getAuid()))
                    .eq(SystemMessage::getCategory, DBConst.SystemMessageCategory.APPLY_ROOM.name())
                    .eq(SystemMessage::getMessageRead, 0)
                    .list();
            systemMessagesUnread = systemMessagesUnread.stream().filter(v->{
                if(Beans.strNotEmpty(v.getEx())){
                    ApplyRecord applyRecord = Beans.beans(v.getEx(), ApplyRecord.class);
                    return form.getTargetRid().equals(applyRecord.getToId()); // 获取当前群的申请记录
                }
                return true;
            }).collect(Collectors.toList());
            if(!systemMessagesUnread.isEmpty()){
                systemMessagesUnread.forEach(v->v.setMessageRead(1));
                systemMessageService.updateBatchById(systemMessagesUnread);
            }
        }
        // 更新申请记录的状态
        record.setStatus(status);
        applyRecordService.updateById(record);
        return Resp.ok(null);
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.AGREE_ROOM, consumer = UserFriendAgreeForm.class)
    public Resp<Void> agreeRoom(@Valid RoomAgreeForm form){
        if(!roomUserAdminService.checkRoomAdmin(form.getAuid(), form.getTargetRid())){
            return Resp.failed(String.format("%s 不是群:%d 的群管理员", form.getAuid(), form.getTargetRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        return doApplyRoom(form, DBConst.ApplyRecordStatus.FINISH.name());
    }

    @MethodFor(value = Topic.APP_ROOM.METHOD.REJECT_ROOM, consumer = UserFriendAgreeForm.class)
    public Resp<Void> rejectRoom(@Valid RoomRejectForm form){
        if(!roomUserAdminService.checkRoomAdmin(form.getAuid(), form.getTargetRid())){
            return Resp.failed(String.format("%s 不是群:%d 的群管理员", form.getAuid(), form.getTargetRid()), AppRespError.ROOM_OWNER_NOT_MASTER.getCode());
        }
        return doApplyRoom(Beans.copy(form, RoomAgreeForm.class), DBConst.ApplyRecordStatus.REJECT.name());
    }

}
