package com.echatim.service.app;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.broker.base.protocol.response.Resp;
import com.commom.AppRespError;
import com.echatim.entity.SdkApp;
import com.echatim.mapper.SdkAppMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:21
 * Description: echatim
 **/


@Slf4j
@DS("master")
@Service
public class SdkAppService extends ServiceImpl<SdkAppMapper, SdkApp>  {
    public Resp<Void> verifySDKKey(String sdkKey){
        SdkApp sdkApp = this.lambdaQuery().eq(SdkApp::getAppKey, sdkKey).one();
        if(sdkApp == null){
            return Resp.failed(AppRespError.SDK_KEY_NOT_EXIST);
        }
        if(sdkApp.getExpireDate().getTime() <= new Date().getTime()){
            return Resp.failed(AppRespError.SDK_KEY_EXPIRE);
        }

        return Resp.ok(null);
    }
}
