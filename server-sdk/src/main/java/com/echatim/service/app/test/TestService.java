package com.echatim.service.app.test;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.corundumstudio.socketio.SocketIOServer;
import com.echatim.ApplicationWrapper;
import com.echatim.broker.localsvc.TopicSender;
import com.echatim.form.MessageSendForm;
import com.echatim.service.app.MessageService;
import com.utils.LoopRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author Daizhiqiang <zqdai@sailfish.com.cn>
 * create by 2021/6/22 10:08
 * Description: easyim
 **/
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@DS("master")
@Service
public class TestService {
    @Autowired
    private TopicSender topicSender;
    @Autowired
    private SocketIOServer socketIOServer;
    @Autowired
    private MessageService messageService;


//    @Scheduled(initialDelay = 10000, fixedRate = 180*1000) //  每120秒, 测试发送
    public void sendMessage(){
        if(!ApplicationWrapper.contextReady()){
            return;
        }
        MessageSendForm messageSendForm = new MessageSendForm();
        messageSendForm.setFromUser("admin");
        messageSendForm.setToTarget("admin");
        messageSendForm.setToTargetAuid("admin");
        messageSendForm.setWay("P2P");
        messageSendForm.setType("TEXT");
        messageSendForm.setBody(JSONObject.parseObject("{\"body\": \"12345\"}"));
        // LoopRunner multiRun 【send_message】 finish, use 21672ms 发1W条 kafka: 476 QPS
        LoopRunner.multiRun("send_message", 10000, (_v)->{
            messageService.send(messageSendForm);
        });
    }

//    @Scheduled(initialDelay = 10000, fixedRate = 120*1000) //  每60秒, 测试发送
    public void sendMessageToP2R(){
        if(!ApplicationWrapper.contextReady()){
            return;
        }
        MessageSendForm messageSendForm = new MessageSendForm();
        messageSendForm.setFromUser("admin");
        messageSendForm.setToTarget("1");
        messageSendForm.setToTargetAuid("admin");
        messageSendForm.setWay("P2R");
        messageSendForm.setType("TEXT");
        messageSendForm.setBody(JSONObject.parseObject("{\"body\": \"12345\"}"));
        // LoopRunner multiRun 【send_message】 finish, use 21340ms 发100条万人群消息 kafka: 400 QPS
        LoopRunner.multiRun("send_message", 100, (_v)->{
            messageService.send(messageSendForm);
        });
    }

}
