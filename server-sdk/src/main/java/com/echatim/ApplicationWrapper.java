package com.echatim;

import org.springframework.context.ApplicationContext;

public class ApplicationWrapper {
    private static ApplicationContext ctx;
    public static boolean contextReady(){
        return ctx != null;
    }
    public static ApplicationContext getContext(){
        if(ctx == null){
            throw new RuntimeException("ApplicationContext not init.");
        }
        return ctx;
    }
    public static void setContext(ApplicationContext context){
        ctx = context;
    }

}
