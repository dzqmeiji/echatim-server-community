package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "群-设置禁言")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomMutedForm extends SocketHideProtocolMessage {
    @NotNull(message = "群ID不能为空")
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID
    @ApiModelProperty("群主账号auid")
    @NotBlank(message = "群主用户帐号不能为空")
    private String owner = ""; // 群主auid
    @ApiModelProperty("设置全员禁言(1:是 0:否)")
    private Integer muted = 0; // 设置全员禁言(1:是 0:否)
}
