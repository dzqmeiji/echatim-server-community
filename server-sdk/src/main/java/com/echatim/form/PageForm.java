package com.echatim.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Daizhiqiang <androidsimu@163.com>
 * create by 2019/12/2 16:03
 * Description: pageForm
 **/

@ApiModel(description = "分页信息")
@Data
public class PageForm{
    @ApiModelProperty("页码")
    protected Integer page = 1;
    @ApiModelProperty("每页数量")
    protected  Integer pageSize = 20;
}
