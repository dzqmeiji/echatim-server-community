package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@ApiModel(value = "历史会话")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class HistoryListSessionForm extends SocketHideProtocolMessage {
    @ApiModelProperty("用户auid")
    @NotBlank(message = "from不能为空")
    private String auid = ""; // 用户auid
    @ApiModelProperty("用户会话开始时间戳(毫秒)")
    @Range(min = 0)
    private Long startTimestamp = 0L; // 会话开始时间戳(毫秒)
    @ApiModelProperty("用户会话结束时间戳(毫秒)")
    @Range(min = 0)
    private Long endTimestamp = 0L; // 会话结束时间戳(毫秒)

    @ApiModelProperty(hidden = true)
    private Date startTime = new Date();
    public Date getStartTime(){
        return new Date(this.startTimestamp);
    }

    @ApiModelProperty(hidden = true)
    private Date endTime = new Date();
    public Date getEndTime(){
        return new Date(this.endTimestamp);
    }
}
