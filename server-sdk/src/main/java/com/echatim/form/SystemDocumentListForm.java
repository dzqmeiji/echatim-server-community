package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


@ApiModel(value = "系统文档")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class SystemDocumentListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("系统文档分类, 数组格式, 成员为字符串类型")
    @Size(min = 1)
    private List<String> categories = new ArrayList<>(); // 系统文档分类
}
