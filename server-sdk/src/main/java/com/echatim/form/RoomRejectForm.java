package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "用户-拒绝加好友申请")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomRejectForm extends SocketHideProtocolMessage {
    @ApiModelProperty("申请记录id")
    @NotNull(message = "申请记录id不能为空")
    private Long applyRecordId = 0L; // 申请记录id
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 当前用户uid
    @ApiModelProperty("目标群rid")
    @NotNull(message = "目标群rid")
    private Long targetRid; // 目标群rid
    @ApiModelProperty("目标用户auid")
    @NotBlank(message = "目标用户auid")
    private String targetAuid = ""; // 目标用户auid
}

