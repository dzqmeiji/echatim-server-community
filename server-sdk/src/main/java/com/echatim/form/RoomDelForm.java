package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "群-解散群")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomDelForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID
    @ApiModelProperty("群主账号auid")
    @NotBlank(message = "群主用户帐号不能为空")
    private String owner = ""; // 群主auid
}
