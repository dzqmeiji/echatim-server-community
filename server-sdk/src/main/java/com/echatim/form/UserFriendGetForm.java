package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-获取好友信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendGetForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 用户uid
    @ApiModelProperty("要获取的朋友auid")
    @NotBlank(message = "朋友auid不能为空")
    private String targetAuid = ""; // 要获取的朋友auid
}

