package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;


@ApiModel(value = "用户-获取添加好友记录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserApplyRecordsForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 当前用户uid
}

