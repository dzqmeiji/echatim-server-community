package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;


@ApiModel(value = "客户端在线监听")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ClientOnlineMonitorForm extends SocketHideProtocolMessage {
    @ApiModelProperty("用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 监听者用户auid
    @ApiModelProperty("要监听的用户auid数组, 成员格式为字符串")
    @Size(min = 1, message = "要监听的用户auids不能为空")
    private List<String> targetAuids = Collections.emptyList(); // 要监听的用户auids
}


