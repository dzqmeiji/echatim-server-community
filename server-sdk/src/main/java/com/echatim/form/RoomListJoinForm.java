package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "群-获取用户加入的超大群")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomListJoinForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "当前用户auid不能为空")
    private String auid = ""; // 用户auid
}
