package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-添加")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserAddForm extends SocketHideProtocolMessage {
    @ApiModelProperty("用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 要添加的用户uid
    @ApiModelProperty("用户所在的appKey, 固定为TSDKTEST00001")
    @NotBlank(message = "用户所在的appKey不能为空")
    private String appKey = ""; // 用户所在的appKey
    @ApiModelProperty("账号昵称")
    private String name = ""; // 账号昵称
    @ApiModelProperty("用户头像")
    private String avatar = ""; // 用户头像
    @ApiModelProperty("用户登录token")
    private String token = ""; // 用户登录token
    @ApiModelProperty("用户签名")
    private String sign = ""; // 用户签名
    @ApiModelProperty("用户email")
    private String email = ""; // 用户email
    @ApiModelProperty("用户生日")
    private String birth = ""; // 用户生日
    @ApiModelProperty("用户mobile")
    private String mobile = ""; // 用户mobile
    @ApiModelProperty("用户性别，0表示未知，1表示男，2女表示女")
    @Range(min = 0, max = 2)
    private Integer gender = 0; // 用户性别，0表示未知，1表示男，2女表示女
    @ApiModelProperty("用户扩展字段")
    private String ex = ""; // 用户扩展字段
    @ApiModelProperty("用户类型(NORMAL:普通用户;VISITOR:游客)")
    private String userType = ""; // 用户类型(NORMAL:普通用户;VISITOR:游客)
    @ApiModelProperty("业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)")
    private String businessPlatform = ""; // 业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)
}
