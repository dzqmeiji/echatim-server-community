package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;


@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class EventSendForm extends SocketHideProtocolMessage {
    @NotBlank(message = "事件类型type不能为空")
    private String eventType = ""; // 事件类型eventType
    private String body = ""; // 事件内容(JSON格式)
}
