package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

@ApiModel(value = "群-通过ID获取群信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomGetForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    @Range(min = 1, message = "请输入群ID")
    private Long rid = 0L; // 群ID
}
