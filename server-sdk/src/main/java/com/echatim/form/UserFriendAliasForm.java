package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-修改好友的备注名称")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendAliasForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 用户uid
    @ApiModelProperty("要编辑的朋友auid")
    @NotBlank(message = "朋友auid不能为空")
    private String targetAuid = ""; // 要编辑的朋友uid
    @ApiModelProperty("朋友备注名")
    @NotBlank(message = "朋友备注名不能为空")
    private String alias = ""; // 要修改的朋友备注名
}

