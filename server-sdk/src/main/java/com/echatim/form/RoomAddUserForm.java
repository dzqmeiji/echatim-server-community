package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "群-添加群成员")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomAddUserForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID
//    @ApiModelProperty("群主账号auid")
//    @NotBlank(message = "群主账号auid")
//    private String owner = ""; // 群主auid
    @ApiModelProperty("当前用户auid")
//    @NotBlank(message = "当前用户auid") // 这里不强制要求填, 兼容旧接口
    private String auid = ""; // 当前用户auid
    @ApiModelProperty("邀请群成员auid数组, 成员为字符串格式")
    @Size(min = 1, message = "邀请的群成员为空")
    private List<String> members = new ArrayList<>(); // 群成员, 无需再加owner自己的账号
    @ApiModelProperty("拉群时是否需要确认 1:是 0:否")
    private Integer needAgree = 0; // 拉群时是否需要确认.
    @ApiModelProperty("邀请文字")
    private String inviteText = ""; // 邀请文字
    @ApiModelProperty("群信息扩展字段, JSON字符串格式")
    private String ex = ""; // 扩展字段
}
