package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "群-删除成员")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomDelUserForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID
    @ApiModelProperty("群主账号auid")
    @NotBlank(message = "群主用户帐号不能为空")
    private String owner = ""; // 群主auid
    @ApiModelProperty("移除的群成员auid数组, 成员为字符串格式")
    @Size(min = 1, message = "移除的群成员不能为空")
    private List<String> members = new ArrayList<>(); // 要移除的群成员
    @ApiModelProperty("群扩展字段, JSON格式")
    private String ex = ""; // 扩展字段
}
