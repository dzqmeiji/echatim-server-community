package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-获取黑名单记录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserBlackForbidListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("要查询的用户uid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 要查询的用户uid
}

