package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;

@ApiModel(value = "群-通过ID获取多个群信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群列表ID, 成员为Long格式")
    @Size(min = 1, message = "请输入群列表ID")
    private List<Long> rids = Collections.emptyList(); // 群ID
}
