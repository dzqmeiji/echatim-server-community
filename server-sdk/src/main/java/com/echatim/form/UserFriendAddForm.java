package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-添加好友")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendAddForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 要添加的用户uid
    @ApiModelProperty("朋友auid")
    @NotBlank(message = "朋友auid不能为空")
    private String targetAuid = ""; // 要添加的朋友auid
    @ApiModelProperty("ADD:直接加好友;APPLY:直接加好友;AGREE:同意加好友;REJECT:拒绝加好友;")
    @NotBlank(message = "type必须为ADD,APPLY,AGREE,REJECT 其中之一")
    private String type = ""; // ADD:直接加好友;APPLY:直接加好友;AGREE:同意加好友;REJECT:拒绝加好友;
    @ApiModelProperty("添加好友时的备注名")
    private String alias = ""; // 添加好友时的备注名
    @ApiModelProperty("请求加好友对应的请求消息(此信息在接受好友添加请求的时候显示)")
    private String applyText = ""; // 请求加好友对应的请求消息(此信息在接受好友添加请求的时候显示)
}

