package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "用户加入的群最新一条记录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class HistoryRoomRecentlyForm extends SocketHideProtocolMessage {
    @ApiModelProperty("用户auid")
    @NotBlank(message = "from不能为空")
    private String auid = ""; // 用户auid
    @ApiModelProperty("群id列表, 成员为整形类型")
    private List<Long> rids = new ArrayList<>();
}
