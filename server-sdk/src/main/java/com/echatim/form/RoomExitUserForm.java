package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "群-退出群")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomExitUserForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID
    @ApiModelProperty("当前群员auid")
    @NotBlank(message = "当前群员auid不能为空")
    private String auid = ""; // 当前群员auid
}
