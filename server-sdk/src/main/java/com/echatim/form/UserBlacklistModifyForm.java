package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-修改黑名单")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserBlacklistModifyForm extends SocketHideProtocolMessage {
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 要添加的用户uid
    @NotBlank(message = "朋友auid不能为空")
    private String targetAuid = ""; // 要添加的朋友uid
    @NotBlank(message = "type必须为ADD_BLACKLIST,ADD_FORBID,REMOVE_BLACKLIST,REMOVE_FORBID 其中之一")
    private String type = ""; // ADD_BLACKLIST:添加黑名单;ADD_FORBID:添加禁言;REMOVE_BLACKLIST:移除黑名单;REMOVE_FORBID:移除禁言;
}

