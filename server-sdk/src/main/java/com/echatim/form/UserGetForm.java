package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;


@ApiModel(value = "用户-获取好友信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserGetForm extends SocketHideProtocolMessage {
    @ApiModelProperty("要获取的用户auid")
    @NotBlank(message = "用户userAuid不能为空")
    private String userAuid = ""; // 要获取的用户auid
}
