package com.echatim.form;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@ApiModel(value = "发送消息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class MessageSendForm extends SocketHideProtocolMessage {
    @ApiModelProperty("来源用户auid")
    @NotBlank(message = "from不能为空")
    private String fromUser = ""; // 用户auid
    @ApiModelProperty("目标用户auid/目标群id")
    @NotBlank(message = "to不能为空")
    private String toTarget = ""; // 用户auid/ 群id
    @ApiModelProperty("在 toTarget 是群id 的情况下, 这是接受者的auid")
    private String toTargetAuid = ""; // 在 toTarget 是群id 的情况下, 这是接受者的auid
    @ApiModelProperty("消息发送方式(P2P:点对点;P2G:点对多)")
    @NotBlank(message = "way不能为空")
    private String way = "";// 消息发送方式(P2P:点对点;P2G:点对多)
    @ApiModelProperty("消息类型, 取值为:TEXT,IMAGE,AUDIO,VIDEO,POSITION,FILE,NOTIFY,CUSTOM")
    @NotBlank(message = "type不能为空")
    private String type = "";//TEXT,IMAGE,AUDIO,VIDEO,POSITION,FILE,NOTIFY,CUSTOM
//    @NotBlank(message = "body不能为空")
    @ApiModelProperty("消息体,json格式")
    private JSONObject body = new JSONObject();//消息体,json格式
    @ApiModelProperty("消息选项设置")
    private String msgOption = "";//消息选项设置
    @ApiModelProperty("消息要AT的用户,json 数组格式")
    private String at_users = "";//消息要AT的用户,json 数组格式

    @ApiModelProperty(hidden = true)
    private String fromUserName = ""; // 用户名称
    @ApiModelProperty(hidden = true)
    private String fromUserAvatar = ""; // 用户头像
    @ApiModelProperty(hidden = true)
    private String toTargetName = ""; // 目标用户名称
    @ApiModelProperty(hidden = true)
    private String toTargetAvatar = ""; // 目标用户头像

    @ApiModelProperty("消息的发送时间")
    private Date sendTime = new Date();
    @ApiModelProperty(value = "消息uid", hidden = true)
    private String mid = ""; // 消息uid
}
