package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-删除好友")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendDelForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 要添加的用户uid
    @ApiModelProperty("要删除的朋友auid")
    @NotBlank(message = "朋友auid不能为空")
    private String targetAuid = ""; // 要删除的朋友auid
}

