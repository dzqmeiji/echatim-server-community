package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;

@ApiModel(value = "用户-获取多个用信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("要获取的用户auid数组, 成员为字符串格式")
    @Size(min = 1, message = "用户auids不能为空")
    private List<String> auids = Collections.emptyList(); // 要获取的用户uid
}
