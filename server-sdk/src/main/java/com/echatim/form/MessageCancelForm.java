package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "发送消息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class MessageCancelForm extends SocketHideProtocolMessage {
    @ApiModelProperty("消息id/消息mid")
    @NotBlank(message = "消息id不能为空")
    private String msgId = ""; // 消息id

    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "当前用户auid不能为空")
    private String auid = ""; // 当前用户auid
}
