package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "用户-获取好友列表")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("当前用户auid")
    @NotBlank(message = "用户auid不能为空")
    private String auid = ""; // 当前用户auid
    @ApiModelProperty("该时间戳之后更新的好友列表(不填则显示全部)")
    private Long updatetime = 0L; // 返回该时间戳之后更新的好友列表(不填则显示全部)
}

