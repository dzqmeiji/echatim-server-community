package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "发送消息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomMessageCancelForm extends SocketHideProtocolMessage {
    @NotNull(message = "群ID不能为空")
    @ApiModelProperty("群ID")
    private Long rid = 0L; // 群ID

    @ApiModelProperty("群主账号auid")
    @NotBlank(message = "群主用户帐号不能为空")
    private String owner = ""; // 群主auid

    @ApiModelProperty("消息id/消息uid")
    @NotBlank(message = "消息id不能为空")
    private String msgId = ""; // 消息id

    @ApiModelProperty("消息发送者auid")
    @NotBlank(message = "消息发送者auid不能为空")
    private String senderAuid = ""; // 消息发送者auid
}
