package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;


@ApiModel(value = "客户端在线信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ClientUserOnlineForm extends SocketHideProtocolMessage {
    @ApiModelProperty("要查询的用户auid数组, 成员格式为字符串")
    @NotNull(message = "用户auids不能为空")
    private List<String> auids = Collections.emptyList(); // 要查询的用户auids
}
