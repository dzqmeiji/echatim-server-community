package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import java.util.Date;


@ApiModel(value = "历史消息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class HistoryListForm extends SocketHideProtocolMessage {
    @ApiModelProperty("来源用户auid")
    @NotBlank(message = "from不能为空")
    private String fromUser = ""; // 用户auid
    @ApiModelProperty("目标用户auid/群rid")
    @NotBlank(message = "to不能为空")
    private String toTarget = ""; // 用户auid/ 群uid
    @ApiModelProperty("发送方式 P2P/P2R")
    @NotBlank(message = "发送方式不能为空")
    private String way = ""; // 发送方式 P2P/P2R
    @ApiModelProperty("查询开始时间戳(毫秒)")
    @Range(min = 0)
    private Long startTimestamp = 0L; // 开始时间戳(毫秒)
    @ApiModelProperty("查询结束时间戳(毫秒)")
    @Range(min = 0)
    private Long endTimestamp = 0L; // 结束时间戳(毫秒)
    @ApiModelProperty("消息条数最大值(默认值10条)")
    @Range(min = 0, max = 100)
    private Integer limit = 10; // 消息条数最大值(默认值10条)

    @ApiModelProperty(hidden = true)
    private Date startTime = new Date();
    public Date getStartTime(){
        return new Date(this.startTimestamp);
    }
    @ApiModelProperty(hidden = true)
    private Date endTime = new Date();
    public Date getEndTime(){
        return new Date(this.endTimestamp);
    }
}
