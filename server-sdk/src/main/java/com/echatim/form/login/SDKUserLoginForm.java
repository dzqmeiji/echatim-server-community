package com.echatim.form.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "后端管理用户登录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class SDKUserLoginForm {
    @ApiModelProperty("后端管理用户名称")
    private String name = ""; //
    @ApiModelProperty("后端管理邮箱地址(登录名)")
    private String email = ""; //
    @ApiModelProperty("后端管理手机号")
    private String phone = ""; //
    @ApiModelProperty("后端管理密码(登录密码)")
    @NotBlank(message = "用户password不能为空")
    private String password = ""; //
}
