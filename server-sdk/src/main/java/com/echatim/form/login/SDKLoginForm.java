package com.echatim.form.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Deprecated
@ApiModel(value = "SDK登录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class SDKLoginForm {
    @ApiModelProperty("appKey")
    @NotBlank(message = "appKey不能为空")
    private String appKey = ""; //
    @ApiModelProperty("appSecret")
    @NotBlank(message = "appSecret不能为空")
    private String appSecret = ""; //
}
