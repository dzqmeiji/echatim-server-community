package com.echatim.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "群-退出群")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomJoinForm extends SocketHideProtocolMessage {
    @ApiModelProperty("群ID")
    @Range(min = 1, message = "请输入群ID")
    private Long rid = 0L; // 群ID
    @ApiModelProperty("加入的用户auid")
    @NotBlank(message = "auid不能为空")
    private String auid = ""; // 加入的用户auid

    private Integer needAgree = 0; // 入群是否需要群管理员同意(1:需要; 0:不同意)
}
