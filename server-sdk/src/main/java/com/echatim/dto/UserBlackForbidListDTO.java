package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "黑名单记录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserBlackForbidListDTO {
    @ApiModelProperty("黑名单记录")
    private List<UserFriendListDTO> blacklists = new ArrayList<>(); // 黑名单列表
    @ApiModelProperty("禁言用户")
    private List<UserFriendListDTO> forbids = new ArrayList<>(); // 禁言用户列表
}
