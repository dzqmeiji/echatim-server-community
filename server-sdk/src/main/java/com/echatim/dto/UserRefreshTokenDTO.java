package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@ApiModel(value = "随机更新用户登录密码")
@Deprecated
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserRefreshTokenDTO  {
    @ApiModelProperty("用户auid")
    private String auid = ""; // 用户auid
    @ApiModelProperty("用户token")
    private String token = ""; // 用户token
}
