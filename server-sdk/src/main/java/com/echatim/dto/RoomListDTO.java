package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@ApiModel(value = "群列表")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class RoomListDTO  {
    @ApiModelProperty("群id")
    private Long rid = 0L; // 群id
    @ApiModelProperty("群名")
    private String name = ""; //群名
    @ApiModelProperty("群号(8位)")
    private Long roomNumber = 0L; //群号(8位)
    @ApiModelProperty("群主,用户auid")
    private String owner = ""; // 群主,用户auid
    @ApiModelProperty("群公告")
    private String announce = ""; // 群公告
    @ApiModelProperty("群简介")
    private String introduce = ""; // 群简介
    @ApiModelProperty("邀请文字")
    private String inviteText = ""; // 邀请文字
    @ApiModelProperty("群LOGO,头像")
    private String avatar = ""; // 群LOGO,头像
    @ApiModelProperty("群加入模式")
    private String confJoinmode = ""; // 群加入模式
    @ApiModelProperty("群员被邀请方式")
    private Integer confBeinvite = 0; // 群员被邀请方式
    @ApiModelProperty("群员邀请权限(OWNER:仅群主;ALL:任何人也可以)")
    private Integer confInviteother = 0; // 群员邀请权限(OWNER:仅群主;ALL:任何人也可以)
    @ApiModelProperty("群信息更新权限(OWNER:仅群主;ALL:任何人也可以)")
    private Integer confUpdate = 0; // 群信息更新权限(OWNER:仅群主;ALL:任何人也可以)
    @ApiModelProperty("群最大成员数量")
    private Integer maxMember = 0; // 群最大成员数量
    @ApiModelProperty("设置全员禁言(1:是 0:否)")
    private Integer muted = 0; // 设置全员禁言(1:是 0:否)
    @ApiModelProperty("状态(NORMAL:正常;DEL: 已解散)")
    private String status = ""; // 状态(NORMAL:正常;DEL: 已解散)
    @ApiModelProperty("创建时间")
    private Date createTime = new Date(1); // 创建时间
}
