package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@ApiModel(value = "用户在线信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ClientUserOnlineDTO{
    @ApiModelProperty("用户auid")
    private String auid = "";
    @ApiModelProperty("用户是否在线. 0: 不在线; 1: 在线")
    private Integer online = 0; // 是否在线. 0: 不在线; 1: 在线
}
