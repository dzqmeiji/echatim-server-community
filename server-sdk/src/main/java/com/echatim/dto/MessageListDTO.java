package com.echatim.dto;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;


@ApiModel(value = "用户聊天消息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class MessageListDTO  {
    @ApiModelProperty("消息id")
    private Long id = 0L; // 消息id
    @ApiModelProperty("头像")
    private String avatar = ""; // 头像
    @ApiModelProperty("消息发送方式(P2P,P2R,P2LR)")
    private String way = ""; // 发送方式(P2P,P2R,P2LR)
    @ApiModelProperty("消息发送者")
    private String fromUser = ""; // 消息发送者
    @ApiModelProperty("消息发送者名字")
    private String fromUserName = ""; // 消息发送者名字
    @ApiModelProperty("消息发送者头像")
    private String fromUserAvatar = ""; // 消息发送者头像
    @ApiModelProperty("消息发送对象auid")
    private String toTarget = ""; // 消息发送对象
    @ApiModelProperty("消息发送对象名字")
    private String toTargetName = ""; // 消息发送对象名字
    @ApiModelProperty("消息发送对象头像")
    private String toTargetAvatar = ""; // 消息发送对象头像
    @ApiModelProperty("消息是不是发给我的(0:自己发出去的消息; 1. 别人发给我的消息)")
    private Integer toMe = 0; // 消息是不是发给我的(0:自己发出去的消息; 1. 别人发给我的消息)
    @ApiModelProperty("消息类型, 有以下取值: TEXT,IMAGE,AUDIO,VIDEO,POSITON,FILE,NOTIFY,CUSTOM")
    private String type = ""; // 消息类型
    @ApiModelProperty("消息内容")
    private JSONObject body = new JSONObject(); // 消息内容
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    @ApiModelProperty("消息创建时间")
    private Date createTime = new Date(1); // 创建时间
}
