package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@ApiModel(value = "客户端列表")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ClientListDTO {
    @ApiModelProperty("用户登录的客户端id")
    private String clientId = ""; // 用户登录的客户端id
    @ApiModelProperty("用户auid")
    private String auid = ""; // 用户auid
    @ApiModelProperty("用户账号昵称")
    private String name = ""; // 账号昵称
}
