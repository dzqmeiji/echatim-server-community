package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@ApiModel(value = "好友列表信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserFriendListDTO  {
    @ApiModelProperty("好友的auid")
    private String auid = ""; // 好友的auid
    @ApiModelProperty("好友名字")
    private String name = ""; // 好友名字
    @ApiModelProperty("好友名字拼音")
    private String namePinyin = ""; // 好友名字拼音
    @ApiModelProperty("好友头像")
    private String avatar = ""; // 好友头像
    @ApiModelProperty("好友备注名")
    private String alias = ""; // 好友备注名
    @ApiModelProperty("好友签名")
    private String sign = ""; // 好友签名
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    private Date createTime = new Date(1L); // 创建时间
}
