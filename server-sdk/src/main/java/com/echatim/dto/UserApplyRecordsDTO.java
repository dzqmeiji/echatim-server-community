package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@ApiModel(value = "用户加好友记录")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserApplyRecordsDTO {
    @ApiModelProperty("申请记录id")
    private Long id; // 申请记录id
    @ApiModelProperty("附加文字")
    private String attachText; // 附加文字
    private String status; // 申请状态
    private String source; // 来源

    @ApiModelProperty("好友的auid")
    private String auid = ""; // 好友的auid
    @ApiModelProperty("好友名字")
    private String name = ""; // 好友名字
    @ApiModelProperty("好友头像")
    private String avatar = ""; // 好友头像
    @ApiModelProperty("好友备注名")
    private String alias = ""; // 好友备注名
    @ApiModelProperty("好友签名")
    private String sign = ""; // 好友签名
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    private Date createTime = new Date(1L); // 创建时间

    // 群的相关信息
    @ApiModelProperty("群id")
    private Long rid = 0L; // 群id
    @ApiModelProperty("群名称")
    private String roomName = ""; // 群名称
    @ApiModelProperty("群号")
    private Long roomNumber = 0L; // 群号
    @ApiModelProperty("群头像")
    private String roomAvatar = ""; // 群头像
}
