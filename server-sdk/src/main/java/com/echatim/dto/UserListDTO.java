package com.echatim.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@ApiModel(value = "用户列表信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class UserListDTO {
    @ApiModelProperty("用户auid")
    private String auid = ""; // 用户auid
    @ApiModelProperty("账号昵称")
    private String name = ""; // 账号昵称
    @ApiModelProperty("用户头像")
    private String avatar = ""; // 用户头像
    @ApiModelProperty("用户签名")
    private String sign = ""; // 用户签名
    @ApiModelProperty("用户email")
    private String email = ""; // 用户email
    @ApiModelProperty("用户生日")
    private String birth = ""; // 用户生日
    @ApiModelProperty("用户mobile")
    private String mobile = ""; // 用户mobile
    @ApiModelProperty("用户性别，0表示未知，1表示男，2女表示女")
    private Integer gender = 0; // 用户性别，0表示未知，1表示男，2女表示女
    @ApiModelProperty("用户扩展字段")
    private String ex = ""; // 用户扩展字段
    @ApiModelProperty("账号昵称拼音")
    private String namePinyin = ""; // 账号昵称拼音
    @ApiModelProperty("创建时间")
    private Date createTime = new Date(1); // 创建时间
}
