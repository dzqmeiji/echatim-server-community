package com.echatim.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "服务端信息")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ServerInfoDTO {
    @ApiModelProperty("git分支名")
    private String branch;
    @ApiModelProperty("git id值")
    private String gitIdAbbrev;
    @ApiModelProperty("git提交时间")
    private String gitTime;
    @ApiModelProperty("认证类型")
    private String authType;
    @ApiModelProperty("系统已加载的模块")
    private List<String> modules = new ArrayList<>();
    @ApiModelProperty("系统已加载的工具集合")
    private List<ServerToolkit> toolkit = new ArrayList<>();

    @JsonIgnoreProperties(ignoreUnknown = true)
    @NoArgsConstructor
    @Accessors(chain = true)
    @Data
    public static class ServerToolkit{
        @ApiModelProperty("工具名称")
        private String name;
        @ApiModelProperty("该工具是否使用 1:是 0:否")
        private Integer enable;
    }


}
