package com.echatim.dto;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@ApiModel(value = "用户历史会话")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class HistorySessionDTO {
    @ApiModelProperty("消息id")
    private Long id = 0L; // 消息id
    @ApiModelProperty("消息的来源用户auid")
    private String fromUser = ""; // fromUser auid
    @ApiModelProperty("消息的目标用户auid")
    private String toTarget = ""; // toTarget auid
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    @ApiModelProperty("消息的最后更新的时间戳")
    private Date createTime = new Date(1); // 最后更新的时间戳
    @ApiModelProperty("消息的来源用户名")
    private String fromUserName = ""; // fromUser 用户名字
    @ApiModelProperty("消息的来源用户头像")
    private String fromUserAvatar = ""; // fromUser 用户头像
    @ApiModelProperty("消息的目标用户名")
    private String toTargetName = ""; // toTarget 用户名字
    @ApiModelProperty("消息的目标用户头像")
    private String toTargetAvatar = ""; // toTarget 用户头像
    @ApiModelProperty("消息内容")
    private JSONObject body = new JSONObject(); // 消息内容
    @ApiModelProperty("消息类型, 有以下取值: TEXT,IMAGE,AUDIO,VIDEO,POSITON,FILE,NOTIFY,CUSTOM")
    private String type = ""; // 消息类型
    @ApiModelProperty("消息未读数(未使用)")
    private Integer unread = 0; // 未读数
}
