package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 平台的APP管理SDK
 * </p>
 *
 * @author sun
 * @since 2020-05-13
 */
@Deprecated
@ApiModel(value = "sdk app")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SdkApp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("唯一性带时间戳自增长的ID")
    @TableField("uid")
    private Long uid;

    /**
     * APP 拥有者, sdk_user id
     */
    @TableField("owner")
    private Long owner;

    /**
     * app_key:识别是那个APP
     */
    @ApiModelProperty("app_key:识别是那个APP")
    @TableField("app_key")
    private String appKey;

    /**
     * app 的等级(TEST: 使用版; BASE: 基础版; PROFESSIONAL: 专业版)
     */
    @TableField("level")
    private String level;

    /**
     * APP名字
     */
    @ApiModelProperty("APP名字")
    @TableField("name")
    private String name;

    /**
     * 行业类型
     */
    @ApiModelProperty("行业类型")
    @TableField("business_type")
    private String businessType;

    /**
     * 业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)
     */
    @TableField("business_platform")
    private String businessPlatform;

    /**
     * 简介
     */
    @ApiModelProperty("简介")
    @TableField("introduce")
    private String introduce;

    /**
     * app_secret. 用于1. 用户后台API 请求
     */
    @TableField("app_secret")
    private String appSecret;

    /**
     * client_secret.用于1.文件服务器; 2. embed app
     */
    @TableField("client_secret")
    private String clientSecret;

    /**
     * 限制的连接数
     */
    @ApiModelProperty("限制的连接数")
    @TableField("limit_connection")
    private Integer limitConnection;

    /**
     * app 允许使用时间(天)
     */
    @TableField("allow_use_day")
    private Integer allowUseDay;

    /**
     * app 过期时间
     */
    @TableField("expire_date")
    private Date expireDate;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * 状态(NORMAL:正常;DEL: 已删除)
     */
    @TableField("status")
    private String status;

}
