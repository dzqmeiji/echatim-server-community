package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 阿里云OSS文件信息
 * </p>
 *
 * @author sun
 * @since 2020-05-07
 */
@ApiModel(value = "文件信息")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FileInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * SDK APP ID
     */
    @ApiModelProperty("appKey")
    @TableField("app_key")
    private String appKey;

    /**
     * 文件服务器
     */
    @ApiModelProperty("文件服务器")
    @TableField("server")
    private String server;

    /**
     * OSS 对象名(组成: 路径+文件名)
     */
    @ApiModelProperty("OSS 对象名(组成: 路径+文件名)")
    @TableField("filename")
    private String filename;

    /**
     * 上传的文件大小(字节)
     */
    @ApiModelProperty("上传的文件大小(字节)")
    @TableField("size")
    private Integer size;

    /**
     * 上传文件的mime类型
     */
    @ApiModelProperty("上传文件的mime类型")
    @TableField("mimeType")
    private String mimeType;

    /**
     * 若是图片，这是图片的高度
     */
    @ApiModelProperty("若是图片，这是图片的高度")
    @TableField("height")
    private Integer height;

    /**
     * 若是图片，这是图片的宽度
     */
    @ApiModelProperty("若是图片，这是图片的宽度")
    @TableField("width")
    private Integer width;

    /**
     * 上传的原始文件名
     */
    @ApiModelProperty("上传的原始文件名")
    @TableField("originFileName")
    private String originFileName;

    /**
     * 文件类型: IMAGE, FILE, OTHER
     */
    @ApiModelProperty("文件类型: IMAGE, FILE, OTHER")
    @TableField("fileType")
    private String fileType;

    /**
     * 文件下载地址
     */
    @ApiModelProperty("文件下载地址")
    @TableField("url")
    private String url;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdk id, 用于租户隔离", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
