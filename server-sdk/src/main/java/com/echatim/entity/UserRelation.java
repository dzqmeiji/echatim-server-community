package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 用户关系表
 * </p>
 *
 * @author sun
 * @since 2020-05-07
 */
@ApiModel(value = "用户关系")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内部ID
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("uid")
    @TableField("uid")
    private Long uid;

    /**
     * SDK APP KEY
     */
    @ApiModelProperty("appKey")
    @TableField("app_key")
    private String appKey;

    /**
     * 当前用户auid
     */
    @ApiModelProperty("当前用户auid")
    @TableField("auid")
    private String auid;

    /**
     * 关系用户auid
     */
    @ApiModelProperty("关系用户auid")
    @TableField("target_auid")
    private String targetAuid;

    /**
     * 关系(FRIEND:好友)
     */
    @ApiModelProperty("关系(FRIEND:好友)")
    @TableField("type")
    private String type;

    /**
     * 请求添加好友时的请求文字
     */
    @ApiModelProperty("请求添加好友时的请求文字")
    @TableField("apply_text")
    private String applyText;

    /**
     * 备注名
     */
    @ApiModelProperty("备注名")
    @TableField("alias")
    private String alias;

    /**
     * 是否拉黑(1:是;0:否)
     */
    @ApiModelProperty("是否拉黑(1:是;0:否)")
    @TableField("blacklist")
    private Integer blacklist;

    /**
     * 是否禁言(1:是;0:否)
     */
    @ApiModelProperty("是否禁言(1:是;0:否)")
    @TableField("forbid")
    private Integer forbid;

    /**
     * 状态(NORMAL:正常;DEL:删除)
     */
    @ApiModelProperty("状态(NORMAL:正常;DEL:删除)")
    @TableField("status")
    private String status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdk id, 用于租户隔离", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
