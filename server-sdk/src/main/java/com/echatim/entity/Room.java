package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 组
 * </p>
 *
 * @author sun
 * @since 2020-05-07
 */
@ApiModel(value = "群")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("唯一性带时间戳自增长的ID")
    @TableField("uid")
    private Long uid;

    /**
     * 群号(唯一)
     */
    @ApiModelProperty("群号(唯一)")
    @TableField("room_number")
    private Long roomNumber;

    /**
     * SDK APP KEY
     */
    @TableField("app_key")
    private String appKey;

    /**
     * 群名
     */
    @ApiModelProperty("群名")
    @TableField("name")
    private String name;

    /**
     * 群主,用户auid
     */
    @ApiModelProperty("群主,用户auid")
    @TableField("owner")
    private String owner;

    /**
     * 群公告
     */
    @ApiModelProperty("群公告")
    @TableField("announce")
    private String announce;

    /**
     * 群简介
     */
    @ApiModelProperty("群简介")
    @TableField("introduce")
    private String introduce;

    /**
     * 邀请文字
     */
    @ApiModelProperty("邀请文字")
    @TableField("invite_text")
    private String inviteText;

    /**
     * 群LOGO,头像
     */
    @ApiModelProperty("群LOGO,头像")
    @TableField("avatar")
    private String avatar;

    /**
     * 群加入模式
     */
    @ApiModelProperty("群加入模式")
    @TableField("conf_joinmode")
    private String confJoinmode;

    /**
     * 群员被邀请方式
     */
    @ApiModelProperty("群员被邀请方式")
    @TableField("conf_beinvite")
    private String confBeinvite;

    /**
     * 群员邀请权限(OWNER:仅群主;ALL:任何人也可以)
     */
    @ApiModelProperty("群员邀请权限(OWNER:仅群主;ALL:任何人也可以)")
    @TableField("conf_inviteother")
    private String confInviteother;

    /**
     * 群信息更新权限(OWNER:仅群主;ALL:任何人也可以)
     */
    @ApiModelProperty("群信息更新权限(OWNER:仅群主;ALL:任何人也可以)")
    @TableField("conf_update")
    private String confUpdate;

    /**
     * 群最大成员数量
     */
    @ApiModelProperty("群最大成员数量")
    @TableField("max_member")
    private Integer maxMember;


    /**
     * 群是否设置了全员禁言(1:是 0:否)
     */
    @ApiModelProperty("群是否设置了全员禁言(1:是 0:否)")
    @TableField("muted")
    private Integer muted;

    /**
     * 文件状态(NORMAL:正常;DEL: 已解散)
     */
    @TableField("status")
    private String status;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @TableField("sdk_id")
    private Integer sdkId;
}
