package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 系统消息表
 * </p>
 *
 * @author sun
 * @since 2021-08-17
 */
@ApiModel(value = "系统消息")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SystemMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内部ID
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("uid")
    @TableField("uid")
    private Long uid;

    /**
     * SDK APP ID
     */
    @ApiModelProperty("appKey")
    @TableField("app_key")
    private String appKey;

    /**
     * 目标用户auid
     */
    @ApiModelProperty("目标用户auid")
    @TableField("to_target")
    private String toTarget;
    /**
     * 业务分类
     */
    @ApiModelProperty("业务分类")
    @TableField("category")
    private String category;
    /**
     * TEXT
     */
    @ApiModelProperty("类型")
    @TableField("type")
    private String type;

    /**
     * 消息体,json格式
     */
    @ApiModelProperty("消息体,json格式")
    @TableField("body")
    private String body;

    /**
     * 业务附加消息,json格式
     */
    @ApiModelProperty("业务附加消息,json格式")
    @TableField("ex")
    private String ex;

    /**
     * 消息状态(NORMAL:正常)
     */
    @ApiModelProperty("消息状态(NORMAL:正常)")
    @TableField("status")
    private String status;

    /**
     * 是否已读(1:已读; 0:未读)
     */
    @ApiModelProperty("是否已读(1:已读; 0:未读)")
    @TableField("message_read")
    private Integer messageRead;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdk id, 用于租户隔离", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
