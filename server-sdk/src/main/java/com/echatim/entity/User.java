package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 用户表
 * </p>
 *
 * @author sun
 * @since 2020-10-15
 */
@ApiModel(value = "用户")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内部ID
     */
    @ApiModelProperty("用户id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("用户uid")
    @TableField("uid")
    private Long uid;

    /**
     * SDK APP KEY
     */
    @ApiModelProperty("appKey")
    @TableField("app_key")
    private String appKey;

    /**
     * APP用户账号ID(与客户系统对接的用户ID)
     */
    @ApiModelProperty("用户auid")
    @TableField("auid")
    private String auid;

    /**
     * 账号昵称
     */
    @ApiModelProperty("用户账号昵称")
    @TableField("name")
    private String name;

    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    @TableField("avatar")
    private String avatar;

    /**
     * 用户登录密码
     */
    @ApiModelProperty("用户登录密码")
    @TableField("token")
    private String token;

    /**
     * 用户登录token 过期时间
     */
    @ApiModelProperty("用户登录密码过期时间")
    @TableField("token_expire_date")
    private Date tokenExpireDate;

    /**
     * 用户签名
     */
    @ApiModelProperty("用户签名")
    @TableField("sign")
    private String sign;

    /**
     * 用户email
     */
    @ApiModelProperty("用户email")
    @TableField("email")
    private String email;

    /**
     * 用户生日
     */
    @ApiModelProperty("用户生日")
    @TableField("birth")
    private String birth;

    /**
     * 用户mobile
     */
    @ApiModelProperty("用户mobile")
    @TableField("mobile")
    private String mobile;

    /**
     * 用户性别，0表示未知，1表示男，2女表示女
     */
    @ApiModelProperty("用户性别，0表示未知，1表示男，2女表示女")
    @TableField("gender")
    private Integer gender;

    /**
     * 用户扩展字段
     */
    @ApiModelProperty("用户扩展字段, JSON格式")
    @TableField("ex")
    private String ex;

    /**
     * 用户状态(NORMAL:正常, DISABLE:封禁)
     */
    @ApiModelProperty("用户状态(NORMAL:正常, DISABLE:封禁)")
    @TableField("status")
    private String status;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private Date updateTime;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * 用户类型(NORMAL:普通用户;VISITOR:游客)
     */
    @ApiModelProperty("用户类型(NORMAL:普通用户;VISITOR:游客)")
    @TableField("user_type")
    private String userType;

    /**
     * 业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)
     */
    @ApiModelProperty("业务平台(BASE:基础平台;CSVC: 客服平台; SOCIAL:社交平台)")
    @TableField("business_platform")
    private String businessPlatform;

    /**
     * 账号昵称拼音
     */
    @ApiModelProperty("账号昵称拼音")
    @TableField("name_pinyin")
    private String namePinyin;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdk id, 用于租户隔离", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
