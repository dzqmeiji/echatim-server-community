package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 组用户关系表
 * </p>
 *
 * @author sun
 * @since 2020-05-07
 */
@ApiModel(value = "群成员")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RoomUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 群ID
     */
    @ApiModelProperty("群ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 唯一性带时间戳自增长的ID
     */
    @ApiModelProperty("唯一性带时间戳自增长的ID")
    @TableField("uid")
    private Long uid;

    /**
     * SDK APP KEY
     */
    @TableField("app_key")
    private String appKey;

    /**
     * 群ID
     */
    @ApiModelProperty("群ID")
    @TableField("rid")
    private Long rid;

    /**
     * 用户auid
     */
    @ApiModelProperty("用户auid")
    @TableField("auid")
    private String auid;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @TableField("sdk_id")
    private Integer sdkId;
}
