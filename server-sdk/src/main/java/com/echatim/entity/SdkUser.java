package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 
 * </p>
 *
 * @author sun
 * @since 2020-05-07
 */
@ApiModel(value = "sdk用户(后台管理员)")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SdkUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("id")
    @TableId("id")
    private Long id;

    /**
     * 登录名
     */
    @ApiModelProperty("用户名")
    @TableField("name")
    private String name;

    /**
     * 电子邮件
     */
    @ApiModelProperty("电子邮件(登录名)")
    @TableField("email")
    private String email;

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    @TableField("phone")
    private String phone;

    /**
     * 密码(md5加盐)
     */
    @ApiModelProperty("密码(md5加盐)")
    @TableField("password")
    private String password;

    /**
     * 角色(ADMIN:管理员; SUPER_ADMIN: 超级管理员)
     */
    @ApiModelProperty("角色(ADMIN:管理员; SUPER_ADMIN: 超级管理员)")
    @TableField("role")
    private String role;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;
}
