package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 系统文档表
 * </p>
 *
 * @author sun
 * @since 2021-08-17
 */
@ApiModel(value = "系统文档")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SystemDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内部ID
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * SDK APP ID
     */
    @ApiModelProperty("appKey")
    @TableField("app_key")
    private String appKey;

    /**
     * 文档的业务分类
     */
    @ApiModelProperty("文档的业务分类")
    @TableField("category")
    private String category;

    /**
     * URL:链接方式;TEXT:文本方式
     */
    @ApiModelProperty("URL:链接方式;TEXT:文本方式")
    @TableField("type")
    private String type;
    /**
     * 文档标题
     */
    @ApiModelProperty("文档标题")
    @TableField("title")
    private String title;

    /**
     * 文本正文
     */
    @ApiModelProperty("文本正文")
    @TableField("content")
    private String content;

    /**
     * 文档链接地址
     */
    @ApiModelProperty("文档链接地址")
    @TableField("url")
    private String url;

    /**
     * 文档图标
     */
    @ApiModelProperty("文档图标")
    @TableField("icon")
    private String icon;

    /**
     * 当type=URL时, 是否传递用户jwt 1:是 0:否
     */
    @ApiModelProperty("当type=URL时, 是否传递用户jwt 1:是 0:否")
    @TableField("pass_user_info")
    private Integer passUserInfo;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdkId", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
