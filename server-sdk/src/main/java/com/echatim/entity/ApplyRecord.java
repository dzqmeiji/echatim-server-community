package com.echatim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 申请记录表
 * </p>
 *
 * @author sun
 * @since 2021-07-30
 */
@ApiModel(value = "好友申请记录表")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ApplyRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 群ID
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 请求来源id
     */
    @ApiModelProperty("请求来源id")
    @TableField("from_id")
    private Long fromId;

    /**
     * 目标来源id
     */
    @ApiModelProperty("目标来源id")
    @TableField("to_id")
    private Long toId;

    /**
     * 附言
     */
    @ApiModelProperty("附言")
    @TableField("attach_text")
    private String attachText;

    /**
     * 来源 ADD_FRIEND:添加朋友
     */
    @ApiModelProperty("来源 ADD_FRIEND:添加朋友")
    @TableField("source")
    private String source;

    /**
     * 申请状态(NORMAL:正常;REJECT:已拒绝;EXPIRE:超时未处理;FINISH:已完成;DEL: 已删除)
     */
    @ApiModelProperty("申请状态(NORMAL:正常;REJECT:已拒绝;EXPIRE:超时未处理;FINISH:已完成;DEL: 已删除)")
    @TableField("status")
    private String status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * sdk id, 用于租户隔离
     */
    @ApiModelProperty(value = "sdk id, 用于租户隔离", hidden = true)
    @TableField("sdk_id")
    private Integer sdkId;
}
