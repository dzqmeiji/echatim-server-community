package com.echatim.filter;

import com.broker.base.protocol.request.RequestMessage;
import com.broker.base.protocol.response.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


/**
 *  Topic 请求拦截器, 功能类似springMVC 的 HandlerInterceptor
 *  功能:
 *  1. 可排除不拦截的Topic;
 *  2. 对拦截的Topic, 做jwt检测, 返回jwt信息.
 *  3. 请求重复提交(待完成)
 *  4. 请求的权限管理(待完成)
 * */
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Slf4j
@Component
public class TopicInterceptor implements ProtocolHandlerInterceptor{
    // 若返回true, 继续执行后面的流程
    @Override
    public boolean preHandle(RequestMessage request, ResponseMessage response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void afterHandler(RequestMessage request, ResponseMessage response, Object handler) throws Exception {

    }

}
