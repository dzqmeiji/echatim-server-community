package com.echatim.broker.localsvc;

import com.broker.base.IBrokerEventBus;
import com.broker.base.IBrokerStorage;
import com.broker.base.event.ClientSendEvent;
import com.broker.base.event.MQSubscribe;
import com.broker.base.protocol.ProtocolMessage;
import com.broker.utils.strorage.StorageFactory;
import com.commom.DBConst;
import com.commom.Topic;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.VoidAckCallback;
import com.echatim.ApplicationWrapper;
import com.echatim.broker.storage.event.ClientOnlineEvent;
import com.echatim.entity.MessageStableOffline;
import com.echatim.form.EventSendForm;
import com.echatim.form.MessageSendForm;
import com.echatim.service.app.MessageStableOfflineService;
import com.google.common.eventbus.Subscribe;
import com.utils.Beans;
import com.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/2/21 13:18
 * Description: 向client 发送消息
 **/

@Slf4j
@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Component("topicSender")
public class TopicSender{
//    private MessageStableOfflineService messageStableOfflineService;
    protected IBrokerEventBus eventBus = null;
    protected SocketIOServer socketIOServer = null;
    protected IBrokerStorage storage = null;
    protected Map<String, Consumer<String>> clientAckCallback = new HashMap<>();

    public void registerEventBus(IBrokerEventBus eventBus){
        this.eventBus = eventBus;
        this.eventBus.register(this);
        // 获取 broker 的存储实例
        storage = StorageFactory.getInstance();
    }

    public void setSocketServer(SocketIOServer socketIOServer) {
        this.socketIOServer = socketIOServer;

        // 设置客户端监听回调到 'topic.clientack'
        this.socketIOServer.addEventListener("topic.clientack", String.class, (client, ackId, ackSender) -> {
            Consumer<String> ackCallback = clientAckCallback.get(ackId);
            if(ackCallback != null){
                ackCallback.accept(ackId);
                clientAckCallback.remove(ackId);
            }
        });
    }

    public IBrokerEventBus getSender() {
        return eventBus;
    }
    public SocketIOServer getSocketIOServer() {
        return socketIOServer;
    }
    public IBrokerStorage getStorage() {
        return storage;
    }

    public boolean hasBrokerReady(){
        return socketIOServer != null && storage != null;
    }
    public MessageStableOfflineService getMessageStableOfflineService() {
        return ApplicationWrapper.getContext().getBean(MessageStableOfflineService.class);
    }


    // 发消息到连接的客户端
    @MQSubscribe
    @Subscribe
    public void onReceiveClientSendMessage(ClientSendEvent message){
        if(Beans.strEmpty(message.getTo())){
            return;
        }
        SocketIOClient client = findClientWithId(message.getClientId());
        if(client == null){
            return;
        }
        String appKey = message.getAppKey();
        MessageSendForm protocolMessage = Beans.copy(message, MessageSendForm.class)
                .setFromUser(message.getFrom())
                .setToTarget(message.getTo())
                .setToTargetAuid(message.getToAuid())
                .setBody(message.getMsgBody());
        protocolMessage.setRequestId(new Date().getTime()+"");
        String topic = Topic.APP_DOWNSTREAM_MESSAGE.name;
        protocolMessage.setMethod(Topic.APP_DOWNSTREAM_MESSAGE.METHOD.SEND_MESSAGE_TO_CLIENT);

        clientSendEventWithAck(client, topic, protocolMessage, (ackId)->{
            log.info("clientSendEventWithAck  ok, ackId:" + ackId);
            getMessageStableOfflineService().lambdaUpdate()
                    .set(MessageStableOffline::getStatus, DBConst.MessageStableOfflineStatus.SENT.name())
                    // where
                    .eq(MessageStableOffline::getAppKey, appKey)
                    .eq(MessageStableOffline::getMid, message.getMid())
                    // update
                    .update();
        });
    }

    // 向连接的客户端发送指令消息
    public void sendClientOnlineMessage(ClientOnlineEvent notifyEvent) {
        SocketIOClient client = findClientWithId(notifyEvent.getClientId());
        if(client == null){
            return;
        }
        EventSendForm eventSendForm = new EventSendForm();
        eventSendForm.setClientId(notifyEvent.getClientId());
        eventSendForm.setRequestId(new Date().getTime()+"");
        eventSendForm.setEventType("CLIENT_ONLINE"); // 事件状态
        eventSendForm.setBody(Beans.json(new HashMap<String, Object>(){{
            put("auid", notifyEvent.getAuid());
            put("online", notifyEvent.getOnline());
        }}));
        String topic = Topic.APP_DOWNSTREAM_MESSAGE.name;
        eventSendForm.setMethod(Topic.APP_DOWNSTREAM_MESSAGE.METHOD.SEND_EVENT_TO_CLIENT);

        clientSendEventWithAck(client, topic, eventSendForm, (ackId)->{
            log.info("clientSendEventWithAck  ok, ackId:" + ackId);
        });
    }

    @Nullable
    protected SocketIOClient findClientWithId(String clientId){
        return this.socketIOServer.getAllClients().stream().filter(v->v.getSessionId().toString().equals(clientId)).findFirst().orElse(null);
    }

    protected void clientSendEventWithAck(SocketIOClient client, String topic, ProtocolMessage protocolMessage, Consumer<String> consumer){
        String ackKey = "ack-" + UUIDUtils.gen();
        clientAckCallback.put(ackKey, consumer);
        client.sendEvent(topic, protocolMessage, new VoidAckCallback(){
            protected void onSuccess() {
            }
        }, ackKey);
    }


}
