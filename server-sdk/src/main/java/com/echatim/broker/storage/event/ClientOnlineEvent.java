package com.echatim.broker.storage.event;

import com.broker.base.BrokerEventMessage;
import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ClientOnlineEvent extends BrokerEventMessage {
    private String eventType = "CLIENT_ONLINE";
    private String clientId = ""; // socket id
    private String auid = ""; // 相关的用户
    private Integer online = -1; // 1: 在线; 0: 不在线
}
