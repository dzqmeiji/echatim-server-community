package com.echatim.broker.storage.event;

import lombok.Data;
import lombok.experimental.Accessors;


@Accessors(chain = true)
@Data
public class LoginEventMessage {
    private String clientId;
    private String appKey = "";
    private String auid = "";
    private String token = "";

    private String jwt = "";
    private String jwtType = "";
}
