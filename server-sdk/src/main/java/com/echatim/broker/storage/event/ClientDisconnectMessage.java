package com.echatim.broker.storage.event;

import lombok.Data;
import lombok.experimental.Accessors;


@Accessors(chain = true)
@Data
public class ClientDisconnectMessage {
    private String clientId;
}
