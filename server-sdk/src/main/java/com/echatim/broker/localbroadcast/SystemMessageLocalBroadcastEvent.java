package com.echatim.broker.localbroadcast;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;


// 系统消息更新事件
@Accessors(chain = true)
@Data
public class SystemMessageLocalBroadcastEvent {
    private List<Long> ids = new ArrayList<>();
}
