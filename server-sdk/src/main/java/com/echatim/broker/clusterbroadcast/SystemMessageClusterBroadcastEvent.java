package com.echatim.broker.clusterbroadcast;

import com.echatim.entity.SystemMessage;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;


// 系统消息更新事件
@Accessors(chain = true)
@Data
public class SystemMessageClusterBroadcastEvent extends SystemMessage {
    private String way = ""; // P2P:单播,P2R:广播
    private List<String> roomMembers = new ArrayList<>(); // 若way = P2R, 这是群所有成员的auid
}
