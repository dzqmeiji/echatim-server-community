package com.echatim.config;

import com.corundumstudio.socketio.SocketConfig;
import com.corundumstudio.socketio.SocketIOServer;
import com.echatim.broker.BrokerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Configuration
public class SocketIOConfig {
    @Value("${socketio.host}")
    private String host;

    @Value("${socketio.port}")
    private Integer port;

    @Value("${socketio.bossCount}")
    private int bossCount;

    @Value("${socketio.workCount}")
    private int workCount;

    @Value("${socketio.allowCustomRequests}")
    private boolean allowCustomRequests;

    @Value("${socketio.upgradeTimeout}")
    private int upgradeTimeout;

    @Value("${socketio.pingTimeout}")
    private int pingTimeout;

    @Value("${socketio.pingInterval}")
    private int pingInterval;

    @Value("${broker.machineId}")
    private String brokerMachineId;
    @Value("${broker.auth.signingkey}")
    private String brokeAuthSigningKey;
    @Value("${broker.auth.tokenvalid}")
    private String brokeAuthTokenValid;
    @Value("${broker.storage.use}")
    private String brokerStorageUse;
    @Value("${broker.storage.redis-host}")
    private String brokerStorageRedisHost;
    @Value("${broker.event.use}")
    private String brokerEventUse;
    @Value("${broker.event.mq-server}")
    private String brokerEventMqServer;
    @Value("${broker.event.mq-point}")
    private String brokerEventMqPoint;



    /**
     * @return
     */
    @Bean
    public SocketIOServer socketIOServer() {
        SocketConfig socketConfig = new SocketConfig();
        socketConfig.setTcpNoDelay(true);
        socketConfig.setSoLinger(0);
        com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
        config.setSocketConfig(socketConfig);
        if(!"".equals(host)){
            config.setHostname(host);
        }
        config.setPort(port);
        config.setBossThreads(bossCount);
        config.setWorkerThreads(workCount);
        config.setAllowCustomRequests(allowCustomRequests);
        config.setUpgradeTimeout(upgradeTimeout);
        config.setPingTimeout(pingTimeout);
        config.setPingInterval(pingInterval);
        return new SocketIOServer(config);
    }


    /**
     * @return
     */
    @Bean
    public BrokerConfig brokerConfig() {
        BrokerConfig brokerConfig = new BrokerConfig();
        brokerConfig.setMachineId(brokerMachineId);
        brokerConfig.setStorageUsing(brokerStorageUse);
        brokerConfig.setRedisHost(brokerStorageRedisHost);
        brokerConfig.setEventUsing(brokerEventUse);
        brokerConfig.setMqPoint(brokerEventMqPoint);
        brokerConfig.setMqServerUrl(brokerEventMqServer);
        brokerConfig.setSigningKey(brokeAuthSigningKey);
        brokerConfig.setTokenValid(brokeAuthTokenValid);

        return brokerConfig;
    }
}
