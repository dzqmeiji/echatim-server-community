package com.echatim.config;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: daizhiqiang
 * @date: 2021年5月27日14:42:54
 * @describe flyway配置
 */

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Configuration
public class FlywayConfig {

	@Value("${spring.datasource.dynamic.datasource.master.url}")
	private String datasourceUrl;
	@Value("${spring.datasource.dynamic.datasource.master.username}")
	private String databaseUsername;
	@Value("${spring.datasource.dynamic.datasource.master.password}")
	private String databasePassword;
	@Value("${spring.profiles.active}")
	private String profilesActive;

	/**
	 * version: 6.0.7 写法
	 */
	@Bean
	Flyway flyway() {
		ClassicConfiguration classicConfiguration = new ClassicConfiguration();
		classicConfiguration.setDataSource(datasourceUrl, databaseUsername, databasePassword);
		classicConfiguration.setBaselineOnMigrate(true);
		classicConfiguration.setLocations(new Location("db/migration"));
		classicConfiguration.setPlaceholderReplacement(false); // 禁止类似${XXX}的变量替换
		Flyway flyway = new Flyway(classicConfiguration);
		flyway.repair();
		flyway.migrate();
		return flyway;
	}

}
