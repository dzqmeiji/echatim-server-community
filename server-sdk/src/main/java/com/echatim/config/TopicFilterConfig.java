package com.echatim.config;

import com.echatim.filter.HttpTopicInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Configuration
public class TopicFilterConfig implements WebMvcConfigurer {
    @Bean
    public HttpTopicInterceptor getLoginInterceptor(){
        return  new HttpTopicInterceptor();
    }
    //   把自定义的拦截器添加到mvc 配置中
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.getLoginInterceptor()).addPathPatterns("/**");
    }
}
