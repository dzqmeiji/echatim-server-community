package com.echatim.config;


import com.event.EventBus;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Configuration
public class EventBusConfig {
    @Bean(name = "eventBus")
    public EventBus getSender() {
        return new EventBus();
    }

}
