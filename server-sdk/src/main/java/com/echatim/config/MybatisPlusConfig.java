package com.echatim.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {
    /**
     *  Page 使用方式
     *  (1).java 方式:
     *         IPage<User> userPage = this.lambdaQuery()
     *         .like(StringUtil.isNotEmpty(form.getMemo()), User::getMemo, form.getMemo())
     *         .eq(StringUtil.isNotEmpty(form.getStudentNumber()), User::getStudentNumber, form.getStudentNumber())
     *         .page(new Page<>(form.getPage(), form.getPageSize()));
     *
     *  (2).mapper xml 方式:
     *     IPage<OrdersListDTO> listOrder(Page page, @Param(value = "form") OrdersQueryForm form);
     *
     *     xml:
     *     <select id="listOrder" resultType="com.xxx.dto.OrdersListDTO">
     *         ...
     *     </select>
     * */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
