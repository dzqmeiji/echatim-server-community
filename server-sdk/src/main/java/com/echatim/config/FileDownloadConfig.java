package com.echatim.config;

import com.commom.LocalFileRoute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@ConditionalOnProperty(name="echatim.sdk.auth-type", havingValue="community")
@Configuration
public class FileDownloadConfig extends WebMvcConfigurerAdapter {
    @Value("${echatim.file.upload-file-dir}")
    private String uploadFileDir;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(LocalFileRoute.common.base +  LocalFileRoute.common.file.download + "/**").addResourceLocations("file:" + uploadFileDir + "/");
    }

}
