package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.User;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface UserMapper extends CommonMapper<User> {
}
