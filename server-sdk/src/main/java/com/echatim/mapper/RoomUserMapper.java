package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.RoomUser;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface RoomUserMapper extends CommonMapper<RoomUser> {
}
