package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.SystemMessage;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface SystemMessageMapper extends CommonMapper<SystemMessage> {
}
