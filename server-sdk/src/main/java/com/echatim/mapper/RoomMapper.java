package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.Room;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface RoomMapper extends CommonMapper<Room> {
}
