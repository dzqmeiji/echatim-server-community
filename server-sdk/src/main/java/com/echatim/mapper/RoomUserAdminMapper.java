package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.RoomUserAdmin;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface RoomUserAdminMapper extends CommonMapper<RoomUserAdmin> {
}
