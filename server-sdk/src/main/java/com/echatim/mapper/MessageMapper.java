package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.dto.HistorySessionDTO;
import com.echatim.entity.Message;
import com.echatim.form.HistoryListForm;
import com.echatim.form.HistoryListSessionForm;
import com.echatim.form.HistoryRoomRecentlyForm;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@DS("master")
@Repository
public interface MessageMapper extends CommonMapper<Message> {
    List<Message> listHistory(@Param("form") HistoryListForm listForm);
    List<Message> listRoomHistory(@Param("form") HistoryListForm listForm);
    List<HistorySessionDTO> listHistorySession(@Param("form") HistoryListSessionForm listForm);
    List<Message> listRoomRecentlyOne(@Param("form") HistoryRoomRecentlyForm listForm);
}
