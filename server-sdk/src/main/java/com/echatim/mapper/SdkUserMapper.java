package com.echatim.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.SdkUser;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface SdkUserMapper extends CommonMapper<SdkUser> {
}
