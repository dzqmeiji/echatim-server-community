package com.echatim.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.echatim.CommonMapper;
import com.echatim.entity.FileInfo;
import org.springframework.stereotype.Repository;

@DS("master")
@Repository
public interface FileInfoMapper extends CommonMapper<FileInfo> {
}
