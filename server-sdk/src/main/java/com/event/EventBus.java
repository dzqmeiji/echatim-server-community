package com.event;

public class EventBus {
    /** 事件任务总线 */
    private final static com.google.common.eventbus.EventBus eventBus = new com.google.common.eventbus.EventBus();
    /**
     * 触发同步事件
     *
     * @param event
     */
    public void post(Object event) {
        eventBus.post(event);
    }
    /**
     * 注册事件处理器
     *
     * @param handler
     */
    public void register(Object handler) {
        eventBus.register(handler);
    }
    /**
     * 注销事件处理器
     *
     * @param handler
     */
    public void unregister(Object handler) {
        eventBus.unregister(handler);
    }
}
