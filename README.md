#### echatim 后端项目

官网: http://www.echat.work

文档: http://47.115.54.220:57070

1. 项目说明

    采用 springboot+mybatis 开发, 使用Http api, socket.io 协议与客户端SDK交互。

    2.01版本后已合并社区版中间件echatim-server-broker-community, 无需再另行下载编译。

2. 运行方式
```bash
mvn clean install -DskipTests=true
java -jar server-community-deploy/target/server-community-deploy-1.0.jar

```

2. 端口情况

    swagger:  http://localhost:8082/swagger-ui.html
    
    socket-io: http://localhost:9092


#### 使用/管理

1. 用户登录
```bash
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "appKey": "TSDKTEST00001", \ 
   "auid": "admin", \ 
   "token": "f2ea07d1cad84b95ad76f106ebf4f412" \ 
 }' 'http://localhost:8082/v1/connection/authority_request'
```

2. 创建一个用户
```bash

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d '{ \ 
   "auid": "newuser001", \ 
   "avatar": "头像url", \ 
   "birth": "生日", \ 
   "email": "邮件", \ 
   "ex": "{}", \ 
   "gender": 0, \ 
   "mobile": "手机", \ 
   "name": "账号昵称", \ 
   "sign": "签名", \ 
   "token": "password123456" \ 
 }' 'http://localhost:8082/v1/user/add'
```


#### 支持的 uniapp 客户端(社交版)

客户端:

![1_2](https://gitee.com/dzqmeiji/echatim-resource/raw/master/pics/readme/echat_social_1_2.gif)

[详细请点击](http://doc.echat.work:58083/1.1%20%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D/1.1.1%20%E5%95%86%E7%94%A8%E7%A4%BE%E4%BA%A4%E7%89%88/)

#### 版本记录:
##### v 2.01 - 2021-08-20

* 支持商用社交版

##### v 1.02 - 2020-06-28

* 加入本地文件服务器支持.


##### v 1.01 - 2020-05-24
* 第一版源码发布


#### 其它:
##### 推荐购买阿里云服务器测试
安装完java环境后，使用 java -jar server-community-deploy-1.0.jar 即可完成部署。

[详细请点击](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=bbibyrq9)

[![阿里云服务器优惠](https://gitee.com/dzqmeiji/echatim-resource/raw/master/pics/adv/aliyun_adv.png)](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=bbibyrq9)
