# -*- coding: utf-8 -*-
import os

from json2cppFull import *


class Generator:
   
    def __init__(self, inputJsonPath, outputPath):
        self.inputJsonPath = inputJsonPath
        self.outputPath = outputPath

    def generate(self):
        # jsonData = "{\"aa\":''}"
        jsonData = """
{"RoomListDTO":{"rid":0,"name":"","owner":"","announce":"","introduce":"","inviteText":"","avatar":"","confJoinmode":"","confBeinvite":"","confInviteother":"","confUpdate":"","maxMember":0,"status":""}}
        """
        dirs = os.listdir(self.inputJsonPath)
        for file in dirs:
            print("file:" + file)
            f = open(self.inputJsonPath + "/" + file, "r")
            jsonData = f.read()
            Json2cpp().runJsonData(str(jsonData), self.outputPath, None)



if __name__ == '__main__':
    currentPath = os.getcwd()
    targetPath = currentPath.split('server-tools')[0] + "/server-tools/target"
    jsonPath = targetPath + "/dto_json"
    cppPath = targetPath + "/dto_cpp"
    print("jsonPath:" + jsonPath)
    print("cppPath:" + cppPath)
    generator = Generator(jsonPath, cppPath)
    generator.generate()

