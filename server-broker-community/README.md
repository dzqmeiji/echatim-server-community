# echatim-server-broker-community

#### 介绍
社区版echatim 后台中间件, 供[echatim后端项目](https://gitee.com/dzqmeiji/echatim-server-community)使用

#### 编译方式
```bash
git clone https://gitee.com/dzqmeiji/echatim-server-broker-community.git
cd echatim-server-broker-community/
git checkout -b v1.01 v1.01
mvn clean install -DskipTests=true  # 编译后端中间件
```


#### 官网

官网: http://www.echat.work

文档: http://doc.echat.work:58083
