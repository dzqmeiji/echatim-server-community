package com.broker.utils.events.support;

public interface IMQProducer {
     void start();
     void sendMessage(String topic, Object msg) throws Exception;
     void stop();
}
