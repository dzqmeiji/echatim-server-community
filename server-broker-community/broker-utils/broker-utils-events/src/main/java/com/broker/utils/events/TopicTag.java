package com.broker.utils.events;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * @author Daizhiqiang <zqdai@sailfish.com.cn>
 * create by 2020/3/23 15:09
 * Description: echatimbroker
 **/
@Accessors(chain = true)
@Data
public class TopicTag {
    private String topic;
    private String tag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicTag topicTag = (TopicTag) o;
        return Objects.equals(topic, topicTag.topic) &&
                Objects.equals(tag, topicTag.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, tag);
    }
}
