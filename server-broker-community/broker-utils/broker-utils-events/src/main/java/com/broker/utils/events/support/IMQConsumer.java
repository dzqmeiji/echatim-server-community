package com.broker.utils.events.support;

import java.util.function.BiConsumer;

public interface IMQConsumer {
     void start(String topic);
     void appendSubscribe(String topic, String tag, BiConsumer<String, String> consumer);
     void stop();
}
