package com.broker.utils.events;

import com.broker.base.BrokerEventMessage;
import com.broker.base.IBrokerEventBus;
import com.broker.utils.events.support.MQConfig;
import com.broker.utils.events.support.MQEvent;
import com.broker.utils.events.support.eventbus.EventBusEvent;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/12 9:31
 * Description: echatimbroker
 **/
public class EventFactory {
    public static final String EVENT_TYPE_EVENTBUS = "eventbus";
    public static final String EVENT_TYPE_ROCKETMQ = "rocketmq";
    public static final String EVENT_TYPE_KAFKA = "kafka";

    private static IBrokerEventBus storage = null;
    // 懒加载
    public  static IBrokerEventBus getInstance(){
        if(storage == null){
            storage =  EventFactory.create();
            return storage;
        }
        else {
            return storage;
        }
    }
    public static  <T extends BrokerEventMessage> IBrokerEventBus create(){
        // 默认使用 google cache
        String eventUsing = "".equals(System.getProperty("broker.event.use", "")) ? EventFactory.EVENT_TYPE_EVENTBUS : System.getProperty("broker.event.use");
        if(EventFactory.EVENT_TYPE_EVENTBUS.equals(eventUsing)){
            return new EventBusEvent();
        }
        else if(EventFactory.EVENT_TYPE_ROCKETMQ.equals(eventUsing) || EventFactory.EVENT_TYPE_KAFKA.equals(eventUsing)){
            //设置 mq 配置项值
            MQConfig rocketMQConfig = new MQConfig();
            rocketMQConfig.setMqType(eventUsing);
            rocketMQConfig.setMqServerUrl(System.getProperty("broker.event.mq.server", "localhost:9876"));
            String point = System.getProperty("broker.event.mq.point", "");
            if("".equals(point)){
                throw new RuntimeException("Please special broker.event.mq.point value in system.property when using mq.");
            }
            rocketMQConfig.setMqPoint(point);
            return new MQEvent(rocketMQConfig);
        }
        else {
            throw new RuntimeException(" broker storage initialization failed, with setting: broker.storage.use=" + System.getProperty("broker.storage.use"));
        }
    }
}
