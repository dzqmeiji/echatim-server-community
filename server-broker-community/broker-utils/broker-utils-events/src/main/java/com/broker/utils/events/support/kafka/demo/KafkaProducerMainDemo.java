package com.broker.utils.events.support.kafka.demo;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Date;
import java.util.Properties;

/**
 * @author Daizhiqiang <zqdai@sailfish.com.cn>
 * create by 2020/3/23 12:42
 * Description: echatimbroker
 **/
public class KafkaProducerMainDemo {
//    private static final String mqServerUrl = "192.168.0.6:9092";

    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put("bootstrap.servers", KafkaDemoConfig.MQ_SERVER_URL);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        for (int i = 0; i < 100; i++){
            String value = i + "_" + new Date().getTime();
            System.out.println(value);
            producer.send(new ProducerRecord<String, String>(KafkaDemoConfig.MQ_TOPIC, Integer.toString(i), value));
            Thread.sleep(1000);
        }
        producer.close();
    }
}
