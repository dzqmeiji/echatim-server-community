package com.broker.utils.events.support.kafka.demo;

/**
 * @author Daizhiqiang <zqdai@sailfish.com.cn>
 * create by 2020/3/23 12:51
 * Description: echatimbroker
 **/
public class KafkaDemoConfig {
    public static final String MQ_SERVER_URL = "192.168.0.6:9092";
    public static final String MQ_TOPIC = "topic-java-test2";
//    public static final String MQ_TOPIC = "kafka-test-topic";
}
