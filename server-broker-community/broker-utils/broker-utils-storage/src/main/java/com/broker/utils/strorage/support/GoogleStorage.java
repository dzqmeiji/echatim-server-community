package com.broker.utils.strorage.support;

import com.broker.base.IBrokerStorage;
import com.broker.base.StorageException;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/12 9:29
 * Description: echatimbroker
 **/

@Slf4j
public class GoogleStorage implements IBrokerStorage {
//    Cache<String, V> cache = null;
    Map<String, Cache<String, String>> objCachesMap = new HashMap<>();
    public GoogleStorage(){
    }

    private void createObjCacheIfNotExist(String obj){
        if(!objCachesMap.containsKey(obj)){
            Cache<String, String> cache = CacheBuilder.newBuilder()
                    //设置cache的初始大小为10，要合理设置该值
                    .initialCapacity(10)
                    //设置并发数为5，即同一时间最多只能有5个线程往cache执行写入操作
                    .concurrencyLevel(5)
                    //构建cache实例
                    .build();
            objCachesMap.putIfAbsent(obj, cache);
        }
    }

    @Override
    public void setKeyValue(String obj, String key, String value) throws RuntimeException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        objCache.put(key, value);
    }

    @Override
    public void setKeyValue(String obj, Map<String, String> keyValues) throws RuntimeException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        keyValues.forEach((k,v)->{
            objCache.put(k, v);
        });

    }

//    @NotNull
    @Nullable
    public String getValue(String obj, String key) throws StorageException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        return objCache.getIfPresent(key);
    }



    @Nullable
    @Override
    public String getValue(String obj, String key, Supplier<String> fetchFunc) throws StorageException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        String value =  objCache.getIfPresent(key);
        if(value == null){
            String newValue = fetchFunc.get();
            if(newValue != null){
                objCache.put(key, newValue);
            }
            return newValue;
        }
        return value;
    }



    @Override
    public Map<String, String> getValue(String obj, Set<String> keys) throws StorageException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        Map<String, String> keyValues = new HashMap<>();
        List<String> errors = new ArrayList<>();
        keys.forEach(k->{
            String value =  objCache.getIfPresent(k);
            if(value == null){
                errors.add("[storage] key:" + k + " not exist for:" + obj);
            }
            else {
                keyValues.put(k, value);
            }
        });
        if(!errors.isEmpty()){
            System.err.println("waring:" + String.join("\n", errors));
        }
        return keyValues;
    }


    @Override
    public Map<String, String> getAllKeyValues(String obj) throws StorageException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        Map<String, String> maps = new HashMap<>();
        objCache.asMap().entrySet().forEach(v->{
            maps.put(v.getKey(), v.getValue());
        });
        return maps;
    }

    @Override
    public void remove(String obj, String key) throws StorageException {
        createObjCacheIfNotExist(obj);
        Cache<String, String> objCache = objCachesMap.get(obj);
        if(objCache == null){
            throw new StorageException(" cache not exist.");
        }
        objCache.invalidate(key);
    }

    @Override
    public void globalSynchronized(String key, Consumer<String> handler) throws StorageException {
        synchronized (key){
            handler.accept(key);
        }
    }
}
