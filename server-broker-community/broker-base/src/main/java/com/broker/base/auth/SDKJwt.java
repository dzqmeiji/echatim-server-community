package com.broker.base.auth;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/11/28 11:42
 * Description: education
 **/

@Accessors(chain = true)
@Data
public class SDKJwt {
    private String appKey = ""; // appKey(唯一性)
    private String appSecret = ""; // appSecret(唯一性)

    public static final String APP_KEY = "APP_KEY";
    public static final String APP_SECRET = "APP_SECRET";
    public boolean isEmpty(){
        return this.appKey == null || "".equals(this.appSecret);
    }
}
