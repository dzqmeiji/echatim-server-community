package com.broker.base.protocol.response;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/3/12 14:11
 * Description: echatimbroker
 **/

import com.alibaba.fastjson.JSONObject;
import com.broker.base.utils.ObjectUtils;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * REST API 返回结果
 *
 * @author hubin
 * @since 2018-06-05
 */
@Data
@Accessors(chain = true)
public class Resp<T> implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 业务错误码
     */
    private long code;
    /**
     * 业务错误详情码(用于后台定位错误信息)
     */
    private String errno;
    /**
     * 结果集
     */
    private T data;
    /**
     * 描述
     */
    private String msg;

    public Resp() {
    }
    public JSONObject toJSON(){
        return JSONObject.parseObject(ObjectUtils.json(this));
    }
    public static <T> Resp<T> ok(T data) {
        return restResult(data, 0, "success");
    }

    public static <T> Resp<T> failed(T data, String errMsg) {
        return restResult(data, RespApiErrorCode.FAILED.getCode(), String.valueOf(errMsg));
    }
    public static <T> Resp<T> failed(T data, String errMsg, long code) {
        return restResult(data, code, String.valueOf(errMsg));
    }

    public static <T>  Resp<T>  failed(String errMsg) {
        return restResult(null, RespApiErrorCode.FAILED.getCode(), errMsg);
    }

    public static <T> Resp<T> failed(String errMsg, long code) {
        return restResult(null, code, String.valueOf(errMsg));
    }


    public static <T> Resp<T> failed(IRespErrorCode errorCode) {
        return restResult(null, errorCode.getCode(), errorCode.getMsg());
    }


    private static <T> Resp<T> restResult(T data, long code, String msg) {
        Resp<T> apiResult = new Resp<T>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public boolean ok() {
        return RespApiErrorCode.SUCCESS.getCode() == code;
    }


}

