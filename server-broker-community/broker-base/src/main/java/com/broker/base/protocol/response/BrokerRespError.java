package com.broker.base.protocol.response;

/**
 *  Resp Broker 错误吗范围: -32, -200
 * */
public enum BrokerRespError implements IRespErrorCode{
    TOKEN(-32, "错误"),
    TOKEN_2(-33, "错误");
    private Integer code = -1;
    private String msg = "";
    BrokerRespError(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
