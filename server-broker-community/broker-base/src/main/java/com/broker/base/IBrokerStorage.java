package com.broker.base;

import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface IBrokerStorage {
     void setKeyValue(String obj, String key, String value) throws StorageException;
     void setKeyValue(String obj, Map<String, String> keyValues) throws StorageException;
     @Nullable
     String getValue(String obj, String key) throws StorageException;

     @Nullable
     String getValue(String obj, String key, Supplier<String> fetchFunc) throws StorageException;

     Map<String, String> getValue(String obj, Set<String> keys) throws StorageException;
     Map<String, String> getAllKeyValues(String obj) throws StorageException;
     void remove(String obj, String key) throws StorageException;

    void globalSynchronized(String key, Consumer<String> handler) throws StorageException;
}
