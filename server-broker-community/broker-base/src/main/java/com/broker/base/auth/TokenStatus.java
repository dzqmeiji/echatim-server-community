package com.broker.base.auth;

import lombok.Data;
import lombok.experimental.Accessors;


@Accessors(chain = true)
@Data
public class TokenStatus {
    public static final int ERROR_CODE_INVALID_TOKEN = -11;
    public static final int ERROR_CODE_TOKEN_EXPIRE = -12;


    private Integer code;
    private String errorMsg;
}
