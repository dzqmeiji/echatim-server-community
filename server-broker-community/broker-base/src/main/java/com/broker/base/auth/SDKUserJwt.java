package com.broker.base.auth;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author kong <androidsimu@163.com>
 * create by 2019/11/28 11:42
 * Description: education
 **/

@Accessors(chain = true)
@Data
public class SDKUserJwt {
    private String sdkUid = ""; // sdk_user id(唯一性)
    private String name = ""; // sdk_user name(唯一性)
    private String role = ""; // role(ADMIN,SUPER_ADMIN)

    public static final String SDK_UID = "SDK_UID";
    public static final String NAME = "NAME";
    public static final String ROLE = "ROLE";
    public boolean isEmpty(){
        return this.sdkUid == null || "".equals(this.name) || "".equals(this.role);
    }
}
