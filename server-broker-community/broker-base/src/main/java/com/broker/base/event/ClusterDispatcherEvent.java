package com.broker.base.event;

import com.alibaba.fastjson.JSONObject;
import com.broker.base.BrokerEventMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

//import org.json.JSONObject;

@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
@Data
public class ClusterDispatcherEvent extends BrokerEventMessage {
    private String appKey = ""; // 平台sdk
    private String mid = ""; // 消息uid
    private String from = ""; // from user auid
    private String to = ""; // to user auid(way = P2P) ; to room rid(way = P2R)
    private String toAuid = ""; // to user auid
    private List<String> roomMembers = new ArrayList<>(); // 若way = P2R, 这是群所有成员的auid
    private String way = ""; // P2P,P2R,P2HR
    private String type; // TEXT, IMAGE ...
    private JSONObject msgBody = new JSONObject();
    private Integer offline = 0; // 0: 普通消息; 1: 离线消息
}
