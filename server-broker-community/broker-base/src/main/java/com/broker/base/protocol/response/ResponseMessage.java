package com.broker.base.protocol.response;


import com.alibaba.fastjson.JSON;
import com.broker.base.utils.ObjectUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class ResponseMessage<T>{
    private String requestId = "";// 单次请求requestId
    //    private Resp response = Resp.ok(null);
    private Object response = Resp.ok(null).toJSON();

    public ResponseMessage(String requestId, boolean succeed, T failMsg){
        this.requestId = requestId;
        if(succeed){
            this.response = Resp.ok(null).toJSON();
        }
        else {
            this.response = Resp.failed(String.valueOf(failMsg)).toJSON();
        }
    }

    public ResponseMessage(boolean succeed, T failMsg){
        if(succeed){
            this.response = JSON.parseObject(ObjectUtils.json(Resp.ok(null)));
        }
        else {
            this.response = Resp.failed(String.valueOf(failMsg)).toJSON();
        }
    }

    public ResponseMessage(boolean succeed){
        if(succeed){
            this.response = Resp.ok(null).toJSON();
        }
        else {
            this.response = Resp.failed("").toJSON();
        }
    }


    public static ResponseMessage succeed(String requestId){
        return new ResponseMessage<String>()
                .setRequestId(requestId)
                .setResponse(Resp.ok("ok").toJSON());
    }

    public static ResponseMessage failed(String requestId, String msg){
        return new ResponseMessage<String>().setResponse(Resp.failed(msg).toJSON());
    }
}